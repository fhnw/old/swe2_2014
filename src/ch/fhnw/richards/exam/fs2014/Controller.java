package ch.fhnw.richards.exam.fs2014;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Controller extends MouseAdapter {
	View v;
	Model m;
	public Controller(View v, Model m) {
		this.v = v;
		this.m = m;
		
		// Register the event handler
		v.canvas.addMouseListener(this);
	}
	
	/**
	 * Process "mouseClicked" events
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		Point p = e.getPoint();
		m.process(p);
		v.refresh();
	}
}
