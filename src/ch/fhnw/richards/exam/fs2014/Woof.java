package ch.fhnw.richards.exam.fs2014;

import java.util.ArrayList;
import java.util.TreeSet;

public class Woof {
	private static Woof w;
	ArrayList<Thing> things = new ArrayList<>();

	public static void main(String[] args) {
		w = new Woof();
	}
	
	public synchronized void addThing(Thing t) {
		things.add(t);
	}
	
	public synchronized void deleteThing(Thing t) {
		things.remove(t);
	}
	
	public static class Thing {
		
	}
}
