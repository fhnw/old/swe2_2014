package ch.fhnw.richards.exam.fs2014;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleServer extends Thread {
	ServerSocket listener;

	public static void main(String[] args) {
		SimpleServer s = new SimpleServer();
		s.start();
	}

	public SimpleServer() {
		try {
			// Create the ServerSocket, listen on port 50001
			listener = new ServerSocket(50001);
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

	@Override
	public void run() {
		Socket client = null;
		InputStream in = null;
			while (true) {
				try {
					// Accept a client connection
					System.out.println("waiting for a connection");
					client = listener.accept();
					
					// Get the client's input stream
					in = client.getInputStream();

					// Read client input one byte at a time,
					// and write it to the console.
					// Stop when there is no more input.
					int b;
					while ((b = in.read()) != -1) {
						System.out.print((char) b);
					}
				} catch (IOException e) {
					// Do nothing
				} finally {
					try { if (in != null) in.close(); } catch (IOException e) { }
					try { if (client != null) client.close(); } catch (IOException e) { }
				}
			}
	}
}
