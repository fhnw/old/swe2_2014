package ch.fhnw.richards.exam.fs2014;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class View extends JFrame {
	Model m;
	Canvas canvas = new Canvas();

	public View(Model m) {
		super("MVC exam question");
		this.m = m;
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.setLayout(new BorderLayout());
		this.add(canvas, BorderLayout.CENTER);
		canvas.setPreferredSize(new Dimension(300,300));
		this.pack();
		this.setVisible(true);
	}
	
	public void refresh() {
		System.out.println("Refresh the  view");
	}
}
