package ch.fhnw.richards.exam.fs2014;

public class MVC {
	public static void main(String[] args) {
		Model m = new Model();
		View v = new View(m);
		Controller c = new Controller(v, m);
	}
}
