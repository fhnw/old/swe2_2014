package ch.fhnw.richards.exam.fs2014;

import java.awt.Point;

public class Model {
	public Model() {
		// Initialization
	}

	public void process(Point mouseClick) {
		System.out.println("Processing click at " + mouseClick.x + ", " + mouseClick.y);
	}
}
