package ch.fhnw.richards.bonus;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class Lottery extends JFrame {
	private BallLabel[] balls = new BallLabel[6];
	private BallLabel luckyBall;
	private JButton btnPlay;

	public static void main(String[] args) {
		new Lottery();
	}
	
	public Lottery() {
		super();
		this.setTitle("Lottery");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		Box mainContent = Box.createVerticalBox();
		this.setLayout(new BorderLayout());
		this.add(mainContent, BorderLayout.CENTER);
		
		mainContent.add(Box.createVerticalStrut(30));
		mainContent.setBackground(new Color(220,255,255));
		mainContent.setOpaque(true);

		Box labelBox = Box.createHorizontalBox();
		labelBox.add(Box.createHorizontalGlue());
		JLabel winLabel = new JLabel("Your chance of winning: 1 in 31,474,716");
		winLabel.setHorizontalAlignment(JLabel.CENTER);
		labelBox.add(winLabel);
		labelBox.add(Box.createHorizontalGlue());
		mainContent.add(labelBox);
		
		mainContent.add(Box.createVerticalStrut(30));
		
		Box numberBox = Box.createHorizontalBox();
		mainContent.add(numberBox);

		numberBox.add(Box.createHorizontalStrut(30));
		for (int i = 0; i < 6; i++) {
			balls[i] = new BallLabel();
			numberBox.add(balls[i]);
			numberBox.add(Box.createHorizontalStrut(10));
		}
		numberBox.add(Box.createHorizontalStrut(30));
		luckyBall = new BallLabel();
		luckyBall.setBallColor(Color.YELLOW);
		numberBox.add(luckyBall);
		numberBox.add(Box.createHorizontalStrut(30));
		
		balls[0].setText("13");
		balls[1].setText("16");
		balls[2].setText("19");
		balls[3].setText("23");
		balls[4].setText("34");
		balls[5].setText("37");
		luckyBall.setText("4");
		
		mainContent.add(Box.createVerticalStrut(30));
		
		Box controlBox = Box.createHorizontalBox();
		mainContent.add(controlBox);
		btnPlay = new JButton("Play");
		controlBox.add(Box.createHorizontalGlue());
		controlBox.add(btnPlay);
		controlBox.add(Box.createHorizontalStrut(30));

		mainContent.add(Box.createVerticalStrut(30));

		this.pack();
		this.setVisible(true);
	}

	
	public static class BallLabel extends JPanel {
		private JLabel lblInner;
		public BallLabel() {
			super(true);
			this.setOpaque(false);
	        lblInner = new JLabel();
	        lblInner.setHorizontalAlignment(JLabel.CENTER);
	        lblInner.setOpaque(true);
	        lblInner.setBackground(Color.WHITE);
	        LineBorder line = new LineBorder(Color.BLACK, 3, false);
	        EmptyBorder margin = new EmptyBorder(5,5,5,5);
	        lblInner.setBorder(new CompoundBorder(line, margin));
	        add(lblInner, BorderLayout.CENTER);
		}
		
		public void setText(String text) {
			lblInner.setText(text);
		}
		
		public void setBallColor(Color c) {
			lblInner.setBackground(c);
		}
	}
}
