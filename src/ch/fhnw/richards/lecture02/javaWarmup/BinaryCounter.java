package ch.fhnw.richards.lecture02.javaWarmup;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

public class BinaryCounter extends JFrame implements ActionListener {
	private static final int NUMBER_OF_BITS = 8;
	private Integer value;
	private boolean[] bitValues = new boolean[NUMBER_OF_BITS];

	// GUI components
	private Counter counter = new Counter(NUMBER_OF_BITS);
	private JButton btnIncrement = new JButton("Increment");
	private JLabel lblDecimalValue = new JLabel();

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new BinaryCounter();
			}
		});
	}

	public BinaryCounter() {
		super("Binary Counter");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Setup the layout
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.add(Box.createVerticalStrut(20));
		this.add(counter);
		this.add(Box.createVerticalStrut(20));
		Box lowerBox = Box.createHorizontalBox();
		this.add(lowerBox);
		this.add(Box.createVerticalStrut(20));

		lowerBox.add(Box.createHorizontalGlue());
		lowerBox.add(Box.createHorizontalStrut(10));
		lowerBox.add(btnIncrement);
		lowerBox.add(Box.createHorizontalStrut(10));
		lowerBox.add(lblDecimalValue);
		lowerBox.add(Box.createHorizontalStrut(10));

		value = 0;

		btnIncrement.addActionListener(this);

		this.pack();
		this.setVisible(true);
		
		displayValue();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		value++;
		displayValue();
	}

	private void displayValue() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				counter.setValue(value);
				lblDecimalValue.setText(value.toString());
			}
		});
	}

	private static class Counter extends Box {
		private int numBits;
		private Light[] lights;

		public Counter(int numBits) {
			super(BoxLayout.X_AXIS);
			this.numBits = numBits;
			this.lights = new Light[numBits];
			this.add(Box.createHorizontalGlue());

			// Initialize all of the lights
			for (int i = 0; i < numBits; i++) {
				this.lights[i] = new Light();
				this.add(lights[i]);
				this.add(Box.createHorizontalStrut(5));
			}
		}

		public void setValue(int value) {
			for (int i = 1; i <= numBits; i++) {
				lights[lights.length - i].setBit(value % 2 == 1);
				value /= 2;
			}
		}
	}

	private static class Light extends JLabel {
		public Light() {
			super();
			setPreferredSize(new Dimension(40, 40));
			setOpaque(true);
			setBit(false);
		}

		public void setBit(boolean on) {
			if (on) {
				setBackground(Color.PINK);
				setText("1");
			} else {
				setBackground(Color.LIGHT_GRAY);
				setText("0");
			}
		}
	}
}
