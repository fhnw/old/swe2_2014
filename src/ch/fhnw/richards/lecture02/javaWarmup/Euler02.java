package ch.fhnw.richards.lecture02.javaWarmup;

public class Euler02 {

	public static void main(String[] args) {
		int fibo1 = 1;
		int fibo2 = 1;
		int sum = 0;
		while (fibo2 <= 4000000) {
			int fiboNew = fibo1 + fibo2;
			fibo1 = fibo2;
			fibo2 = fiboNew;
			if (fibo2 % 2 == 0) {
				sum += fibo2;
			}
		}
		
		System.out.println("Sum = " + sum);
	}

}
