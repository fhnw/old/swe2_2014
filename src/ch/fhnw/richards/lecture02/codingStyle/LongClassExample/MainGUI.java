package ch.fhnw.richards.lecture02.codingStyle.LongClassExample;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

import ch.fhnw.richards.lecture02.codingStyle.LongClassExample.refClasses.*;

/**
 * Erstellt die Hauptansicht der Applikation
 * @author ***anonymized***
 *
 */
public class MainGUI{

	
	
	/**
	 * Main Method - launch application  --TestMetode zum starten der HAuptansicht
	 *  @author ***anonymized***
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frameMainGUI.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	
	
	
	//********************************************************************************************************************************************
	//Erstellen der Objekte 
	
	// erstellen des Frames
	public JFrame frameMainGUI;

	//Erstellen der TextAreas
	JTextArea textAreaNewDossSituQuest = new JTextArea();
	JTextArea textAreaNewDossPerspQuest = new JTextArea();
	JTextArea textAreaNewDossNucQuest = new JTextArea();
	JTextArea textAreaNewDossInverseQuest = new JTextArea();
	JTextArea textAreaNewDossReverseQuest = new JTextArea();
	JTextArea textAreaNewDossFuxiQuest = new JTextArea();
	
	JTextArea textAreaNewDossSituUserComment = new JTextArea();
	JTextArea textAreaNewDossPerspComment = new JTextArea();
	JTextArea textAreaNewDossNucComment = new JTextArea();
	JTextArea textAreaNewDossInverseUserComment = new JTextArea();
	JTextArea textAreaNewDossReverseComment = new JTextArea();
	JTextArea textAreaNewDossFuxiComment = new JTextArea();
	JTextArea textAreaNewDossLineComment = new JTextArea();
	
	JTextArea textAreaNewDossSituJudgement = new JTextArea();
	JTextArea textAreaNewDossPerspJudgement = new JTextArea();
	JTextArea textAreaNewDossNucJudgement = new JTextArea();
	JTextArea textAreaNewDossInverseJudgement = new JTextArea();
	JTextArea textAreaNewDossReverseJudgement = new JTextArea();
	JTextArea textAreaNewDossFuxiJudgement = new JTextArea();
	JTextArea textAreaNewDossLineSynth = new JTextArea();

	//Erstellen der Comboboxen
	static JComboBox comboBoxNewDossSituCategory = new JComboBox();
	
	JComboBox comboBoxNewDossSitu6 = new JComboBox();
	JComboBox comboBoxNewDossSitu5 = new JComboBox();
	JComboBox comboBoxNewDossSitu4 = new JComboBox();
	JComboBox comboBoxNewDossSitu3 = new JComboBox();
	JComboBox comboBoxNewDossSitu1 = new JComboBox();
	JComboBox comboBoxNewDossSitu2 = new JComboBox();

	//Erstellen der Panels zur Kommentierung der Hexagrammzeilen
	//LineComment für SituationsHexagramm
	JLabel labelNewDossSituLineComLineNr = new JLabel("line NR");
	JPanel panelLineCommentSituLine1 =  new JPanel();
	JPanel panelLineCommentSituLine2 =  new JPanel();
	JPanel panelLineCommentSituLine3 =  new JPanel();
	JPanel panelLineCommentSituLine4 =  new JPanel();
	JPanel panelLineCommentSituLine5 =  new JPanel();
	JPanel panelLineCommentSituLine6 =  new JPanel();
	
	JLabel labelNewDossPerspLineComLineNr = new JLabel("line NR");
	JPanel panelLineCommentPerspLine1 = new JPanel();
	JPanel panelLineCommentPerspLine2 = new JPanel();
	JPanel panelLineCommentPerspLine3 = new JPanel();
	JPanel panelLineCommentPerspLine4 = new JPanel();
	JPanel panelLineCommentPerspLine5 = new JPanel();
	JPanel panelLineCommentPerspLine6 = new JPanel();
	
	JLabel labelNewDossNucLineComLineNr = new JLabel("line NR");
	JPanel panelLineCommentNucLine1 = new JPanel();
	JPanel panelLineCommentNucLine2 = new JPanel();
	JPanel panelLineCommentNucLine3 = new JPanel();
	JPanel panelLineCommentNucLine4 = new JPanel();
	JPanel panelLineCommentNucLine5 = new JPanel();
	JPanel panelLineCommentNucLine6 = new JPanel();
	
	JLabel labelNewDossInvLineComLineNr = new JLabel("line NR");
	JPanel panelLineCommentInvLine1 = new JPanel();
	JPanel panelLineCommentInvLine2 = new JPanel();
	JPanel panelLineCommentInvLine3 = new JPanel();
	JPanel panelLineCommentInvLine4 = new JPanel();
	JPanel panelLineCommentInvLine5 = new JPanel();
	JPanel panelLineCommentInvLine6 = new JPanel();
	
	JLabel labelNewDossRevLineComLineNr = new JLabel("line NR");
	JPanel panelLineCommentRevLine1 = new JPanel();
	JPanel panelLineCommentRevLine2 = new JPanel();
	JPanel panelLineCommentRevLine3 = new JPanel();
	JPanel panelLineCommentRevLine4 = new JPanel();
	JPanel panelLineCommentRevLine5 = new JPanel();
	JPanel panelLineCommentRevLine6 = new JPanel();
	
	JLabel labelNewDossFuxiLineComLineNr = new JLabel("line NR");
	JPanel panelLineCommentFuxiLine1 = new JPanel();
	JPanel panelLineCommentFuxiLine2 = new JPanel();
	JPanel panelLineCommentFuxiLine3 = new JPanel();
	JPanel panelLineCommentFuxiLine4 = new JPanel();
	JPanel panelLineCommentFuxiLine5 = new JPanel();
	JPanel panelLineCommentFuxiLine6 = new JPanel();
	
	
	
	// erstellen der Buttons zur visualisierung der Hexagramme
	//Situation	
	JButton buttonNewDossSituLineFull6 = new JButton();
	JButton buttonNewDossSituLineFull5 = new JButton();
	JButton buttonNewDossSituLineFull4 = new JButton();
	JButton buttonNewDossSituLineFull3 = new JButton();
	JButton buttonNewDossSituLineFull2 = new JButton();
	JButton buttonNewDossSituLineFull1 = new JButton();
	JButton buttonNewDossSituLineHalf6 = new JButton();
	JButton buttonNewDossSituLineHalf5 = new JButton();
	JButton buttonNewDossSituLineHalf4 = new JButton();
	JButton buttonNewDossSituLineHalf3 = new JButton();
	JButton buttonNewDossSituLineHalf2 = new JButton();
	JButton buttonNewDossSituLineHalf1 = new JButton();
	JButton buttonNewDossSituLineCross6 = new JButton();
	JButton buttonNewDossSituLineCross5 = new JButton();
	JButton buttonNewDossSituLineCross4 = new JButton();
	JButton buttonNewDossSituLineCross3 = new JButton();
	JButton buttonNewDossSituLineCross2 = new JButton();
	JButton buttonNewDossSituLineCross1 = new JButton();
	JButton buttonNewDossSituLineCircle6 = new JButton();
	JButton buttonNewDossSituLineCircle5 = new JButton();
	JButton buttonNewDossSituLineCircle4 = new JButton();
	JButton buttonNewDossSituLineCircle3 = new JButton();
	JButton buttonNewDossSituLineCircle2 = new JButton();
	JButton buttonNewDossSituLineCircle1 = new JButton();
	//Perspektive
	JButton buttonNewDossPersLineFull6 = new JButton();
	JButton buttonNewDossPersLineFull5 = new JButton();
	JButton buttonNewDossPersLineFull4 = new JButton();
	JButton buttonNewDossPersLineFull3 = new JButton();
	JButton buttonNewDossPersLineFull2 = new JButton();
	JButton buttonNewDossPersLineFull1 = new JButton();
	JButton buttonNewDossPersLineHalf6 = new JButton();
	JButton buttonNewDossPersLineHalf5 = new JButton();
	JButton buttonNewDossPersLineHalf4 = new JButton();
	JButton buttonNewDossPersLineHalf3 = new JButton();
	JButton buttonNewDossPersLineHalf2 = new JButton();
	JButton buttonNewDossPersLineHalf1 = new JButton();
	//Nuclear
	JButton buttonNewDossNucLineFull6 = new JButton();
	JButton buttonNewDossNucLineFull5 = new JButton();
	JButton buttonNewDossNucLineFull4 = new JButton();
	JButton buttonNewDossNucLineFull3 = new JButton();
	JButton buttonNewDossNucLineFull2 = new JButton();
	JButton buttonNewDossNucLineFull1 = new JButton();
	JButton buttonNewDossNucLineHalf6 = new JButton();
	JButton buttonNewDossNucLineHalf5 = new JButton();
	JButton buttonNewDossNucLineHalf4 = new JButton();
	JButton buttonNewDossNucLineHalf3 = new JButton();
	JButton buttonNewDossNucLineHalf2 = new JButton();
	JButton buttonNewDossNucLineHalf1 = new JButton();
	//Inverse
	JButton buttonNewDossInverLineFull6 = new JButton();
	JButton buttonNewDossInverLineFull5 = new JButton();
	JButton buttonNewDossInverLineFull4 = new JButton();
	JButton buttonNewDossInverLineFull3 = new JButton();
	JButton buttonNewDossInverLineFull2 = new JButton();
	JButton buttonNewDossInverLineFull1 = new JButton();
	JButton buttonNewDossInverLineHalf6 = new JButton();
	JButton buttonNewDossInverLineHalf5 = new JButton();
	JButton buttonNewDossInverLineHalf4 = new JButton();
	JButton buttonNewDossInverLineHalf3 = new JButton();
	JButton buttonNewDossInverLineHalf2 = new JButton();
	JButton buttonNewDossInverLineHalf1 = new JButton();
	//Reverse
	JButton buttonNewDossRevLineFull6 = new JButton();
	JButton buttonNewDossRevLineFull5 = new JButton();
	JButton buttonNewDossRevLineFull4 = new JButton();
	JButton buttonNewDossRevLineFull3 = new JButton();
	JButton buttonNewDossRevLineFull2 = new JButton();
	JButton buttonNewDossRevLineFull1 = new JButton();
	JButton buttonNewDossRevLineHalf6 = new JButton();
	JButton buttonNewDossRevLineHalf5 = new JButton();
	JButton buttonNewDossRevLineHalf4 = new JButton();
	JButton buttonNewDossRevLineHalf3 = new JButton();
	JButton buttonNewDossRevLineHalf2 = new JButton();
	JButton buttonNewDossRevLineHalf1 = new JButton();
	//Fuxi
	JButton buttonNewDossFuxiLineFull6 = new JButton();
	JButton buttonNewDossFuxiLineFull5 = new JButton();
	JButton buttonNewDossFuxiLineFull4 = new JButton();
	JButton buttonNewDossFuxiLineFull3 = new JButton();
	JButton buttonNewDossFuxiLineFull2 = new JButton();
	JButton buttonNewDossFuxiLineFull1 = new JButton();
	JButton buttonNewDossFuxiLineHalf6 = new JButton();
	JButton buttonNewDossFuxiLineHalf5 = new JButton();
	JButton buttonNewDossFuxiLineHalf4 = new JButton();
	JButton buttonNewDossFuxiLineHalf3 = new JButton();
	JButton buttonNewDossFuxiLineHalf2 = new JButton();
	JButton buttonNewDossFuxiLineHalf1 = new JButton();
	//LinienHexagramme Linke Seite
	JButton buttonNewDossLineLineFullLeft6 = new JButton();
	JButton buttonNewDossLineLineFullLeft5 = new JButton();
	JButton buttonNewDossLineLineFullLeft4 = new JButton();
	JButton buttonNewDossLineLineFullLeft3 = new JButton();
	JButton buttonNewDossLineLineFullLeft2 = new JButton();
	JButton buttonNewDossLineLineFullLeft1 = new JButton();
	JButton buttonNewDossLineLineHalfLeft6 = new JButton();
	JButton buttonNewDossLineLineHalfLeft5 = new JButton();
	JButton buttonNewDossLineLineHalfLeft4 = new JButton();
	JButton buttonNewDossLineLineHalfLeft3 = new JButton();
	JButton buttonNewDossLineLineHalfLeft2 = new JButton();
	JButton buttonNewDossLineLineHalfLeft1 = new JButton();
	JButton buttonNewDossLineLineCrossLeft6 = new JButton();
	JButton buttonNewDossLineLineCrossLeft5 = new JButton();
	JButton buttonNewDossLineLineCrossLeft4 = new JButton();
	JButton buttonNewDossLineLineCrossLeft3 = new JButton();
	JButton buttonNewDossLineLineCrossLeft2 = new JButton();
	JButton buttonNewDossLineLineCrossLeft1 = new JButton();
	JButton buttonNewDossLineLineCircleLeft6 = new JButton();
	JButton buttonNewDossLineLineCircleLeft5 = new JButton();
	JButton buttonNewDossLineLineCircleLeft4 = new JButton();
	JButton buttonNewDossLineLineCircleLeft3 = new JButton();
	JButton buttonNewDossLineLineCircleLeft2 = new JButton();
	JButton buttonNewDossLineLineCircleLeft1 = new JButton();
	//LinienHexagramme Rechte Seite
	JButton buttonNewDossLineLineFullRight6 = new JButton();
	JButton buttonNewDossLineLineFullRight5 = new JButton();
	JButton buttonNewDossLineLineFullRight4 = new JButton();
	JButton buttonNewDossLineLineFullRight3 = new JButton();
	JButton buttonNewDossLineLineFullRight2 = new JButton();
	JButton buttonNewDossLineLineFullRight1 = new JButton();
	JButton buttonNewDossLineLineHalfRight6 = new JButton();
	JButton buttonNewDossLineLineHalfRight5 = new JButton();
	JButton buttonNewDossLineLineHalfRight4 = new JButton();
	JButton buttonNewDossLineLineHalfRight3 = new JButton();
	JButton buttonNewDossLineLineHalfRight2 = new JButton();
	JButton buttonNewDossLineLineHalfRight1 = new JButton();
	
	
	JTextField textFieldEditProfFirstName = new JTextField();	
	JTextField textFieldEditProfLastName = new JTextField();
	JTextField textFieldEditProfEmail = new JTextField();
	JPasswordField passwordFieldEditProfPassword = new JPasswordField();


	JLabel labelShowDossDataRight;
	
	//LinkDossier
	JList listLinkDossDoss = new JList();
	JList listLinkDossRelation = new JList();
	//UnlinkDossier
	JList listUnlinkDossDataRight = new JList();
	JList listUnlinkDossDataLeft = new JList();
	//ShowDossier
	JList listShowDossDataRight = new JList();
	
	JButton buttonLinkDossLinkTogether = new JButton("Link together");

	Export export = new Export();



	
	//********************************************************************************************************************************************
	//Initialisierung der Lokalen Speicherbereiche
	//********************************************************************************************************************************************

	
	//String zum übermitteln des Identifiers des SituationsHexagrammes
	String hexagrammID = "";
	String[] visualizeLineHexa;
	
	static String situationID = "111111";
	static String perspectiveID = "111111";
	//Strings zum Epmfangen der Identifier der Mutierten Hexagramme
	static String recieveNuc = "000000";
	static String recieveInv = "000000";
	static String recieveRev = "000000";
	static String recieveFuxi = "000000";
	// ArrayList zum speichern der Category Titel
	static ArrayList<String> categoryTitleRepository = new ArrayList<String>();
	//ArrayList zum speichern der Dossier Fragen;
	static ArrayList dossierQuestionRepository = new ArrayList();
	//ArrayList zum spechern der DossierLinks
	static ArrayList dossierLinkRepository = new ArrayList();
	
	
	
									//Testwerte
									static String a = "a";
									static String b = "b";
									static String c = "c";

	
	
	
	//Int zum zwischenspeichern der UserID

			//static String messageType = "NEWDOSSIER";
	

	
//********************************************************************************************************************************************
//Erstellen und Starten des Konstruktores "MainGUI"
//********************************************************************************************************************************************

	
	/**
	 * Initialisierung MainGUI
	 * @author ***anonymized***
	 */
	 public MainGUI() {
		initializeMainGUI();
	}

	/**
	 * Inizialisierung der Objekte vom MainGUI
	 * @author ***anonymized***
	 */
	private void initializeMainGUI() {
//		//********************************************************************************************************************************************
//		//Füllen der Lokalen SpeicherVariabeln
//		//********************************************************************************************************************************************
//
				//		//Testwerte		
				//		categoryTitleRepository.add(a);
				//		categoryTitleRepository.add(b);
				//		categoryTitleRepository.add(c);
				//		
				//		listShowDossDataRight.setListData(categoryTitleRepository.toArray());
				//		listLinkDossDoss.setListData(categoryTitleRepository.toArray());
				//		listLinkDossRelation.setListData(categoryTitleRepository.toArray());
				//		listUnlinkDossDataLeft.setListData(categoryTitleRepository.toArray());
				//		listUnlinkDossDataRight.setListData(categoryTitleRepository.toArray());
		
		
//		LocalRepository.createCategoryLocalRepository(LocalRepository.currentUser);
		
//		//Füllen der KategorienTitel	
//		for(int i = 0; i < LocalRepository.userCategories.length; i++){
//			categoryTitleRepository.add(LocalRepository.userCategories[i].getCategoryTitle());
//		}
//			//Hinzufügen der Titel in die ComboBox	Q
//		comboBoxNewDossSituCategory.setModel(new DefaultComboBoxModel(categoryTitleRepository.toArray()));
//
//	//Fïüllen des Lokalen Speichers mit den DossierFragen
//		for(int i = 0; i < LocalRepository.dossiers.length; i++){
//		dossierQuestionRepository.add(LocalRepository.dossiers[i].getQuestion());
//		}
//		//Hinzufïügen der Fragestellungen zu JList in LinkDossier
//			listLinkDossDoss.setListData(dossierQuestionRepository.toArray());	
//			listLinkDossRelation.setListData(dossierQuestionRepository.toArray());
//			
//			//Hinzufügen der Fragestellungen zu JList in ShowDossier
//			listShowDossDataRight.setListData(dossierQuestionRepository.toArray());
//		
//			
//			//Füllen der Linken JList  von UnlinkDossier mit den Dossier Fragen
//			listUnlinkDossDataLeft.setListData(dossierQuestionRepository.toArray());
//
//		
//		
//		
//		//Füllen des Lokalen speichers mit den DossierLinks
////		listUnlinkDossDataLeft.add(LocalRepository.dossierlinks[1].get)
//		
//		
//		
//
//		
//		//Füllen der EditProfile Felder
//		textFieldEditProfFirstName.setText(LocalRepository.currentUser.getFirstName());	
//		textFieldEditProfLastName.setText(LocalRepository.currentUser.getLastName());	
//		textFieldEditProfEmail.setText(LocalRepository.currentUser.geteMail());	
//		
		
		//********************************************************************************************************************************************

		
		//Initialisierung Frame
		frameMainGUI = new JFrame();
		frameMainGUI.setResizable(false);
		frameMainGUI.setBounds(130, 20, 1350, 800);
		frameMainGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Intialisierung und hinzufügen der TabbedPane
		JTabbedPane tabbedPaneTop = new JTabbedPane(JTabbedPane.TOP);
		tabbedPaneTop.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		tabbedPaneTop.setFont(new Font("Tahoma", Font.BOLD, 15));
		JTabbedPane tabbedPaneLeft = new JTabbedPane(JTabbedPane.LEFT);
		tabbedPaneLeft.setFont(new Font("Tahoma", Font.BOLD, 13));
		tabbedPaneTop.addTab("new dossier", null, tabbedPaneLeft, null);
		frameMainGUI.getContentPane().add(tabbedPaneTop, BorderLayout.CENTER);
		
		//********************************************************************************************************************************************

		


		//Initialisierung der Panels		
		JPanel panelSituation = new JPanel();
		panelSituation.setBackground(UIManager.getColor("Panel.background"));
		tabbedPaneLeft.addTab("situation", null, panelSituation, null);
		panelSituation.setLayout(null);
		
			JPanel panelPerspective = new JPanel();
			tabbedPaneLeft.addTab("perspective", null, panelPerspective, null);
			panelPerspective.setLayout(null);
			
			JPanel panelNuclear = new JPanel();
			tabbedPaneLeft.addTab("nuclear", null, panelNuclear, null);
			panelNuclear.setLayout(null);
			
			JPanel panelInverse = new JPanel();
			tabbedPaneLeft.addTab("inverse", null, panelInverse, null);
			panelInverse.setLayout(null);
			
			JPanel panelReverse = new JPanel();
			tabbedPaneLeft.addTab("reverse", null, panelReverse, null);
			panelReverse.setLayout(null);
			
			JPanel panelFuxi = new JPanel();
			tabbedPaneLeft.addTab("fuxi", null, panelFuxi, null);
			panelFuxi.setLayout(null);
			
			JPanel panelLine = new JPanel();
			tabbedPaneLeft.addTab("line", null, panelLine, null);
			panelLine.setLayout(null);
		
			
		JPanel panelLinkDossier = new JPanel();
		tabbedPaneTop.addTab("Link dossier", null, panelLinkDossier, null);
		panelLinkDossier.setLayout(null);
		
		JPanel panelUnlinkDossier = new JPanel();
		tabbedPaneTop.addTab("Show link/unlink", null, panelUnlinkDossier, null);
		panelUnlinkDossier.setLayout(null);
		
			
				
				
		//********************************************************************************************************************************************
		//		
		//Initialisierung Unlink Dossier
		//
		
		JLabel labelUnlinkDossUnlinkDoss = new JLabel("Unlink Dossier");
		labelUnlinkDossUnlinkDoss.setForeground(Color.BLACK);
		labelUnlinkDossUnlinkDoss.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelUnlinkDossUnlinkDoss.setBounds(766, 23, 159, 21);
		panelUnlinkDossier.add(labelUnlinkDossUnlinkDoss);
		
		JLabel labelUnlinkDossDoss = new JLabel("Dossier");
		labelUnlinkDossDoss.setVerticalAlignment(SwingConstants.TOP);
		labelUnlinkDossDoss.setHorizontalAlignment(SwingConstants.LEFT);
		labelUnlinkDossDoss.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelUnlinkDossDoss.setBounds(72, 27, 76, 23);
		panelUnlinkDossier.add(labelUnlinkDossDoss);
		
		JScrollPane scrollPaneUnlinkDoss = new JScrollPane();
		scrollPaneUnlinkDoss.setBounds(72, 61, 825, 518);
		panelUnlinkDossier.add(scrollPaneUnlinkDoss);
		
		JPanel panelUnlinkDossForScrollpane = new JPanel();
		scrollPaneUnlinkDoss.setViewportView(panelUnlinkDossForScrollpane);
		panelUnlinkDossForScrollpane.setLayout(null);
		
		listUnlinkDossDataRight.setBounds(411, 0, 412, 516);
		panelUnlinkDossForScrollpane.add(listUnlinkDossDataRight);
		
		listUnlinkDossDataLeft.setBounds(0, 0, 412, 516);
		panelUnlinkDossForScrollpane.add(listUnlinkDossDataLeft);
		
		JButton buttonUnlinkDossUnlink = new JButton("Unlink");
		buttonUnlinkDossUnlink.setFont(new Font("Tahoma", Font.BOLD, 13));
		buttonUnlinkDossUnlink.setBounds(72, 655, 129, 23);
		panelUnlinkDossier.add(buttonUnlinkDossUnlink);
		
		JPanel panelShowDossier = new JPanel();
		tabbedPaneTop.addTab("show dossier", null, panelShowDossier, null);
		panelShowDossier.setLayout(null);
		
		JPanel panelEditProfile = new JPanel();
		tabbedPaneTop.addTab("edit profile", null, panelEditProfile, null);
		panelEditProfile.setLayout(null);
		
		
		//********************************************************************************************************************************************
		//
		//Erstellen ImageIcon Unterbrochene Linie, linie mit Kreuz, linie mit Kreis
		ImageIcon lineUnterbrochen = new ImageIcon(getClass().getResource("lineUnterbrochen.png"));
		ImageIcon lineKreuz = new ImageIcon(getClass().getResource("lineKreuz.png"));
		ImageIcon lineKreis = new ImageIcon(getClass().getResource("lineKreis.png"));
		ImageIcon lineFull = new ImageIcon(getClass().getResource("lineFull.png"));
	
		//********************************************************************************************************************************************
		//		
		//Erstellen der HexagrammVisualisierungButtons für alle Panels
		
			// Buttons fürr Hexagram Situation
		
				//-Durchzogene Linie
				buttonNewDossSituLineFull6.setIcon(lineFull);
				panelSituation.add(buttonNewDossSituLineFull6);
				
				buttonNewDossSituLineFull5.setIcon(lineFull);
				panelSituation.add(buttonNewDossSituLineFull5);
				
				buttonNewDossSituLineFull4.setIcon(lineFull);
				panelSituation.add(buttonNewDossSituLineFull4);
				
				buttonNewDossSituLineFull3.setIcon(lineFull);
				panelSituation.add(buttonNewDossSituLineFull3);
				
				buttonNewDossSituLineFull2.setIcon(lineFull);
				panelSituation.add(buttonNewDossSituLineFull2);		
				
				buttonNewDossSituLineFull1.setIcon(lineFull);
				panelSituation.add(buttonNewDossSituLineFull1);	

				//Unterbrochene Linie
				buttonNewDossSituLineHalf6.setIcon(lineUnterbrochen);
				panelSituation.add(buttonNewDossSituLineHalf6);
				
				buttonNewDossSituLineHalf5.setIcon(lineUnterbrochen);
				panelSituation.add(buttonNewDossSituLineHalf5);
				
				buttonNewDossSituLineHalf4.setIcon(lineUnterbrochen);
				panelSituation.add(buttonNewDossSituLineHalf4);
		
				buttonNewDossSituLineHalf3.setIcon(lineUnterbrochen);
				panelSituation.add(buttonNewDossSituLineHalf3);
				
				buttonNewDossSituLineHalf2.setIcon(lineUnterbrochen);
				panelSituation.add(buttonNewDossSituLineHalf2);
				
				buttonNewDossSituLineHalf1.setIcon(lineUnterbrochen);
				panelSituation.add(buttonNewDossSituLineHalf1);		
				
				//Linie mit Kreuz
				buttonNewDossSituLineCross6.setIcon(lineKreuz);
				panelSituation.add(buttonNewDossSituLineCross6);
				
				buttonNewDossSituLineCross5.setIcon(lineKreuz);
				panelSituation.add(buttonNewDossSituLineCross5);
				
				buttonNewDossSituLineCross4.setIcon(lineKreuz);
				panelSituation.add(buttonNewDossSituLineCross4);
				
				buttonNewDossSituLineCross3.setIcon(lineKreuz);
				panelSituation.add(buttonNewDossSituLineCross3);
				
				buttonNewDossSituLineCross2.setIcon(lineKreuz);
				panelSituation.add(buttonNewDossSituLineCross2);
				
				buttonNewDossSituLineCross1.setIcon(lineKreuz);
				panelSituation.add(buttonNewDossSituLineCross1);
				
				//Linie mit Kreis
		
				buttonNewDossSituLineCircle6.setIcon(lineKreis);
				panelSituation.add(buttonNewDossSituLineCircle6);
				
				buttonNewDossSituLineCircle5.setIcon(lineKreis);
				panelSituation.add(buttonNewDossSituLineCircle5);
				
				buttonNewDossSituLineCircle4.setIcon(lineKreis);
				panelSituation.add(buttonNewDossSituLineCircle4);
				
				buttonNewDossSituLineCircle3.setIcon(lineKreis);
				panelSituation.add(buttonNewDossSituLineCircle3);
				
				buttonNewDossSituLineCircle2.setIcon(lineKreis);
				panelSituation.add(buttonNewDossSituLineCircle2);
				
				buttonNewDossSituLineCircle1.setIcon(lineKreis);
				panelSituation.add(buttonNewDossSituLineCircle1);


				
			//Buttons für Hexagram Perspective
				//-Durchzogene Linie
				
				buttonNewDossPersLineFull6.setIcon(lineFull);
				panelPerspective.add(buttonNewDossPersLineFull6);
				
				buttonNewDossPersLineFull5.setIcon(lineFull);
				panelPerspective.add(buttonNewDossPersLineFull5);
				
				buttonNewDossPersLineFull4.setIcon(lineFull);
				panelPerspective.add(buttonNewDossPersLineFull4);
				
				buttonNewDossPersLineFull3.setIcon(lineFull);
				panelPerspective.add(buttonNewDossPersLineFull3);
				
				buttonNewDossPersLineFull2.setIcon(lineFull);
				panelPerspective.add(buttonNewDossPersLineFull2);		
				
				buttonNewDossPersLineFull1.setIcon(lineFull);
				panelPerspective.add(buttonNewDossPersLineFull1);
	
				//-Unterbrochene Linie		
				
				buttonNewDossPersLineHalf6.setIcon(lineUnterbrochen);
				panelPerspective.add(buttonNewDossPersLineHalf6);
				
				buttonNewDossPersLineHalf5.setIcon(lineUnterbrochen);
				panelPerspective.add(buttonNewDossPersLineHalf5);
				
				buttonNewDossPersLineHalf4.setIcon(lineUnterbrochen);
				panelPerspective.add(buttonNewDossPersLineHalf4);
		
				buttonNewDossPersLineHalf3.setIcon(lineUnterbrochen);
				panelPerspective.add(buttonNewDossPersLineHalf3);
				
				buttonNewDossPersLineHalf2.setIcon(lineUnterbrochen);
				panelPerspective.add(buttonNewDossPersLineHalf2);
				
				buttonNewDossPersLineHalf1.setIcon(lineUnterbrochen);
				panelPerspective.add(buttonNewDossPersLineHalf1);


				
			//Buttons für Hexagram Perspective
			 	//-Durchzogene Linie
					
				buttonNewDossNucLineFull6.setIcon(lineFull);
				panelNuclear.add(buttonNewDossNucLineFull6);
				
				buttonNewDossNucLineFull5.setIcon(lineFull);
				panelNuclear.add(buttonNewDossNucLineFull5);
					
				buttonNewDossNucLineFull4.setIcon(lineFull);
				panelNuclear.add(buttonNewDossNucLineFull4);
					
				buttonNewDossNucLineFull3.setIcon(lineFull);
				panelNuclear.add(buttonNewDossNucLineFull3);
					
				buttonNewDossNucLineFull2.setIcon(lineFull);
				panelNuclear.add(buttonNewDossNucLineFull2);		
					
				buttonNewDossNucLineFull1.setIcon(lineFull);
				panelNuclear.add(buttonNewDossNucLineFull1);
								
				//-Unterbrochene Linie		
					
				buttonNewDossNucLineHalf6.setIcon(lineUnterbrochen);
				panelNuclear.add(buttonNewDossNucLineHalf6);
					
				buttonNewDossNucLineHalf5.setIcon(lineUnterbrochen);
				panelNuclear.add(buttonNewDossNucLineHalf5);
					
				buttonNewDossNucLineHalf4.setIcon(lineUnterbrochen);
				panelNuclear.add(buttonNewDossNucLineHalf4);
			
				buttonNewDossNucLineHalf3.setIcon(lineUnterbrochen);
				panelNuclear.add(buttonNewDossNucLineHalf3);
				
				buttonNewDossNucLineHalf2.setIcon(lineUnterbrochen);
				panelNuclear.add(buttonNewDossNucLineHalf2);
				
				buttonNewDossNucLineHalf1.setIcon(lineUnterbrochen);
				panelNuclear.add(buttonNewDossNucLineHalf1);
				

			//Buttons für Hexagram Inverse		
				//-Durchzogene Linie
				buttonNewDossInverLineFull6.setIcon(lineFull);
				panelInverse.add(buttonNewDossInverLineFull6);
				
				buttonNewDossInverLineFull5.setIcon(lineFull);
				panelInverse.add(buttonNewDossInverLineFull5);
				
				buttonNewDossInverLineFull4.setIcon(lineFull);
				panelInverse.add(buttonNewDossInverLineFull4);
				
				buttonNewDossInverLineFull3.setIcon(lineFull);
				panelInverse.add(buttonNewDossInverLineFull3);
				
				buttonNewDossInverLineFull2.setIcon(lineFull);
				panelInverse.add(buttonNewDossInverLineFull2);		
				
				buttonNewDossInverLineFull1.setIcon(lineFull);
				panelInverse.add(buttonNewDossInverLineFull1);
							
				//-Unterbrochene Linie		
				
				buttonNewDossInverLineHalf6.setIcon(lineUnterbrochen);
				panelInverse.add(buttonNewDossInverLineHalf6);
				
				buttonNewDossInverLineHalf5.setIcon(lineUnterbrochen);
				panelInverse.add(buttonNewDossInverLineHalf5);
				
				buttonNewDossInverLineHalf4.setIcon(lineUnterbrochen);
				panelInverse.add(buttonNewDossInverLineHalf4);
		
				buttonNewDossInverLineHalf3.setIcon(lineUnterbrochen);
				panelInverse.add(buttonNewDossInverLineHalf3);
				
				buttonNewDossInverLineHalf2.setIcon(lineUnterbrochen);
				panelInverse.add(buttonNewDossInverLineHalf2);
				
				buttonNewDossInverLineHalf1.setIcon(lineUnterbrochen);
				panelInverse.add(buttonNewDossInverLineHalf1);
				
				
			//Buttons für Hexagram Reverse
				//-Durchzogene Linie
				buttonNewDossRevLineFull6.setIcon(lineFull);
				panelReverse.add(buttonNewDossRevLineFull6);
				
				buttonNewDossRevLineFull5.setIcon(lineFull);
				panelReverse.add(buttonNewDossRevLineFull5);
				
				buttonNewDossRevLineFull4.setIcon(lineFull);
				panelReverse.add(buttonNewDossRevLineFull4);
				
				buttonNewDossRevLineFull3.setIcon(lineFull);
				panelReverse.add(buttonNewDossRevLineFull3);
				
				buttonNewDossRevLineFull2.setIcon(lineFull);
				panelReverse.add(buttonNewDossRevLineFull2);		
				
				buttonNewDossRevLineFull1.setIcon(lineFull);
				panelReverse.add(buttonNewDossRevLineFull1);
							
				//-Unterbrochene Linie				
				buttonNewDossRevLineHalf6.setIcon(lineUnterbrochen);
				panelReverse.add(buttonNewDossRevLineHalf6);
				
				buttonNewDossRevLineHalf5.setIcon(lineUnterbrochen);
				panelReverse.add(buttonNewDossRevLineHalf5);
				
				buttonNewDossRevLineHalf4.setIcon(lineUnterbrochen);
				panelReverse.add(buttonNewDossRevLineHalf4);
		
				buttonNewDossRevLineHalf3.setIcon(lineUnterbrochen);
				panelReverse.add(buttonNewDossRevLineHalf3);
				
				buttonNewDossRevLineHalf2.setIcon(lineUnterbrochen);
				panelReverse.add(buttonNewDossRevLineHalf2);
				
				buttonNewDossRevLineHalf1.setIcon(lineUnterbrochen);
				panelReverse.add(buttonNewDossRevLineHalf1);
			
		
			//Buttons für Hexagram Fuxi
				//-Durchzogene Linie				
				buttonNewDossFuxiLineFull6.setIcon(lineFull);
				panelFuxi.add(buttonNewDossFuxiLineFull6);
				
				buttonNewDossFuxiLineFull5.setIcon(lineFull);
				panelFuxi.add(buttonNewDossFuxiLineFull5);
				
				buttonNewDossFuxiLineFull4.setIcon(lineFull);
				panelFuxi.add(buttonNewDossFuxiLineFull4);
				
				buttonNewDossFuxiLineFull3.setIcon(lineFull);
				panelFuxi.add(buttonNewDossFuxiLineFull3);
				
				buttonNewDossFuxiLineFull2.setIcon(lineFull);
				panelFuxi.add(buttonNewDossFuxiLineFull2);		
				
				buttonNewDossFuxiLineFull1.setIcon(lineFull);
				panelFuxi.add(buttonNewDossFuxiLineFull1);
				
				//-Unterbrochene Linie		
				buttonNewDossFuxiLineHalf6.setIcon(lineUnterbrochen);
				panelFuxi.add(buttonNewDossFuxiLineHalf6);
				
				buttonNewDossFuxiLineHalf5.setIcon(lineUnterbrochen);
				panelFuxi.add(buttonNewDossFuxiLineHalf5);
				
				buttonNewDossFuxiLineHalf4.setIcon(lineUnterbrochen);
				panelFuxi.add(buttonNewDossFuxiLineHalf4);
		
				buttonNewDossFuxiLineHalf3.setIcon(lineUnterbrochen);
				panelFuxi.add(buttonNewDossFuxiLineHalf3);
				
				buttonNewDossFuxiLineHalf2.setIcon(lineUnterbrochen);
				panelFuxi.add(buttonNewDossFuxiLineHalf2);
				
				buttonNewDossFuxiLineHalf1.setIcon(lineUnterbrochen);
				panelFuxi.add(buttonNewDossFuxiLineHalf1);
					
			//Buttons für Hexagram Line
				//
				//Linkes Hexagramm -Durchzogene Linie

				
				buttonNewDossLineLineFullLeft6.setIcon(lineFull);					
				panelLine.add(buttonNewDossLineLineFullLeft6);
					
				buttonNewDossLineLineFullLeft5.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullLeft5);
					
				buttonNewDossLineLineFullLeft4.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullLeft4);
					
				buttonNewDossLineLineFullLeft3.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullLeft3);
					
				buttonNewDossLineLineFullLeft2.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullLeft2);		
					
				buttonNewDossLineLineFullLeft1.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullLeft1);
						
					
				//Linkes Hexagramm -Unterbrochene Linie		
				buttonNewDossLineLineHalfLeft6.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfLeft6);
					
				buttonNewDossLineLineHalfLeft5.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfLeft5);
					
				buttonNewDossLineLineHalfLeft4.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfLeft4);
			
				buttonNewDossLineLineHalfLeft3.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfLeft3);
					
				buttonNewDossLineLineHalfLeft2.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfLeft2);
				
				buttonNewDossLineLineHalfLeft1.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfLeft1);
				
				//Rechtes Hexagramm -Durchzogene Linie
				buttonNewDossLineLineFullRight6.setIcon(lineFull);					
				panelLine.add(buttonNewDossLineLineFullRight6);
					
				buttonNewDossLineLineFullRight5.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullRight5);
					
				buttonNewDossLineLineFullRight4.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullRight4);
					
				buttonNewDossLineLineFullRight3.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullRight3);
					
				buttonNewDossLineLineFullRight2.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullRight2);		
					
				buttonNewDossLineLineFullRight1.setIcon(lineFull);
				panelLine.add(buttonNewDossLineLineFullRight1);
				
				//Rechtes Hexagramm -Unterbrochene Linie	
				buttonNewDossLineLineHalfRight6.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfRight6);
					
				buttonNewDossLineLineHalfRight5.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfRight5);
					
				buttonNewDossLineLineHalfRight4.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfRight4);
			
				buttonNewDossLineLineHalfRight3.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfRight3);
					
				buttonNewDossLineLineHalfRight2.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfRight2);
				
				buttonNewDossLineLineHalfRight1.setIcon(lineUnterbrochen);
				panelLine.add(buttonNewDossLineLineHalfRight1);

			//********************************************************************************************************************************************
			//
			//Initalisierung NewDossier - Situation
			
				JLabel labelNewDossSituName = new JLabel(/*LocalRepository.currentUser.getFirstName()*/"Name");
			labelNewDossSituName.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituName.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituName.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituName.setBounds(56, 26, 76, 23);
			panelSituation.add(labelNewDossSituName);
		
			JLabel labelNewDossSituComment = new JLabel("User comment");
			labelNewDossSituComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituComment.setBounds(56, 167, 131, 23);
			panelSituation.add(labelNewDossSituComment);
		
			JLabel labelNewDossSituJudgement = new JLabel("Judgement");
			labelNewDossSituJudgement.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituJudgement.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituJudgement.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituJudgement.setBounds(56, 304, 97, 23);
			panelSituation.add(labelNewDossSituJudgement);
			
			JLabel labelNewDossSituImage = new JLabel("Image");
			labelNewDossSituImage.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituImage.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituImage.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituImage.setBounds(59, 441, 73, 23);
			panelSituation.add(labelNewDossSituImage);
			
			JLabel labelNewDossSituQuestion = new JLabel("Question");
			labelNewDossSituQuestion.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituQuestion.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituQuestion.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituQuestion.setBounds(56, 59, 76, 23);
			panelSituation.add(labelNewDossSituQuestion);
			
			//Definieren des DatumFormates
	        GregorianCalendar calender = new GregorianCalendar();
	        DateFormat calFormat = DateFormat.getDateInstance(DateFormat.SHORT);
			
			JLabel labelNewDossSituCategory = new JLabel("Category");
			labelNewDossSituCategory.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituCategory.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituCategory.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituCategory.setBounds(289, 27, 104, 23);
			panelSituation.add(labelNewDossSituCategory);
			
			
			
			comboBoxNewDossSituCategory.setFont(new Font("Tahoma", Font.PLAIN, 13));
			comboBoxNewDossSituCategory.setBounds(375, 26, 168, 20);
			panelSituation.add(comboBoxNewDossSituCategory);
			
			final JTextArea textAreaNewDossSituName = new JTextArea();
			textAreaNewDossSituName.setBounds(128, 23, 131, 22);
			panelSituation.add(textAreaNewDossSituName);
			
			JScrollPane scrollPaneNewDossSituQestion = new JScrollPane();
			scrollPaneNewDossSituQestion.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossSituQestion.setBounds(56, 84, 564, 72);
			panelSituation.add(scrollPaneNewDossSituQestion);
			
			textAreaNewDossSituQuest.setWrapStyleWord(true);
			textAreaNewDossSituQuest.setLineWrap(true);
			scrollPaneNewDossSituQestion.setViewportView(textAreaNewDossSituQuest);
			
			JScrollPane scrollPaneNewDossSituComment = new JScrollPane();
			scrollPaneNewDossSituComment.setBounds(56, 195, 564, 98);
			panelSituation.add(scrollPaneNewDossSituComment);
			
			textAreaNewDossSituUserComment.setWrapStyleWord(true);
			textAreaNewDossSituQuest.append(textAreaNewDossSituQuest.getText());
			textAreaNewDossSituUserComment.setLineWrap(true);
			scrollPaneNewDossSituComment.setViewportView(textAreaNewDossSituUserComment);
			
			JScrollPane scrollPaneNewDossSituSynth = new JScrollPane();
			scrollPaneNewDossSituSynth.setBounds(56, 338, 564, 92);
			panelSituation.add(scrollPaneNewDossSituSynth);
			
			textAreaNewDossSituJudgement.setWrapStyleWord(true);
			textAreaNewDossSituJudgement.setLineWrap(true);
			scrollPaneNewDossSituSynth.setViewportView(textAreaNewDossSituJudgement);
			
			JScrollPane scrollPaneNewDossSituReflec = new JScrollPane();
			scrollPaneNewDossSituReflec.setBounds(56, 466, 564, 167);
			panelSituation.add(scrollPaneNewDossSituReflec);

			final JTextArea textAreaNewDossSituImage = new JTextArea();
			textAreaNewDossSituImage.setWrapStyleWord(true);
			textAreaNewDossSituImage.setLineWrap(true);
			scrollPaneNewDossSituReflec.setViewportView(textAreaNewDossSituImage);

			
						
			//Erstellen der ComboBoxen NewDossSituation 
			comboBoxNewDossSitu6.setFont(new Font("Tahoma", Font.BOLD, 13));
			comboBoxNewDossSitu6.setModel(new DefaultComboBoxModel(new String[] {"", "6", "7", "8", "9"}));
			comboBoxNewDossSitu6.setBounds(632, 87, 40, 20);
			panelSituation.add(comboBoxNewDossSitu6);


			comboBoxNewDossSitu5.setFont(new Font("Tahoma", Font.BOLD, 13));
			comboBoxNewDossSitu5.setModel(new DefaultComboBoxModel(new String[] {"","6", "7", "8", "9"}));
			comboBoxNewDossSitu5.setBounds(632, 139, 40, 20);
			panelSituation.add(comboBoxNewDossSitu5);
	
			comboBoxNewDossSitu4.setFont(new Font("Tahoma", Font.BOLD, 13));
			comboBoxNewDossSitu4.setModel(new DefaultComboBoxModel(new String[] {"","6", "7", "8", "9"}));
			comboBoxNewDossSitu4.setBounds(632, 189, 40, 20);
			panelSituation.add(comboBoxNewDossSitu4);
			
			comboBoxNewDossSitu3.setFont(new Font("Tahoma", Font.BOLD, 13));
			comboBoxNewDossSitu3.setModel(new DefaultComboBoxModel(new String[] {"","6", "7", "8", "9"}));
			comboBoxNewDossSitu3.setBounds(632, 244, 40, 20);
			panelSituation.add(comboBoxNewDossSitu3);

			comboBoxNewDossSitu2.setFont(new Font("Tahoma", Font.BOLD, 13));
			comboBoxNewDossSitu2.setModel(new DefaultComboBoxModel(new String[] {"","6", "7", "8", "9"}));
			comboBoxNewDossSitu2.setBounds(632, 295, 40, 20);
			panelSituation.add(comboBoxNewDossSitu2);
			
			comboBoxNewDossSitu1.setFont(new Font("Tahoma", Font.BOLD, 13));
			comboBoxNewDossSitu1.setModel(new DefaultComboBoxModel(new String[] {"","6", "7", "8", "9"}));
			comboBoxNewDossSitu1.setBounds(632, 345, 40, 20);
			panelSituation.add(comboBoxNewDossSitu1);
			
			
			JButton buttonNewDossSituSave = new JButton("Save");
			buttonNewDossSituSave.setFont(new Font("Tahoma", Font.BOLD, 13));
			buttonNewDossSituSave.setBounds(56, 665, 108, 23);
			panelSituation.add(buttonNewDossSituSave);
			
	
			JButton buttonNewDossSituClear = new JButton("Clear");
			buttonNewDossSituClear.setFont(new Font("Tahoma", Font.BOLD, 13));
			buttonNewDossSituClear.setBounds(185, 665, 108, 23);
			panelSituation.add(buttonNewDossSituClear);
			
			//Setzen des Icons für editCategory
			ImageIcon editCategroy = new ImageIcon(getClass().getResource("Edit.png"));
			
			JButton buttonNewDossSituEditCate = new JButton();
			buttonNewDossSituEditCate.setIcon(editCategroy);
			buttonNewDossSituEditCate.setBounds(558, 24, 26, 23);
			panelSituation.add(buttonNewDossSituEditCate);		
			buttonNewDossSituEditCate.setToolTipText("Edit Category");

			
			JLabel labelNewDossSituHexagramSituHexa = new JLabel("Situation hexagram");
			labelNewDossSituHexagramSituHexa.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituHexagramSituHexa.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituHexagramSituHexa.setFont(new Font("Tahoma", Font.BOLD, 18));
			labelNewDossSituHexagramSituHexa.setBounds(651, 21, 202, 21);
			panelSituation.add(labelNewDossSituHexagramSituHexa);
			
			JLabel labelNewDossSituDate = new JLabel("Date:");
			labelNewDossSituDate.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituDate.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituDate.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituDate.setBounds(289, 59, 46, 14);
			panelSituation.add(labelNewDossSituDate);
			
			final JLabel labelNewDossSituDateData = new JLabel("13.05.13");
			labelNewDossSituDateData.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituDateData.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituDateData.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituDateData.setBounds(375, 59, 90, 14);
			panelSituation.add(labelNewDossSituDateData);
			
			labelNewDossSituLineComLineNr.setBounds(887, 26, 131, 25);
			panelSituation.add(labelNewDossSituLineComLineNr);
			labelNewDossSituLineComLineNr.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComLineNr.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComLineNr.setFont(new Font("Tahoma", Font.BOLD, 13));
			
			
			//Erstellen der KommentarSparte Line
			panelLineCommentSituLine1.setForeground(Color.BLACK);
			panelLineCommentSituLine1.setBounds(877, 44, 340, 400);
			panelSituation.add(panelLineCommentSituLine1);
			panelLineCommentSituLine1.setLayout(null);
			
			JLabel labelNewDossSituLineComDiscrLine1 = new JLabel("Line discription");
			labelNewDossSituLineComDiscrLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComDiscrLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComDiscrLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComDiscrLine1.setBounds(10, 11, 131, 25);
			panelLineCommentSituLine1.add(labelNewDossSituLineComDiscrLine1);
			
			JScrollPane scrollPaneNewDossSituLineComDiscrLine1 = new JScrollPane();
			scrollPaneNewDossSituLineComDiscrLine1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossSituLineComDiscrLine1.setBounds(10, 36, 320, 156);
			panelLineCommentSituLine1.add(scrollPaneNewDossSituLineComDiscrLine1);
			
			JTextArea textAreaNewDossSituLineComDiscrLine1 = new JTextArea();
			textAreaNewDossSituLineComDiscrLine1.setEditable(false);
			textAreaNewDossSituLineComDiscrLine1.setWrapStyleWord(true);
			textAreaNewDossSituLineComDiscrLine1.setLineWrap(true);
			scrollPaneNewDossSituLineComDiscrLine1.setViewportView(textAreaNewDossSituLineComDiscrLine1);
			
			JLabel labelNewDossSituLineComComLine1 = new JLabel("Line comment");
			labelNewDossSituLineComComLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComComLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComComLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComComLine1.setBounds(12, 209, 106, 25);
			panelLineCommentSituLine1.add(labelNewDossSituLineComComLine1);
			
			JScrollPane scrollPaneNewDossSituLineComComLine1 = new JScrollPane();
			scrollPaneNewDossSituLineComComLine1.setBounds(10, 234, 318, 156);
			panelLineCommentSituLine1.add(scrollPaneNewDossSituLineComComLine1);
			
			final JTextArea textAreaNewDossSituLineComComLine1 = new JTextArea();
			scrollPaneNewDossSituLineComComLine1.setViewportView(textAreaNewDossSituLineComComLine1);
			textAreaNewDossSituLineComComLine1.setWrapStyleWord(true);
			textAreaNewDossSituLineComComLine1.setLineWrap(true);
			
			panelLineCommentSituLine2.setLayout(null);
			panelLineCommentSituLine2.setForeground(Color.BLACK);
			panelLineCommentSituLine2.setBounds(877, 44, 340, 400);
			panelSituation.add(panelLineCommentSituLine2);
			
			JLabel labelNewDossSituLineComDiscrLine2 = new JLabel("Line discription");
			labelNewDossSituLineComDiscrLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComDiscrLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComDiscrLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComDiscrLine2.setBounds(10, 11, 131, 25);
			panelLineCommentSituLine2.add(labelNewDossSituLineComDiscrLine2);
			
			JScrollPane scrollPaneNewDossSituLineComDiscrLine2 = new JScrollPane();
			scrollPaneNewDossSituLineComDiscrLine2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossSituLineComDiscrLine2.setBounds(10, 36, 320, 156);
			panelLineCommentSituLine2.add(scrollPaneNewDossSituLineComDiscrLine2);
			
			JTextArea textAreaNewDossSituLineComDiscrLine2 = new JTextArea();
			textAreaNewDossSituLineComDiscrLine2.setWrapStyleWord(true);
			textAreaNewDossSituLineComDiscrLine2.setLineWrap(true);
			textAreaNewDossSituLineComDiscrLine2.setEditable(false);
			scrollPaneNewDossSituLineComDiscrLine2.setViewportView(textAreaNewDossSituLineComDiscrLine2);
			
			JLabel labelNewDossSituLineComComLine2 = new JLabel("Line comment");
			labelNewDossSituLineComComLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComComLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComComLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComComLine2.setBounds(12, 209, 106, 25);
			panelLineCommentSituLine2.add(labelNewDossSituLineComComLine2);
			
			JScrollPane scrollPaneNewDossSituLineComComLine2 = new JScrollPane();
			scrollPaneNewDossSituLineComComLine2.setBounds(10, 234, 318, 156);
			panelLineCommentSituLine2.add(scrollPaneNewDossSituLineComComLine2);
			
			final JTextArea textAreaNewDossSituLineComComLine2 = new JTextArea();
			textAreaNewDossSituLineComComLine2.setWrapStyleWord(true);
			textAreaNewDossSituLineComComLine2.setLineWrap(true);
			scrollPaneNewDossSituLineComComLine2.setViewportView(textAreaNewDossSituLineComComLine2);
			
			panelLineCommentSituLine3.setLayout(null);
			panelLineCommentSituLine3.setForeground(Color.BLACK);
			panelLineCommentSituLine3.setBounds(877, 44, 340, 400);
			panelSituation.add(panelLineCommentSituLine3);
			
			JLabel labelNewDossSituLineComDiscrLine3 = new JLabel("Line discription");
			labelNewDossSituLineComDiscrLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComDiscrLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComDiscrLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComDiscrLine3.setBounds(10, 11, 131, 25);
			panelLineCommentSituLine3.add(labelNewDossSituLineComDiscrLine3);
			
			JScrollPane scrollPaneNewDossSituLineComDiscrLine3 = new JScrollPane();
			scrollPaneNewDossSituLineComDiscrLine3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossSituLineComDiscrLine3.setBounds(10, 36, 320, 156);
			panelLineCommentSituLine3.add(scrollPaneNewDossSituLineComDiscrLine3);
			
			JTextArea textAreaNewDossSituLineComDiscrLine3 = new JTextArea();
			textAreaNewDossSituLineComDiscrLine3.setWrapStyleWord(true);
			textAreaNewDossSituLineComDiscrLine3.setLineWrap(true);
			textAreaNewDossSituLineComDiscrLine3.setEditable(false);
			scrollPaneNewDossSituLineComDiscrLine3.setViewportView(textAreaNewDossSituLineComDiscrLine3);
			
			JLabel labelNewDossSituLineComComLine3 = new JLabel("Line comment");
			labelNewDossSituLineComComLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComComLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComComLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComComLine3.setBounds(12, 209, 106, 25);
			panelLineCommentSituLine3.add(labelNewDossSituLineComComLine3);
			
			JScrollPane scrollPaneNewDossSituLineComComLine3 = new JScrollPane();
			scrollPaneNewDossSituLineComComLine3.setBounds(10, 234, 318, 156);
			panelLineCommentSituLine3.add(scrollPaneNewDossSituLineComComLine3);
			
			final JTextArea textAreaNewDossSituLineComComLine3 = new JTextArea();
			textAreaNewDossSituLineComComLine3.setWrapStyleWord(true);
			textAreaNewDossSituLineComComLine3.setLineWrap(true);
			scrollPaneNewDossSituLineComComLine3.setViewportView(textAreaNewDossSituLineComComLine3);
			
			panelLineCommentSituLine4.setLayout(null);
			panelLineCommentSituLine4.setForeground(Color.BLACK);
			panelLineCommentSituLine4.setBounds(877, 44, 340, 400);
			panelSituation.add(panelLineCommentSituLine4);
			
			JLabel labelNewDossSituLineComDiscrLine4 = new JLabel("Line discription");
			labelNewDossSituLineComDiscrLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComDiscrLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComDiscrLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComDiscrLine4.setBounds(10, 11, 131, 25);
			panelLineCommentSituLine4.add(labelNewDossSituLineComDiscrLine4);
			
			JScrollPane scrollPaneNewDossSituLineComDiscrLine4 = new JScrollPane();
			scrollPaneNewDossSituLineComDiscrLine4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossSituLineComDiscrLine4.setBounds(10, 36, 320, 156);
			panelLineCommentSituLine4.add(scrollPaneNewDossSituLineComDiscrLine4);
			
			JTextArea textAreaNewDossSituLineComDiscrLine4 = new JTextArea();
			textAreaNewDossSituLineComDiscrLine4.setWrapStyleWord(true);
			textAreaNewDossSituLineComDiscrLine4.setLineWrap(true);
			textAreaNewDossSituLineComDiscrLine4.setEditable(false);
			scrollPaneNewDossSituLineComDiscrLine4.setViewportView(textAreaNewDossSituLineComDiscrLine4);
			
			JLabel labelNewDossSituLineComComLine4 = new JLabel("Line comment");
			labelNewDossSituLineComComLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComComLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComComLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComComLine4.setBounds(12, 209, 106, 25);
			panelLineCommentSituLine4.add(labelNewDossSituLineComComLine4);
			
			JScrollPane scrollPaneNewDossSituLineComComLine4 = new JScrollPane();
			scrollPaneNewDossSituLineComComLine4.setBounds(10, 234, 318, 156);
			panelLineCommentSituLine4.add(scrollPaneNewDossSituLineComComLine4);
			
			final JTextArea textAreaNewDossSituLineComComLine4 = new JTextArea();
			textAreaNewDossSituLineComComLine4.setWrapStyleWord(true);
			textAreaNewDossSituLineComComLine4.setLineWrap(true);
			scrollPaneNewDossSituLineComComLine4.setViewportView(textAreaNewDossSituLineComComLine4);
			
			panelLineCommentSituLine5.setLayout(null);
			panelLineCommentSituLine5.setForeground(Color.BLACK);
			panelLineCommentSituLine5.setBounds(877, 44, 340, 400);
			panelSituation.add(panelLineCommentSituLine5);
			
			JLabel labelNewDossSituLineComDiscrLine5 = new JLabel("Line discription");
			labelNewDossSituLineComDiscrLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComDiscrLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComDiscrLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComDiscrLine5.setBounds(10, 11, 131, 25);
			panelLineCommentSituLine5.add(labelNewDossSituLineComDiscrLine5);
			
			JScrollPane scrollPaneNewDossSituLineComDiscrLine5 = new JScrollPane();
			scrollPaneNewDossSituLineComDiscrLine5.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossSituLineComDiscrLine5.setBounds(10, 36, 320, 156);
			panelLineCommentSituLine5.add(scrollPaneNewDossSituLineComDiscrLine5);
			
			JTextArea textAreaNewDossSituLineComDiscrLine5 = new JTextArea();
			textAreaNewDossSituLineComDiscrLine5.setWrapStyleWord(true);
			textAreaNewDossSituLineComDiscrLine5.setLineWrap(true);
			textAreaNewDossSituLineComDiscrLine5.setEditable(false);
			scrollPaneNewDossSituLineComDiscrLine5.setViewportView(textAreaNewDossSituLineComDiscrLine5);
			
			JLabel labelNewDossSituLineComComLine5 = new JLabel("Line comment");
			labelNewDossSituLineComComLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComComLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComComLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComComLine5.setBounds(12, 209, 106, 25);
			panelLineCommentSituLine5.add(labelNewDossSituLineComComLine5);
			
			JScrollPane scrollPaneNewDossSituLineComComLine5 = new JScrollPane();
			scrollPaneNewDossSituLineComComLine5.setBounds(10, 234, 318, 156);
			panelLineCommentSituLine5.add(scrollPaneNewDossSituLineComComLine5);
			
			final JTextArea textAreaNewDossSituLineComComLine5 = new JTextArea();
			textAreaNewDossSituLineComComLine5.setWrapStyleWord(true);
			textAreaNewDossSituLineComComLine5.setLineWrap(true);
			scrollPaneNewDossSituLineComComLine5.setViewportView(textAreaNewDossSituLineComComLine5);
			
			panelLineCommentSituLine6.setLayout(null);
			panelLineCommentSituLine6.setForeground(Color.BLACK);
			panelLineCommentSituLine6.setBounds(877, 44, 340, 400);
			panelSituation.add(panelLineCommentSituLine6);
			
			JLabel labelNewDossSituLineComDiscrLine6 = new JLabel("Line discription");
			labelNewDossSituLineComDiscrLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComDiscrLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComDiscrLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComDiscrLine6.setBounds(10, 11, 131, 25);
			panelLineCommentSituLine6.add(labelNewDossSituLineComDiscrLine6);
			
			JScrollPane scrollPaneNewDossSituLineComDiscrLine6 = new JScrollPane();
			scrollPaneNewDossSituLineComDiscrLine6.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossSituLineComDiscrLine6.setBounds(10, 36, 320, 156);
			panelLineCommentSituLine6.add(scrollPaneNewDossSituLineComDiscrLine6);
			
			JTextArea textAreaNewDossSituLineComDiscrLine6 = new JTextArea();
			textAreaNewDossSituLineComDiscrLine6.setWrapStyleWord(true);
			textAreaNewDossSituLineComDiscrLine6.setLineWrap(true);
			textAreaNewDossSituLineComDiscrLine6.setEditable(false);
			scrollPaneNewDossSituLineComDiscrLine6.setViewportView(textAreaNewDossSituLineComDiscrLine6);
			
			JLabel labelNewDossSituLineComComLine6 = new JLabel("Line comment");
			labelNewDossSituLineComComLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossSituLineComComLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossSituLineComComLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossSituLineComComLine6.setBounds(12, 209, 106, 25);
			panelLineCommentSituLine6.add(labelNewDossSituLineComComLine6);
			
			JScrollPane scrollPaneNewDossSituLineComComLine6 = new JScrollPane();
			scrollPaneNewDossSituLineComComLine6.setBounds(10, 234, 318, 156);
			panelLineCommentSituLine6.add(scrollPaneNewDossSituLineComComLine6);
			
			final JTextArea textAreaNewDossSituLineComComLine6 = new JTextArea();
			textAreaNewDossSituLineComComLine6.setWrapStyleWord(true);
			textAreaNewDossSituLineComComLine6.setLineWrap(true);
			scrollPaneNewDossSituLineComComLine6.setViewportView(textAreaNewDossSituLineComComLine6);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(655, 467, 562, 165);
			panelSituation.add(scrollPane);
			
			final JTextArea textAreaNewDossSituHexagramComment = new JTextArea();
			scrollPane.setViewportView(textAreaNewDossSituHexagramComment);
			textAreaNewDossSituHexagramComment.setWrapStyleWord(true);
			textAreaNewDossSituHexagramComment.setLineWrap(true);
			
			JLabel lblSituationHexagramComment = new JLabel("Situation hexagram comment");
			lblSituationHexagramComment.setVerticalAlignment(SwingConstants.TOP);
			lblSituationHexagramComment.setHorizontalAlignment(SwingConstants.LEFT);
			lblSituationHexagramComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			lblSituationHexagramComment.setBounds(654, 443, 226, 23);
			panelSituation.add(lblSituationHexagramComment);
			
			panelLineCommentSituLine1.setVisible(false);
			panelLineCommentSituLine2.setVisible(false);
			panelLineCommentSituLine3.setVisible(false);
			panelLineCommentSituLine4.setVisible(false);
			panelLineCommentSituLine5.setVisible(false);
			panelLineCommentSituLine6.setVisible(false);
			
			//********************************************************************************************************************************************
			//
			//Initialisierung New Dossier - Perspective
			
			JLabel labelNewDossPerspTitleHexa = new JLabel("Perspective Hexagram");
			labelNewDossPerspTitleHexa.setFont(new Font("Tahoma", Font.BOLD, 18));
			labelNewDossPerspTitleHexa.setEnabled(true);
			labelNewDossPerspTitleHexa.setBounds(645, 19, 222, 40);
			panelPerspective.add(labelNewDossPerspTitleHexa);
			
			JLabel labelNewDossPerspQuestion = new JLabel("Question");
			labelNewDossPerspQuestion.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspQuestion.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspQuestion.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspQuestion.setBounds(39, 33, 76, 23);
			panelPerspective.add(labelNewDossPerspQuestion);
			
			JScrollPane scrollPaneNewDossPerspQuest = new JScrollPane();
			scrollPaneNewDossPerspQuest.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossPerspQuest.setBounds(39, 58, 564, 72);
			panelPerspective.add(scrollPaneNewDossPerspQuest);
			
			textAreaNewDossPerspQuest.setEditable(false);
			textAreaNewDossPerspQuest.setWrapStyleWord(true);
			textAreaNewDossPerspQuest.setLineWrap(true);
			scrollPaneNewDossPerspQuest.setViewportView(textAreaNewDossPerspQuest);
			
			JLabel labelNewDossPerspComment = new JLabel("User comment");
			labelNewDossPerspComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspComment.setBounds(39, 141, 116, 23);
			panelPerspective.add(labelNewDossPerspComment);
			
			JScrollPane scrollPaneNewDossPerspComment = new JScrollPane();
			scrollPaneNewDossPerspComment.setBounds(39, 169, 564, 98);
			panelPerspective.add(scrollPaneNewDossPerspComment);
			
			textAreaNewDossPerspComment.setEditable(false);
			textAreaNewDossPerspComment.setWrapStyleWord(true);
			textAreaNewDossPerspComment.setLineWrap(true);
			scrollPaneNewDossPerspComment.setViewportView(textAreaNewDossPerspComment);
			
			JLabel labelNewDossPerspJudgement = new JLabel("Judgement");
			labelNewDossPerspJudgement.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspJudgement.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspJudgement.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspJudgement.setBounds(39, 278, 97, 23);
			panelPerspective.add(labelNewDossPerspJudgement);
			
			JScrollPane scrollPaneNewDossPerspQuestSynth = new JScrollPane();
			scrollPaneNewDossPerspQuestSynth.setBounds(39, 312, 564, 92);
			panelPerspective.add(scrollPaneNewDossPerspQuestSynth);
			textAreaNewDossPerspJudgement.setWrapStyleWord(true);
			textAreaNewDossPerspJudgement.setLineWrap(true);
			scrollPaneNewDossPerspQuestSynth.setViewportView(textAreaNewDossPerspJudgement);
			
			labelNewDossPerspLineComLineNr.setBounds(887, 26, 131, 25);
			panelPerspective.add(labelNewDossPerspLineComLineNr);
			labelNewDossPerspLineComLineNr.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComLineNr.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComLineNr.setFont(new Font("Tahoma", Font.BOLD, 13));
			
			
			//Erstellen der KommentarSparte Line
			
			panelLineCommentPerspLine1.setForeground(Color.BLACK);
			panelLineCommentPerspLine1.setBounds(877, 44, 340, 405);
			panelPerspective.add(panelLineCommentPerspLine1);
			panelLineCommentPerspLine1.setLayout(null);
			
			JLabel labelNewDossPerspLineComDiscrLine1 = new JLabel("Line discription");
			labelNewDossPerspLineComDiscrLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComDiscrLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComDiscrLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComDiscrLine1.setBounds(10, 11, 131, 25);
			panelLineCommentPerspLine1.add(labelNewDossPerspLineComDiscrLine1);
			
			JScrollPane scrollPaneNewDossPerspLineComDiscrLine1 = new JScrollPane();
			scrollPaneNewDossPerspLineComDiscrLine1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossPerspLineComDiscrLine1.setBounds(10, 36, 320, 156);
			panelLineCommentPerspLine1.add(scrollPaneNewDossPerspLineComDiscrLine1);
			
			JTextArea textAreaNewDossPerspLineComDiscrLine1 = new JTextArea();
			textAreaNewDossPerspLineComDiscrLine1.setEditable(false);
			textAreaNewDossPerspLineComDiscrLine1.setWrapStyleWord(true);
			textAreaNewDossPerspLineComDiscrLine1.setLineWrap(true);
			scrollPaneNewDossPerspLineComDiscrLine1.setViewportView(textAreaNewDossPerspLineComDiscrLine1);
			
			JLabel labelNewDossPerspLineComComLine1 = new JLabel("Line comment");
			labelNewDossPerspLineComComLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComComLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComComLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComComLine1.setBounds(12, 209, 106, 25);
			panelLineCommentPerspLine1.add(labelNewDossPerspLineComComLine1);
			
			JScrollPane scrollPaneNewDossPerspLineComComLine1 = new JScrollPane();
			scrollPaneNewDossPerspLineComComLine1.setBounds(10, 234, 318, 156);
			panelLineCommentPerspLine1.add(scrollPaneNewDossPerspLineComComLine1);
			
			final JTextArea textAreaNewDossPerspLineComComLine1 = new JTextArea();
			scrollPaneNewDossPerspLineComComLine1.setViewportView(textAreaNewDossPerspLineComComLine1);
			textAreaNewDossPerspLineComComLine1.setWrapStyleWord(true);
			textAreaNewDossPerspLineComComLine1.setLineWrap(true);
			
			panelLineCommentPerspLine2.setLayout(null);
			panelLineCommentPerspLine2.setForeground(Color.BLACK);
			panelLineCommentPerspLine2.setBounds(877, 44, 340, 405);
			panelPerspective.add(panelLineCommentPerspLine2);
			
			JLabel labelNewDossPerspLineComDiscrLine2 = new JLabel("Line discription");
			labelNewDossPerspLineComDiscrLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComDiscrLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComDiscrLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComDiscrLine2.setBounds(10, 11, 131, 25);
			panelLineCommentPerspLine2.add(labelNewDossPerspLineComDiscrLine2);
			
			JScrollPane scrollPaneNewDossPerspLineComDiscrLine2 = new JScrollPane();
			scrollPaneNewDossPerspLineComDiscrLine2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossPerspLineComDiscrLine2.setBounds(10, 36, 320, 156);
			panelLineCommentPerspLine2.add(scrollPaneNewDossPerspLineComDiscrLine2);
			
			JTextArea textAreaNewDossPerspLineComDiscrLine2 = new JTextArea();
			textAreaNewDossPerspLineComDiscrLine2.setWrapStyleWord(true);
			textAreaNewDossPerspLineComDiscrLine2.setLineWrap(true);
			textAreaNewDossPerspLineComDiscrLine2.setEditable(false);
			scrollPaneNewDossPerspLineComDiscrLine2.setViewportView(textAreaNewDossPerspLineComDiscrLine2);
			
			JLabel labelNewDossPerspLineComComLine2 = new JLabel("Line comment");
			labelNewDossPerspLineComComLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComComLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComComLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComComLine2.setBounds(12, 209, 106, 25);
			panelLineCommentPerspLine2.add(labelNewDossPerspLineComComLine2);
			
			JScrollPane scrollPaneNewDossPerspLineComComLine2 = new JScrollPane();
			scrollPaneNewDossPerspLineComComLine2.setBounds(10, 234, 318, 156);
			panelLineCommentPerspLine2.add(scrollPaneNewDossPerspLineComComLine2);
			
			final JTextArea textAreaNewDossPerspLineComComLine2 = new JTextArea();
			textAreaNewDossPerspLineComComLine2.setWrapStyleWord(true);
			textAreaNewDossPerspLineComComLine2.setLineWrap(true);
			scrollPaneNewDossPerspLineComComLine2.setViewportView(textAreaNewDossPerspLineComComLine2);
		

			panelLineCommentPerspLine3.setLayout(null);
			panelLineCommentPerspLine3.setForeground(Color.BLACK);
			panelLineCommentPerspLine3.setBounds(877, 44, 340, 405);
			panelPerspective.add(panelLineCommentPerspLine3);
			
			JLabel labelNewDossPerspLineComDiscrLine3 = new JLabel("Line discription");
			labelNewDossPerspLineComDiscrLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComDiscrLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComDiscrLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComDiscrLine3.setBounds(10, 11, 131, 25);
			panelLineCommentPerspLine3.add(labelNewDossPerspLineComDiscrLine3);
			
			JScrollPane scrollPaneNewDossPerspLineComDiscrLine3 = new JScrollPane();
			scrollPaneNewDossPerspLineComDiscrLine3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossPerspLineComDiscrLine3.setBounds(10, 36, 320, 156);
			panelLineCommentPerspLine3.add(scrollPaneNewDossPerspLineComDiscrLine3);
			
			JTextArea textAreaNewDossPerspLineComDiscrLin3 = new JTextArea();
			textAreaNewDossPerspLineComDiscrLin3.setWrapStyleWord(true);
			textAreaNewDossPerspLineComDiscrLin3.setLineWrap(true);
			textAreaNewDossPerspLineComDiscrLin3.setEditable(false);
			scrollPaneNewDossPerspLineComDiscrLine3.setViewportView(textAreaNewDossPerspLineComDiscrLin3);
			
			JLabel labelNewDossPerspLineComComLine3 = new JLabel("Line comment");
			labelNewDossPerspLineComComLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComComLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComComLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComComLine3.setBounds(12, 209, 106, 25);
			panelLineCommentPerspLine3.add(labelNewDossPerspLineComComLine3);
			
			JScrollPane scrollPaneNewDossPerspLineComComLine3 = new JScrollPane();
			scrollPaneNewDossPerspLineComComLine3.setBounds(10, 234, 318, 156);
			panelLineCommentPerspLine3.add(scrollPaneNewDossPerspLineComComLine3);
			
			final JTextArea textAreaNewDossPerspLineComComLine3 = new JTextArea();
			textAreaNewDossPerspLineComComLine3.setWrapStyleWord(true);
			textAreaNewDossPerspLineComComLine3.setLineWrap(true);
			scrollPaneNewDossPerspLineComComLine3.setViewportView(textAreaNewDossPerspLineComComLine3);
			
			panelLineCommentPerspLine4.setLayout(null);
			panelLineCommentPerspLine4.setForeground(Color.BLACK);
			panelLineCommentPerspLine4.setBounds(877, 44, 340, 405);
			panelPerspective.add(panelLineCommentPerspLine4);
			
			JLabel labelNewDossPerspLineComDiscrLine4 = new JLabel("Line discription");
			labelNewDossPerspLineComDiscrLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComDiscrLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComDiscrLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComDiscrLine4.setBounds(10, 11, 131, 25);
			panelLineCommentPerspLine4.add(labelNewDossPerspLineComDiscrLine4);
			
			JScrollPane scrollPaneNewDossPerspLineComDiscrLine4 = new JScrollPane();
			scrollPaneNewDossPerspLineComDiscrLine4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossPerspLineComDiscrLine4.setBounds(10, 36, 320, 156);
			panelLineCommentPerspLine4.add(scrollPaneNewDossPerspLineComDiscrLine4);
			
			JTextArea textAreaNewDossPerspLineComDiscrLin4 = new JTextArea();
			textAreaNewDossPerspLineComDiscrLin4.setWrapStyleWord(true);
			textAreaNewDossPerspLineComDiscrLin4.setLineWrap(true);
			textAreaNewDossPerspLineComDiscrLin4.setEditable(false);
			scrollPaneNewDossPerspLineComDiscrLine4.setViewportView(textAreaNewDossPerspLineComDiscrLin4);
			
			JLabel labelNewDossPerspLineComComLine4 = new JLabel("Line comment");
			labelNewDossPerspLineComComLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComComLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComComLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComComLine4.setBounds(12, 209, 106, 25);
			panelLineCommentPerspLine4.add(labelNewDossPerspLineComComLine4);
			
			JScrollPane scrollPaneNewDossPerspLineComComLine4 = new JScrollPane();
			scrollPaneNewDossPerspLineComComLine4.setBounds(10, 234, 318, 156);
			panelLineCommentPerspLine4.add(scrollPaneNewDossPerspLineComComLine4);
			
			final JTextArea textAreaNewDossPerspLineComComLine4 = new JTextArea();
			textAreaNewDossPerspLineComComLine4.setWrapStyleWord(true);
			textAreaNewDossPerspLineComComLine4.setLineWrap(true);
			scrollPaneNewDossPerspLineComComLine4.setViewportView(textAreaNewDossPerspLineComComLine4);
			
			panelLineCommentPerspLine5.setLayout(null);
			panelLineCommentPerspLine5.setForeground(Color.BLACK);
			panelLineCommentPerspLine5.setBounds(877, 44, 340, 405);
			panelPerspective.add(panelLineCommentPerspLine5);
			
			JLabel labelNewDossPerspLineComDiscrLine5 = new JLabel("Line discription");
			labelNewDossPerspLineComDiscrLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComDiscrLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComDiscrLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComDiscrLine5.setBounds(10, 11, 131, 25);
			panelLineCommentPerspLine5.add(labelNewDossPerspLineComDiscrLine5);
			
			JScrollPane scrollPaneNewDossPerspLineComDiscrLine5 = new JScrollPane();
			scrollPaneNewDossPerspLineComDiscrLine5.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossPerspLineComDiscrLine5.setBounds(10, 36, 320, 156);
			panelLineCommentPerspLine5.add(scrollPaneNewDossPerspLineComDiscrLine5);
			
			JTextArea textAreaNewDossPerspLineComDiscrLin5 = new JTextArea();
			textAreaNewDossPerspLineComDiscrLin5.setWrapStyleWord(true);
			textAreaNewDossPerspLineComDiscrLin5.setLineWrap(true);
			textAreaNewDossPerspLineComDiscrLin5.setEditable(false);
			scrollPaneNewDossPerspLineComDiscrLine5.setViewportView(textAreaNewDossPerspLineComDiscrLin5);
			
			JLabel labelNewDossPerspLineComComLine5 = new JLabel("Line comment");
			labelNewDossPerspLineComComLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComComLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComComLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComComLine5.setBounds(12, 209, 106, 25);
			panelLineCommentPerspLine5.add(labelNewDossPerspLineComComLine5);
			
			JScrollPane scrollPaneNewDossPerspLineComComLine5 = new JScrollPane();
			scrollPaneNewDossPerspLineComComLine5.setBounds(10, 234, 318, 156);
			panelLineCommentPerspLine5.add(scrollPaneNewDossPerspLineComComLine5);
			
			final JTextArea textAreaNewDossPerspLineComComLine5 = new JTextArea();
			textAreaNewDossPerspLineComComLine5.setWrapStyleWord(true);
			textAreaNewDossPerspLineComComLine5.setLineWrap(true);
			scrollPaneNewDossPerspLineComComLine5.setViewportView(textAreaNewDossPerspLineComComLine5);
			
			panelLineCommentPerspLine6.setLayout(null);
			panelLineCommentPerspLine6.setForeground(Color.BLACK);
			panelLineCommentPerspLine6.setBounds(877, 44, 340, 405);
			panelPerspective.add(panelLineCommentPerspLine6);
			
			JLabel labelNewDossPerspLineComDiscrLine6 = new JLabel("Line discription");
			labelNewDossPerspLineComDiscrLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComDiscrLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComDiscrLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComDiscrLine6.setBounds(10, 11, 131, 25);
			panelLineCommentPerspLine6.add(labelNewDossPerspLineComDiscrLine6);
			
			JScrollPane scrollPaneNewDossPerspLineComDiscrLine6 = new JScrollPane();
			scrollPaneNewDossPerspLineComDiscrLine6.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossPerspLineComDiscrLine6.setBounds(10, 36, 320, 156);
			panelLineCommentPerspLine6.add(scrollPaneNewDossPerspLineComDiscrLine6);
			
			JTextArea textAreaNewDossPerspLineComDiscrLin6 = new JTextArea();
			textAreaNewDossPerspLineComDiscrLin6.setWrapStyleWord(true);
			textAreaNewDossPerspLineComDiscrLin6.setLineWrap(true);
			textAreaNewDossPerspLineComDiscrLin6.setEditable(false);
			scrollPaneNewDossPerspLineComDiscrLine6.setViewportView(textAreaNewDossPerspLineComDiscrLin6);
			
			JLabel labelNewDossPerspLineComComLine6 = new JLabel("Line comment");
			labelNewDossPerspLineComComLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspLineComComLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspLineComComLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspLineComComLine6.setBounds(12, 209, 106, 25);
			panelLineCommentPerspLine6.add(labelNewDossPerspLineComComLine6);
			
			JScrollPane scrollPaneNewDossPerspLineComComLine6 = new JScrollPane();
			scrollPaneNewDossPerspLineComComLine6.setBounds(10, 234, 318, 156);
			panelLineCommentPerspLine6.add(scrollPaneNewDossPerspLineComComLine6);
			
			final JTextArea textAreaNewDossPerspLineComComLine6 = new JTextArea();
			textAreaNewDossPerspLineComComLine6.setWrapStyleWord(true);
			textAreaNewDossPerspLineComComLine6.setLineWrap(true);
			scrollPaneNewDossPerspLineComComLine6.setViewportView(textAreaNewDossPerspLineComComLine6);
			
			JLabel labelNewDossPerspImage = new JLabel("Image");
			labelNewDossPerspImage.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspImage.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspImage.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspImage.setBounds(39, 457, 73, 23);
			panelPerspective.add(labelNewDossPerspImage);
			
			JScrollPane scrollPanelNewDossPerspImage = new JScrollPane();
			scrollPanelNewDossPerspImage.setBounds(39, 491, 564, 167);
			panelPerspective.add(scrollPanelNewDossPerspImage);
			
			final JTextArea textAreaNewDossPerspImage = new JTextArea();
			textAreaNewDossPerspImage.setWrapStyleWord(true);
			textAreaNewDossPerspImage.setLineWrap(true);
			scrollPanelNewDossPerspImage.setViewportView(textAreaNewDossPerspImage);
			
			JLabel labelNewDossPerspHexagramComment = new JLabel("Perspective hexagram comment");
			labelNewDossPerspHexagramComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossPerspHexagramComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossPerspHexagramComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossPerspHexagramComment.setBounds(641, 457, 226, 23);
			panelPerspective.add(labelNewDossPerspHexagramComment);
			
			JScrollPane scrollPane_2 = new JScrollPane();
			scrollPane_2.setBounds(638, 491, 562, 165);
			panelPerspective.add(scrollPane_2);
			
			final JTextArea textAreaNewDossPerspHexagramComment = new JTextArea();
			textAreaNewDossPerspHexagramComment.setWrapStyleWord(true);
			textAreaNewDossPerspHexagramComment.setLineWrap(true);
			scrollPane_2.setViewportView(textAreaNewDossPerspHexagramComment);
			
			panelLineCommentPerspLine1.setVisible(false);
			panelLineCommentPerspLine2.setVisible(false);
			panelLineCommentPerspLine3.setVisible(false);
			panelLineCommentPerspLine4.setVisible(false);
			panelLineCommentPerspLine5.setVisible(false);
			panelLineCommentPerspLine6.setVisible(false);
			
				

		
			//********************************************************************************************************************************************
			//
			//Initialisierung New Dossier - Nuclear
				
			panelNuclear.add(buttonNewDossNucLineFull6);
			panelNuclear.add(buttonNewDossNucLineFull5);
			panelNuclear.add(buttonNewDossNucLineFull4);
			panelNuclear.add(buttonNewDossNucLineFull3);
			panelNuclear.add(buttonNewDossNucLineFull2);
			panelNuclear.add(buttonNewDossNucLineFull1);
			panelNuclear.add(buttonNewDossNucLineHalf6);
			panelNuclear.add(buttonNewDossNucLineHalf5);
			panelNuclear.add(buttonNewDossNucLineHalf4);
			panelNuclear.add(buttonNewDossNucLineHalf3);
			panelNuclear.add(buttonNewDossNucLineHalf2);
			panelNuclear.add(buttonNewDossNucLineHalf1);
			
			
			buttonNewDossNucLineFull6.setIcon(lineFull);
			buttonNewDossNucLineFull5.setIcon(lineFull);
			buttonNewDossNucLineFull4.setIcon(lineFull);
			buttonNewDossNucLineFull3.setIcon(lineFull);
			buttonNewDossNucLineFull2.setIcon(lineFull);
			buttonNewDossNucLineFull1.setIcon(lineFull);
			buttonNewDossNucLineHalf6.setIcon(lineUnterbrochen);
			buttonNewDossNucLineHalf5.setIcon(lineUnterbrochen);
			buttonNewDossNucLineHalf4.setIcon(lineUnterbrochen);
			buttonNewDossNucLineHalf3.setIcon(lineUnterbrochen);
			buttonNewDossNucLineHalf2.setIcon(lineUnterbrochen);
			buttonNewDossNucLineHalf1.setIcon(lineUnterbrochen);
			
			buttonNewDossNucLineFull6.setBounds(650, 86, 170, 32);
			buttonNewDossNucLineFull5.setBounds(650, 138, 170, 32);
			buttonNewDossNucLineFull4.setBounds(650, 188, 170, 32);
			buttonNewDossNucLineFull3.setBounds(650, 243, 170, 32);
			buttonNewDossNucLineFull2.setBounds(650, 295, 170, 32);
			buttonNewDossNucLineFull1.setBounds(650, 345, 170, 32);
			buttonNewDossNucLineHalf6.setBounds(650, 86, 170, 32);
			buttonNewDossNucLineHalf5.setBounds(650, 138, 170, 32);
			buttonNewDossNucLineHalf4.setBounds(650, 188, 170, 32);
			buttonNewDossNucLineHalf3.setBounds(650, 243, 170, 32);
			buttonNewDossNucLineHalf2.setBounds(650, 295, 170, 32);
			buttonNewDossNucLineHalf1.setBounds(650, 345, 170, 32);
			
			buttonNewDossNucLineFull6.setVisible(false);
			buttonNewDossNucLineFull5.setVisible(false);
			buttonNewDossNucLineFull4.setVisible(false);
			buttonNewDossNucLineFull3.setVisible(false);
			buttonNewDossNucLineFull2.setVisible(false);
			buttonNewDossNucLineFull1.setVisible(false);
			buttonNewDossNucLineHalf6.setVisible(false);
			buttonNewDossNucLineHalf5.setVisible(false);
			buttonNewDossNucLineHalf4.setVisible(false);
			buttonNewDossNucLineHalf3.setVisible(false);
			buttonNewDossNucLineHalf2.setVisible(false);
			buttonNewDossNucLineHalf1.setVisible(false);
			
			
			
			JLabel labelNewDossNucTitleHexa = new JLabel("Nuclear Hexagram");
			labelNewDossNucTitleHexa.setFont(new Font("Tahoma", Font.BOLD, 18));
			labelNewDossNucTitleHexa.setEnabled(true);
			labelNewDossNucTitleHexa.setBounds(645, 20, 190, 40);
			panelNuclear.add(labelNewDossNucTitleHexa);
			
			JLabel labelNewDossNucQuestion = new JLabel("Question");
			labelNewDossNucQuestion.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucQuestion.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucQuestion.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucQuestion.setBounds(39, 33, 76, 23);
			panelNuclear.add(labelNewDossNucQuestion);
			
			JScrollPane scrollPaneNewDossNucQuest = new JScrollPane();
			scrollPaneNewDossNucQuest.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossNucQuest.setBounds(39, 58, 564, 72);
			panelNuclear.add(scrollPaneNewDossNucQuest);
			
			textAreaNewDossNucQuest.setEditable(false);
			textAreaNewDossNucQuest.setWrapStyleWord(true);
			textAreaNewDossNucQuest.setLineWrap(true);
			scrollPaneNewDossNucQuest.setViewportView(textAreaNewDossNucQuest);
			
			JLabel labelNewDossNucComment = new JLabel("User comment");
			labelNewDossNucComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucComment.setBounds(39, 141, 128, 23);
			panelNuclear.add(labelNewDossNucComment);
			
			JScrollPane scrollPaneNewDossNucComment = new JScrollPane();
			scrollPaneNewDossNucComment.setBounds(39, 169, 564, 98);
			panelNuclear.add(scrollPaneNewDossNucComment);
			textAreaNewDossNucComment.setEditable(false);
			textAreaNewDossNucComment.setWrapStyleWord(true);
			textAreaNewDossNucComment.setLineWrap(true);
			scrollPaneNewDossNucComment.setViewportView(textAreaNewDossNucComment);
			
			JLabel labelNewDossNucJudgement = new JLabel("Judgement");
			labelNewDossNucJudgement.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucJudgement.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucJudgement.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucJudgement.setBounds(39, 278, 97, 23);
			panelNuclear.add(labelNewDossNucJudgement);
			
			JScrollPane scrollPaneNewDossNucQuestJudgement = new JScrollPane();
			scrollPaneNewDossNucQuestJudgement.setBounds(39, 312, 564, 92);
			panelNuclear.add(scrollPaneNewDossNucQuestJudgement);
			textAreaNewDossNucJudgement.setWrapStyleWord(true);
			textAreaNewDossNucJudgement.setLineWrap(true);
			scrollPaneNewDossNucQuestJudgement.setViewportView(textAreaNewDossNucJudgement);
			
			labelNewDossNucLineComLineNr.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComLineNr.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComLineNr.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComLineNr.setBounds(887, 26, 131, 25);
			panelNuclear.add(labelNewDossNucLineComLineNr);
			
			panelLineCommentNucLine1.setLayout(null);
			panelLineCommentNucLine1.setForeground(Color.BLACK);
			panelLineCommentNucLine1.setBounds(877, 44, 340, 409);
			panelNuclear.add(panelLineCommentNucLine1);
			
			JLabel labelNewDossNucLineComDiscrLine1 = new JLabel("Line discription");
			labelNewDossNucLineComDiscrLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComDiscrLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComDiscrLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComDiscrLine1.setBounds(10, 11, 131, 25);
			panelLineCommentNucLine1.add(labelNewDossNucLineComDiscrLine1);
			
			JScrollPane scrollPaneNewDossNucLineComDiscrLine1 = new JScrollPane();
			scrollPaneNewDossNucLineComDiscrLine1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossNucLineComDiscrLine1.setBounds(10, 36, 320, 156);
			panelLineCommentNucLine1.add(scrollPaneNewDossNucLineComDiscrLine1);
			
			JTextArea textAreaNewDossNucLineComDiscrLine1 = new JTextArea();
			textAreaNewDossNucLineComDiscrLine1.setWrapStyleWord(true);
			textAreaNewDossNucLineComDiscrLine1.setLineWrap(true);
			textAreaNewDossNucLineComDiscrLine1.setEditable(false);
			scrollPaneNewDossNucLineComDiscrLine1.setViewportView(textAreaNewDossNucLineComDiscrLine1);
			
			JLabel labelNewDossNucLineComComLine1 = new JLabel("Line comment");
			labelNewDossNucLineComComLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComComLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComComLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComComLine1.setBounds(12, 209, 106, 25);
			panelLineCommentNucLine1.add(labelNewDossNucLineComComLine1);
			
			JScrollPane scrollPaneNewDossNucLineComComLine1 = new JScrollPane();
			scrollPaneNewDossNucLineComComLine1.setBounds(10, 234, 318, 156);
			panelLineCommentNucLine1.add(scrollPaneNewDossNucLineComComLine1);
			
			final JTextArea textAreaNewDossNucLineComComLine1 = new JTextArea();
			textAreaNewDossNucLineComComLine1.setWrapStyleWord(true);
			textAreaNewDossNucLineComComLine1.setLineWrap(true);
			scrollPaneNewDossNucLineComComLine1.setViewportView(textAreaNewDossNucLineComComLine1);
			
			panelLineCommentNucLine2.setLayout(null);
			panelLineCommentNucLine2.setForeground(Color.BLACK);
			panelLineCommentNucLine2.setBounds(877, 44, 340, 409);
			panelNuclear.add(panelLineCommentNucLine2);
			
			JLabel labelNewDossNucLineComDiscrLine2 = new JLabel("Line discription");
			labelNewDossNucLineComDiscrLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComDiscrLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComDiscrLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComDiscrLine2.setBounds(10, 11, 131, 25);
			panelLineCommentNucLine2.add(labelNewDossNucLineComDiscrLine2);
			
			JScrollPane scrollPaneNewDossNucLineComDiscrLine2 = new JScrollPane();
			scrollPaneNewDossNucLineComDiscrLine2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossNucLineComDiscrLine2.setBounds(10, 36, 320, 156);
			panelLineCommentNucLine2.add(scrollPaneNewDossNucLineComDiscrLine2);
			
			JTextArea textAreaNewDossNucLineComDiscrLine2 = new JTextArea();
			textAreaNewDossNucLineComDiscrLine2.setWrapStyleWord(true);
			textAreaNewDossNucLineComDiscrLine2.setLineWrap(true);
			textAreaNewDossNucLineComDiscrLine2.setEditable(false);
			scrollPaneNewDossNucLineComDiscrLine2.setViewportView(textAreaNewDossNucLineComDiscrLine2);
			
			JLabel labelNewDossNucLineComComLine2 = new JLabel("Line comment");
			labelNewDossNucLineComComLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComComLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComComLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComComLine2.setBounds(12, 209, 106, 25);
			panelLineCommentNucLine2.add(labelNewDossNucLineComComLine2);
			
			JScrollPane scrollPaneNewDossNucLineComComLine2 = new JScrollPane();
			scrollPaneNewDossNucLineComComLine2.setBounds(10, 234, 318, 156);
			panelLineCommentNucLine2.add(scrollPaneNewDossNucLineComComLine2);
			
			final JTextArea textAreaNewDossNucLineComComLine2 = new JTextArea();
			textAreaNewDossNucLineComComLine2.setWrapStyleWord(true);
			textAreaNewDossNucLineComComLine2.setLineWrap(true);
			scrollPaneNewDossNucLineComComLine2.setViewportView(textAreaNewDossNucLineComComLine2);
			
			panelLineCommentNucLine3.setLayout(null);
			panelLineCommentNucLine3.setForeground(Color.BLACK);
			panelLineCommentNucLine3.setBounds(877, 44, 340, 409);
			panelNuclear.add(panelLineCommentNucLine3);
			
			JLabel labelNewDossNucLineComDiscrLine3 = new JLabel("Line discription");
			labelNewDossNucLineComDiscrLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComDiscrLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComDiscrLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComDiscrLine3.setBounds(10, 11, 131, 25);
			panelLineCommentNucLine3.add(labelNewDossNucLineComDiscrLine3);
			
			JScrollPane scrollPaneNewDossNucLineComDiscrLine3 = new JScrollPane();
			scrollPaneNewDossNucLineComDiscrLine3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossNucLineComDiscrLine3.setBounds(10, 36, 320, 156);
			panelLineCommentNucLine3.add(scrollPaneNewDossNucLineComDiscrLine3);
			
			JTextArea textAreaNewDossNucLineComDiscrLine3 = new JTextArea();
			textAreaNewDossNucLineComDiscrLine3.setWrapStyleWord(true);
			textAreaNewDossNucLineComDiscrLine3.setLineWrap(true);
			textAreaNewDossNucLineComDiscrLine3.setEditable(false);
			scrollPaneNewDossNucLineComDiscrLine3.setViewportView(textAreaNewDossNucLineComDiscrLine3);
			
			JLabel labelNewDossNucLineComComLine3 = new JLabel("Line comment");
			labelNewDossNucLineComComLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComComLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComComLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComComLine3.setBounds(12, 209, 106, 25);
			panelLineCommentNucLine3.add(labelNewDossNucLineComComLine3);
			
			JScrollPane scrollPaneNewDossNucLineComComLine3 = new JScrollPane();
			scrollPaneNewDossNucLineComComLine3.setBounds(10, 234, 318, 156);
			panelLineCommentNucLine3.add(scrollPaneNewDossNucLineComComLine3);
			
			final JTextArea textAreaNewDossNucLineComComLine3 = new JTextArea();
			textAreaNewDossNucLineComComLine3.setWrapStyleWord(true);
			textAreaNewDossNucLineComComLine3.setLineWrap(true);
			scrollPaneNewDossNucLineComComLine3.setViewportView(textAreaNewDossNucLineComComLine3);
			
			panelLineCommentNucLine4.setLayout(null);
			panelLineCommentNucLine4.setForeground(Color.BLACK);
			panelLineCommentNucLine4.setBounds(877, 44, 340, 409);
			panelNuclear.add(panelLineCommentNucLine4);
			
			JLabel labelNewDossNucLineComDiscrLine4 = new JLabel("Line discription");
			labelNewDossNucLineComDiscrLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComDiscrLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComDiscrLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComDiscrLine4.setBounds(10, 11, 131, 25);
			panelLineCommentNucLine4.add(labelNewDossNucLineComDiscrLine4);
			
			JScrollPane scrollPaneNewDossNucLineComDiscrLine4 = new JScrollPane();
			scrollPaneNewDossNucLineComDiscrLine4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossNucLineComDiscrLine4.setBounds(10, 36, 320, 156);
			panelLineCommentNucLine4.add(scrollPaneNewDossNucLineComDiscrLine4);
			
			JTextArea textAreaNewDossNucLineComDiscrLine4 = new JTextArea();
			textAreaNewDossNucLineComDiscrLine4.setWrapStyleWord(true);
			textAreaNewDossNucLineComDiscrLine4.setLineWrap(true);
			textAreaNewDossNucLineComDiscrLine4.setEditable(false);
			scrollPaneNewDossNucLineComDiscrLine4.setViewportView(textAreaNewDossNucLineComDiscrLine4);
			
			JLabel labelNewDossNucLineComComLine4 = new JLabel("Line comment");
			labelNewDossNucLineComComLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComComLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComComLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComComLine4.setBounds(12, 209, 106, 25);
			panelLineCommentNucLine4.add(labelNewDossNucLineComComLine4);
			
			JScrollPane scrollPaneNewDossNucLineComComLine4 = new JScrollPane();
			scrollPaneNewDossNucLineComComLine4.setBounds(10, 234, 318, 156);
			panelLineCommentNucLine4.add(scrollPaneNewDossNucLineComComLine4);
			
			final JTextArea textAreaNewDossNucLineComComLine4 = new JTextArea();
			textAreaNewDossNucLineComComLine4.setWrapStyleWord(true);
			textAreaNewDossNucLineComComLine4.setLineWrap(true);
			scrollPaneNewDossNucLineComComLine4.setViewportView(textAreaNewDossNucLineComComLine4);
			
			panelLineCommentNucLine5.setLayout(null);
			panelLineCommentNucLine5.setForeground(Color.BLACK);
			panelLineCommentNucLine5.setBounds(877, 44, 340, 409);
			panelNuclear.add(panelLineCommentNucLine5);
			
			JLabel labelNewDossNucLineComDiscrLine5 = new JLabel("Line discription");
			labelNewDossNucLineComDiscrLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComDiscrLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComDiscrLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComDiscrLine5.setBounds(10, 11, 131, 25);
			panelLineCommentNucLine5.add(labelNewDossNucLineComDiscrLine5);
			
			JScrollPane scrollPaneNewDossNucLineComDiscrLine5 = new JScrollPane();
			scrollPaneNewDossNucLineComDiscrLine5.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossNucLineComDiscrLine5.setBounds(10, 36, 320, 156);
			panelLineCommentNucLine5.add(scrollPaneNewDossNucLineComDiscrLine5);
			
			JTextArea textAreaNewDossNucLineComDiscrLine5 = new JTextArea();
			textAreaNewDossNucLineComDiscrLine5.setWrapStyleWord(true);
			textAreaNewDossNucLineComDiscrLine5.setLineWrap(true);
			textAreaNewDossNucLineComDiscrLine5.setEditable(false);
			scrollPaneNewDossNucLineComDiscrLine5.setViewportView(textAreaNewDossNucLineComDiscrLine5);
			
			JLabel labelNewDossNucLineComComLine5 = new JLabel("Line comment");
			labelNewDossNucLineComComLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComComLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComComLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComComLine5.setBounds(12, 209, 106, 25);
			panelLineCommentNucLine5.add(labelNewDossNucLineComComLine5);
			
			JScrollPane scrollPaneNewDossNucLineComComLine5 = new JScrollPane();
			scrollPaneNewDossNucLineComComLine5.setBounds(10, 234, 318, 156);
			panelLineCommentNucLine5.add(scrollPaneNewDossNucLineComComLine5);
			
			final JTextArea textAreaNewDossNucLineComComLine5 = new JTextArea();
			textAreaNewDossNucLineComComLine5.setWrapStyleWord(true);
			textAreaNewDossNucLineComComLine5.setLineWrap(true);
			scrollPaneNewDossNucLineComComLine5.setViewportView(textAreaNewDossNucLineComComLine5);
			
			panelLineCommentNucLine6.setLayout(null);
			panelLineCommentNucLine6.setForeground(Color.BLACK);
			panelLineCommentNucLine6.setBounds(877, 44, 340, 409);
			panelNuclear.add(panelLineCommentNucLine6);
			
			JLabel labelNewDossNucLineComDiscrLine6 = new JLabel("Line discription");
			labelNewDossNucLineComDiscrLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComDiscrLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComDiscrLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComDiscrLine6.setBounds(10, 11, 131, 25);
			panelLineCommentNucLine6.add(labelNewDossNucLineComDiscrLine6);
			
			JScrollPane scrollPaneNewDossNucLineComDiscrLine6 = new JScrollPane();
			scrollPaneNewDossNucLineComDiscrLine6.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossNucLineComDiscrLine6.setBounds(10, 36, 320, 156);
			panelLineCommentNucLine6.add(scrollPaneNewDossNucLineComDiscrLine6);
			
			JTextArea textAreaNewDossNucLineComDiscrLine6 = new JTextArea();
			textAreaNewDossNucLineComDiscrLine6.setWrapStyleWord(true);
			textAreaNewDossNucLineComDiscrLine6.setLineWrap(true);
			textAreaNewDossNucLineComDiscrLine6.setEditable(false);
			scrollPaneNewDossNucLineComDiscrLine6.setViewportView(textAreaNewDossNucLineComDiscrLine6);
			
			JLabel labelNewDossNucLineComComLine6 = new JLabel("Line comment");
			labelNewDossNucLineComComLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucLineComComLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucLineComComLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucLineComComLine6.setBounds(12, 209, 106, 25);
			panelLineCommentNucLine6.add(labelNewDossNucLineComComLine6);
			
			JScrollPane scrollPaneNewDossNucLineComComLine6 = new JScrollPane();
			scrollPaneNewDossNucLineComComLine6.setBounds(10, 234, 318, 156);
			panelLineCommentNucLine6.add(scrollPaneNewDossNucLineComComLine6);
			
			final JTextArea textAreaNewDossNucLineComComLine6 = new JTextArea();
			textAreaNewDossNucLineComComLine6.setWrapStyleWord(true);
			textAreaNewDossNucLineComComLine6.setLineWrap(true);
			scrollPaneNewDossNucLineComComLine6.setViewportView(textAreaNewDossNucLineComComLine6);
			
			JLabel labelNewDossNucImage = new JLabel("Image");
			labelNewDossNucImage.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucImage.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucImage.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucImage.setBounds(39, 457, 73, 23);
			panelNuclear.add(labelNewDossNucImage);
			
			JScrollPane scrollPaneNewDossNucImage = new JScrollPane();
			scrollPaneNewDossNucImage.setBounds(39, 491, 564, 167);
			panelNuclear.add(scrollPaneNewDossNucImage);
			
			final JTextArea textAreaNewDossNucImage = new JTextArea();
			textAreaNewDossNucImage.setWrapStyleWord(true);
			textAreaNewDossNucImage.setLineWrap(true);
			scrollPaneNewDossNucImage.setViewportView(textAreaNewDossNucImage);
			
			JLabel labelNewDossNucHexagramComment = new JLabel("Nuclear hexagram comment");
			labelNewDossNucHexagramComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossNucHexagramComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossNucHexagramComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossNucHexagramComment.setBounds(641, 457, 226, 23);
			panelNuclear.add(labelNewDossNucHexagramComment);
			
			JScrollPane scrollPaneNewDossNucHexagramComment = new JScrollPane();
			scrollPaneNewDossNucHexagramComment.setBounds(638, 491, 562, 165);
			panelNuclear.add(scrollPaneNewDossNucHexagramComment);
			
			final JTextArea textAreaNewDossNucHexagramComment = new JTextArea();
			textAreaNewDossNucHexagramComment.setWrapStyleWord(true);
			textAreaNewDossNucHexagramComment.setLineWrap(true);
			scrollPaneNewDossNucHexagramComment.setViewportView(textAreaNewDossNucHexagramComment);
			
			panelLineCommentNucLine1.setVisible(false);
			panelLineCommentNucLine2.setVisible(false);
			panelLineCommentNucLine3.setVisible(false);
			panelLineCommentNucLine4.setVisible(false);
			panelLineCommentNucLine5.setVisible(false);
			panelLineCommentNucLine6.setVisible(false);
			
			
			//********************************************************************************************************************************************
			//
			//Initialisierung New Dossier - Inverse			
			
			
			panelInverse.add(buttonNewDossInverLineFull6);
			panelInverse.add(buttonNewDossInverLineFull5);
			panelInverse.add(buttonNewDossInverLineFull4);
			panelInverse.add(buttonNewDossInverLineFull3);
			panelInverse.add(buttonNewDossInverLineFull2);
			panelInverse.add(buttonNewDossInverLineFull1);
			panelInverse.add(buttonNewDossInverLineHalf6);
			panelInverse.add(buttonNewDossInverLineHalf5);
			panelInverse.add(buttonNewDossInverLineHalf4);
			panelInverse.add(buttonNewDossInverLineHalf3);
			panelInverse.add(buttonNewDossInverLineHalf2);
			panelInverse.add(buttonNewDossInverLineHalf1);
			
			
			buttonNewDossInverLineFull6.setIcon(lineFull);
			buttonNewDossInverLineFull5.setIcon(lineFull);
			buttonNewDossInverLineFull4.setIcon(lineFull);
			buttonNewDossInverLineFull3.setIcon(lineFull);
			buttonNewDossInverLineFull2.setIcon(lineFull);
			buttonNewDossInverLineFull1.setIcon(lineFull);
			buttonNewDossInverLineHalf6.setIcon(lineUnterbrochen);
			buttonNewDossInverLineHalf5.setIcon(lineUnterbrochen);
			buttonNewDossInverLineHalf4.setIcon(lineUnterbrochen);
			buttonNewDossInverLineHalf3.setIcon(lineUnterbrochen);
			buttonNewDossInverLineHalf2.setIcon(lineUnterbrochen);
			buttonNewDossInverLineHalf1.setIcon(lineUnterbrochen);
			
			buttonNewDossInverLineFull6.setBounds(650, 86, 170, 32);
			buttonNewDossInverLineFull5.setBounds(650, 138, 170, 32);
			buttonNewDossInverLineFull4.setBounds(650, 188, 170, 32);
			buttonNewDossInverLineFull3.setBounds(650, 243, 170, 32);
			buttonNewDossInverLineFull2.setBounds(650, 295, 170, 32);
			buttonNewDossInverLineFull1.setBounds(650, 345, 170, 32);
			buttonNewDossInverLineHalf6.setBounds(650, 86, 170, 32);
			buttonNewDossInverLineHalf5.setBounds(650, 138, 170, 32);
			buttonNewDossInverLineHalf4.setBounds(650, 188, 170, 32);
			buttonNewDossInverLineHalf3.setBounds(650, 243, 170, 32);
			buttonNewDossInverLineHalf2.setBounds(650, 295, 170, 32);
			buttonNewDossInverLineHalf1.setBounds(650, 345, 170, 32);
			
			buttonNewDossInverLineFull6.setVisible(false);
			buttonNewDossInverLineFull5.setVisible(false);
			buttonNewDossInverLineFull4.setVisible(false);
			buttonNewDossInverLineFull3.setVisible(false);
			buttonNewDossInverLineFull2.setVisible(false);
			buttonNewDossInverLineFull1.setVisible(false);
			buttonNewDossInverLineHalf6.setVisible(false);
			buttonNewDossInverLineHalf5.setVisible(false);
			buttonNewDossInverLineHalf4.setVisible(false);
			buttonNewDossInverLineHalf3.setVisible(false);
			buttonNewDossInverLineHalf2.setVisible(false);
			buttonNewDossInverLineHalf1.setVisible(false);
			
			
			JLabel labelNewDossInverseTitleHexa = new JLabel("Inverse Hexagram");
			labelNewDossInverseTitleHexa.setFont(new Font("Tahoma", Font.BOLD, 18));
			labelNewDossInverseTitleHexa.setEnabled(true);
			labelNewDossInverseTitleHexa.setBounds(645, 20, 190, 40);
			panelInverse.add(labelNewDossInverseTitleHexa);
			
			JLabel labelNewDossInverseQuestion = new JLabel("Question");
			labelNewDossInverseQuestion.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInverseQuestion.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInverseQuestion.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInverseQuestion.setBounds(39, 33, 76, 23);
			panelInverse.add(labelNewDossInverseQuestion);
			
			JScrollPane scrollPaneNewDossInverseQuest = new JScrollPane();
			scrollPaneNewDossInverseQuest.setBounds(39, 58, 564, 72);
			panelInverse.add(scrollPaneNewDossInverseQuest);
			
			scrollPaneNewDossInverseQuest.setViewportView(textAreaNewDossInverseQuest);
			textAreaNewDossInverseQuest.setWrapStyleWord(true);
			textAreaNewDossInverseQuest.setLineWrap(true);
			textAreaNewDossInverseQuest.setEditable(false);
			
			JLabel labelNewDossInverseComment = new JLabel("User comment");
			labelNewDossInverseComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInverseComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInverseComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInverseComment.setBounds(39, 141, 114, 23);
			panelInverse.add(labelNewDossInverseComment);
			
			JScrollPane scrollPaneNewDossInverseComment = new JScrollPane();
			scrollPaneNewDossInverseComment.setBounds(39, 169, 564, 98);
			panelInverse.add(scrollPaneNewDossInverseComment);
			textAreaNewDossInverseUserComment.setEditable(false);
			
			textAreaNewDossInverseUserComment.setWrapStyleWord(true);
			textAreaNewDossInverseUserComment.setLineWrap(true);
			scrollPaneNewDossInverseComment.setViewportView(textAreaNewDossInverseUserComment);
			
			JLabel labelNewDossInverseJudgement = new JLabel("Judgement");
			labelNewDossInverseJudgement.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInverseJudgement.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInverseJudgement.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInverseJudgement.setBounds(39, 278, 97, 23);
			panelInverse.add(labelNewDossInverseJudgement);
			
			JScrollPane scrollPaneNewDossInverseSynth = new JScrollPane();
			scrollPaneNewDossInverseSynth.setBounds(39, 312, 564, 92);
			panelInverse.add(scrollPaneNewDossInverseSynth);
			
			textAreaNewDossInverseJudgement.setWrapStyleWord(true);
			textAreaNewDossInverseJudgement.setLineWrap(true);
			scrollPaneNewDossInverseSynth.setViewportView(textAreaNewDossInverseJudgement);
			
			labelNewDossInvLineComLineNr.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComLineNr.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComLineNr.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComLineNr.setBounds(887, 26, 131, 25);
			panelInverse.add(labelNewDossInvLineComLineNr);
			
			panelLineCommentInvLine1.setLayout(null);
			panelLineCommentInvLine1.setForeground(Color.BLACK);
			panelLineCommentInvLine1.setBounds(877, 44, 340, 405);
			panelInverse.add(panelLineCommentInvLine1);
			
			JLabel labelNewDossInvLineComDiscrLine1 = new JLabel("Line discription");
			labelNewDossInvLineComDiscrLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComDiscrLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComDiscrLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComDiscrLine1.setBounds(10, 11, 131, 25);
			panelLineCommentInvLine1.add(labelNewDossInvLineComDiscrLine1);
			
			JScrollPane scrollPaneNewDossInvLineComDiscrLine1 = new JScrollPane();
			scrollPaneNewDossInvLineComDiscrLine1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossInvLineComDiscrLine1.setBounds(10, 36, 320, 156);
			panelLineCommentInvLine1.add(scrollPaneNewDossInvLineComDiscrLine1);
			
			JTextArea textAreaNewDossInvLineComDiscrLine1 = new JTextArea();
			textAreaNewDossInvLineComDiscrLine1.setWrapStyleWord(true);
			textAreaNewDossInvLineComDiscrLine1.setLineWrap(true);
			textAreaNewDossInvLineComDiscrLine1.setEditable(false);
			scrollPaneNewDossInvLineComDiscrLine1.setViewportView(textAreaNewDossInvLineComDiscrLine1);
			
			JLabel labelNewDossInvLineComComLine1 = new JLabel("Line comment");
			labelNewDossInvLineComComLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComComLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComComLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComComLine1.setBounds(12, 209, 106, 25);
			panelLineCommentInvLine1.add(labelNewDossInvLineComComLine1);
			
			JScrollPane scrollPaneNewDossInvLineComComLine1 = new JScrollPane();
			scrollPaneNewDossInvLineComComLine1.setBounds(10, 234, 318, 156);
			panelLineCommentInvLine1.add(scrollPaneNewDossInvLineComComLine1);
			
			final JTextArea textAreaNewDossInvLineComComLine1 = new JTextArea();
			textAreaNewDossInvLineComComLine1.setWrapStyleWord(true);
			textAreaNewDossInvLineComComLine1.setLineWrap(true);
			scrollPaneNewDossInvLineComComLine1.setViewportView(textAreaNewDossInvLineComComLine1);
			
			panelLineCommentInvLine2.setLayout(null);
			panelLineCommentInvLine2.setForeground(Color.BLACK);
			panelLineCommentInvLine2.setBounds(877, 44, 340, 405);
			panelInverse.add(panelLineCommentInvLine2);
			
			JLabel labelNewDossInvLineComDiscrLine2 = new JLabel("Line discription");
			labelNewDossInvLineComDiscrLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComDiscrLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComDiscrLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComDiscrLine2.setBounds(10, 11, 131, 25);
			panelLineCommentInvLine2.add(labelNewDossInvLineComDiscrLine2);
			
			JScrollPane scrollPaneNewDossInvLineComDiscrLine2 = new JScrollPane();
			scrollPaneNewDossInvLineComDiscrLine2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossInvLineComDiscrLine2.setBounds(10, 36, 320, 156);
			panelLineCommentInvLine2.add(scrollPaneNewDossInvLineComDiscrLine2);
			
			JTextArea textAreaNewDossInvLineComDiscrLine2 = new JTextArea();
			textAreaNewDossInvLineComDiscrLine2.setWrapStyleWord(true);
			textAreaNewDossInvLineComDiscrLine2.setLineWrap(true);
			textAreaNewDossInvLineComDiscrLine2.setEditable(false);
			scrollPaneNewDossInvLineComDiscrLine2.setViewportView(textAreaNewDossInvLineComDiscrLine2);
			
			JLabel labelNewDossInvLineComComLine2 = new JLabel("Line comment");
			labelNewDossInvLineComComLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComComLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComComLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComComLine2.setBounds(12, 209, 106, 25);
			panelLineCommentInvLine2.add(labelNewDossInvLineComComLine2);
			
			JScrollPane scrollPaneNewDossInvLineComComLine2 = new JScrollPane();
			scrollPaneNewDossInvLineComComLine2.setBounds(10, 234, 318, 156);
			panelLineCommentInvLine2.add(scrollPaneNewDossInvLineComComLine2);
			
			final JTextArea textAreaNewDossInvLineComComLine2 = new JTextArea();
			textAreaNewDossInvLineComComLine2.setWrapStyleWord(true);
			textAreaNewDossInvLineComComLine2.setLineWrap(true);
			scrollPaneNewDossInvLineComComLine2.setViewportView(textAreaNewDossInvLineComComLine2);
			
			panelLineCommentInvLine3.setLayout(null);
			panelLineCommentInvLine3.setForeground(Color.BLACK);
			panelLineCommentInvLine3.setBounds(877, 44, 340, 405);
			panelInverse.add(panelLineCommentInvLine3);
			
			JLabel labelNewDossInvLineComDiscrLine3 = new JLabel("Line discription");
			labelNewDossInvLineComDiscrLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComDiscrLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComDiscrLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComDiscrLine3.setBounds(10, 11, 131, 25);
			panelLineCommentInvLine3.add(labelNewDossInvLineComDiscrLine3);
			
			JScrollPane scrollPaneNewDossInvLineComDiscrLine3 = new JScrollPane();
			scrollPaneNewDossInvLineComDiscrLine3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossInvLineComDiscrLine3.setBounds(10, 36, 320, 156);
			panelLineCommentInvLine3.add(scrollPaneNewDossInvLineComDiscrLine3);
			
			JTextArea textAreaNewDossInvLineComDiscrLine3 = new JTextArea();
			textAreaNewDossInvLineComDiscrLine3.setWrapStyleWord(true);
			textAreaNewDossInvLineComDiscrLine3.setLineWrap(true);
			textAreaNewDossInvLineComDiscrLine3.setEditable(false);
			scrollPaneNewDossInvLineComDiscrLine3.setViewportView(textAreaNewDossInvLineComDiscrLine3);
			
			JLabel labelNewDossInvLineComComLine3 = new JLabel("Line comment");
			labelNewDossInvLineComComLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComComLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComComLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComComLine3.setBounds(12, 209, 106, 25);
			panelLineCommentInvLine3.add(labelNewDossInvLineComComLine3);
			
			JScrollPane scrollPaneNewDossInvLineComComLine3 = new JScrollPane();
			scrollPaneNewDossInvLineComComLine3.setBounds(10, 234, 318, 156);
			panelLineCommentInvLine3.add(scrollPaneNewDossInvLineComComLine3);
			
			final JTextArea textAreaNewDossInvLineComComLine3 = new JTextArea();
			textAreaNewDossInvLineComComLine3.setWrapStyleWord(true);
			textAreaNewDossInvLineComComLine3.setLineWrap(true);
			scrollPaneNewDossInvLineComComLine3.setViewportView(textAreaNewDossInvLineComComLine3);
			
			panelLineCommentInvLine4.setLayout(null);
			panelLineCommentInvLine4.setForeground(Color.BLACK);
			panelLineCommentInvLine4.setBounds(877, 44, 340, 405);
			panelInverse.add(panelLineCommentInvLine4);
			
			JLabel labelNewDossInvLineComDiscrLine4 = new JLabel("Line discription");
			labelNewDossInvLineComDiscrLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComDiscrLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComDiscrLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComDiscrLine4.setBounds(10, 11, 131, 25);
			panelLineCommentInvLine4.add(labelNewDossInvLineComDiscrLine4);
			
			JScrollPane scrollPaneNewDossInvLineComDiscrLine4 = new JScrollPane();
			scrollPaneNewDossInvLineComDiscrLine4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossInvLineComDiscrLine4.setBounds(10, 36, 320, 156);
			panelLineCommentInvLine4.add(scrollPaneNewDossInvLineComDiscrLine4);
			
			JTextArea textAreaNewDossInvLineComDiscrLine4 = new JTextArea();
			textAreaNewDossInvLineComDiscrLine4.setWrapStyleWord(true);
			textAreaNewDossInvLineComDiscrLine4.setLineWrap(true);
			textAreaNewDossInvLineComDiscrLine4.setEditable(false);
			scrollPaneNewDossInvLineComDiscrLine4.setViewportView(textAreaNewDossInvLineComDiscrLine4);
			
			JLabel labelNewDossInvLineComComLine4 = new JLabel("Line comment");
			labelNewDossInvLineComComLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComComLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComComLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComComLine4.setBounds(12, 209, 106, 25);
			panelLineCommentInvLine4.add(labelNewDossInvLineComComLine4);
			
			JScrollPane scrollPaneNewDossInvLineComComLine4 = new JScrollPane();
			scrollPaneNewDossInvLineComComLine4.setBounds(10, 234, 318, 156);
			panelLineCommentInvLine4.add(scrollPaneNewDossInvLineComComLine4);
			
			final JTextArea textAreaNewDossInvLineComComLine4 = new JTextArea();
			textAreaNewDossInvLineComComLine4.setWrapStyleWord(true);
			textAreaNewDossInvLineComComLine4.setLineWrap(true);
			scrollPaneNewDossInvLineComComLine4.setViewportView(textAreaNewDossInvLineComComLine4);
			
			panelLineCommentInvLine5.setLayout(null);
			panelLineCommentInvLine5.setForeground(Color.BLACK);
			panelLineCommentInvLine5.setBounds(877, 44, 340, 405);
			panelInverse.add(panelLineCommentInvLine5);
			
			JLabel labelNewDossInvLineComDiscrLine5 = new JLabel("Line discription");
			labelNewDossInvLineComDiscrLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComDiscrLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComDiscrLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComDiscrLine5.setBounds(10, 11, 131, 25);
			panelLineCommentInvLine5.add(labelNewDossInvLineComDiscrLine5);
			
			JScrollPane scrollPaneNewDossInvLineComDiscrLine5 = new JScrollPane();
			scrollPaneNewDossInvLineComDiscrLine5.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossInvLineComDiscrLine5.setBounds(10, 36, 320, 156);
			panelLineCommentInvLine5.add(scrollPaneNewDossInvLineComDiscrLine5);
			
			JTextArea textAreaNewDossInvLineComDiscrLine5 = new JTextArea();
			textAreaNewDossInvLineComDiscrLine5.setWrapStyleWord(true);
			textAreaNewDossInvLineComDiscrLine5.setLineWrap(true);
			textAreaNewDossInvLineComDiscrLine5.setEditable(false);
			scrollPaneNewDossInvLineComDiscrLine5.setViewportView(textAreaNewDossInvLineComDiscrLine5);
			
			JLabel labelNewDossInvLineComComLine5 = new JLabel("Line comment");
			labelNewDossInvLineComComLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComComLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComComLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComComLine5.setBounds(12, 209, 106, 25);
			panelLineCommentInvLine5.add(labelNewDossInvLineComComLine5);
			
			JScrollPane scrollPaneNewDossInvLineComComLine5 = new JScrollPane();
			scrollPaneNewDossInvLineComComLine5.setBounds(10, 234, 318, 156);
			panelLineCommentInvLine5.add(scrollPaneNewDossInvLineComComLine5);
			
			final JTextArea textAreaNewDossInvLineComComLine5 = new JTextArea();
			textAreaNewDossInvLineComComLine5.setWrapStyleWord(true);
			textAreaNewDossInvLineComComLine5.setLineWrap(true);
			scrollPaneNewDossInvLineComComLine5.setViewportView(textAreaNewDossInvLineComComLine5);
			
			panelLineCommentInvLine6.setLayout(null);
			panelLineCommentInvLine6.setForeground(Color.BLACK);
			panelLineCommentInvLine6.setBounds(877, 44, 340, 405);
			panelInverse.add(panelLineCommentInvLine6);
			
			JLabel labelNewDossInvLineComDiscrLine6 = new JLabel("Line discription");
			labelNewDossInvLineComDiscrLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComDiscrLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComDiscrLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComDiscrLine6.setBounds(10, 11, 131, 25);
			panelLineCommentInvLine6.add(labelNewDossInvLineComDiscrLine6);
			
			JScrollPane scrollPaneNewDossInvLineComDiscrLine6 = new JScrollPane();
			scrollPaneNewDossInvLineComDiscrLine6.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossInvLineComDiscrLine6.setBounds(10, 36, 320, 156);
			panelLineCommentInvLine6.add(scrollPaneNewDossInvLineComDiscrLine6);
			
			JTextArea textAreaNewDossInvLineComDiscrLine6 = new JTextArea();
			textAreaNewDossInvLineComDiscrLine6.setWrapStyleWord(true);
			textAreaNewDossInvLineComDiscrLine6.setLineWrap(true);
			textAreaNewDossInvLineComDiscrLine6.setEditable(false);
			scrollPaneNewDossInvLineComDiscrLine6.setViewportView(textAreaNewDossInvLineComDiscrLine6);
			
			JLabel labelNewDossInvLineComComLine6 = new JLabel("Line comment");
			labelNewDossInvLineComComLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInvLineComComLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInvLineComComLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInvLineComComLine6.setBounds(12, 209, 106, 25);
			panelLineCommentInvLine6.add(labelNewDossInvLineComComLine6);
			
			JScrollPane scrollPaneNewDossInvLineComComLine6 = new JScrollPane();
			scrollPaneNewDossInvLineComComLine6.setBounds(10, 234, 318, 156);
			panelLineCommentInvLine6.add(scrollPaneNewDossInvLineComComLine6);
			
			final JTextArea textAreaNewDossInvLineComComLine6 = new JTextArea();
			textAreaNewDossInvLineComComLine6.setWrapStyleWord(true);
			textAreaNewDossInvLineComComLine6.setLineWrap(true);
			scrollPaneNewDossInvLineComComLine6.setViewportView(textAreaNewDossInvLineComComLine6);
			
			JLabel labelNewDossInverseImage = new JLabel("Image");
			labelNewDossInverseImage.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInverseImage.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInverseImage.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInverseImage.setBounds(39, 457, 73, 23);
			panelInverse.add(labelNewDossInverseImage);
			
			JScrollPane scrollPane_5 = new JScrollPane();
			scrollPane_5.setBounds(39, 491, 564, 167);
			panelInverse.add(scrollPane_5);
			
			final JTextArea textAreaNewDossInverseImage = new JTextArea();
			textAreaNewDossInverseImage.setWrapStyleWord(true);
			textAreaNewDossInverseImage.setLineWrap(true);
			scrollPane_5.setViewportView(textAreaNewDossInverseImage);
			
			JLabel labelNewDossInverseHexagramComment = new JLabel("Inverse hexagram comment");
			labelNewDossInverseHexagramComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossInverseHexagramComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossInverseHexagramComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossInverseHexagramComment.setBounds(641, 457, 226, 23);
			panelInverse.add(labelNewDossInverseHexagramComment);
			
			JScrollPane scrollPane_6 = new JScrollPane();
			scrollPane_6.setBounds(638, 491, 562, 165);
			panelInverse.add(scrollPane_6);
			
			final JTextArea textAreaNewDossInverseHexagramComment = new JTextArea();
			textAreaNewDossInverseHexagramComment.setWrapStyleWord(true);
			textAreaNewDossInverseHexagramComment.setLineWrap(true);
			scrollPane_6.setViewportView(textAreaNewDossInverseHexagramComment);
			
			
			panelLineCommentInvLine1.setVisible(false);
			panelLineCommentInvLine2.setVisible(false);
			panelLineCommentInvLine3.setVisible(false);
			panelLineCommentInvLine4.setVisible(false);
			panelLineCommentInvLine5.setVisible(false);
			panelLineCommentInvLine6.setVisible(false);
			

			//********************************************************************************************************************************************
			//
			//Initialisierung New Dossier - Reverse
			
			
			panelReverse.add(buttonNewDossRevLineFull6);
			panelReverse.add(buttonNewDossRevLineFull5);
			panelReverse.add(buttonNewDossRevLineFull4);
			panelReverse.add(buttonNewDossRevLineFull3);
			panelReverse.add(buttonNewDossRevLineFull2);
			panelReverse.add(buttonNewDossRevLineFull1);
			panelReverse.add(buttonNewDossRevLineHalf6);
			panelReverse.add(buttonNewDossRevLineHalf5);
			panelReverse.add(buttonNewDossRevLineHalf4);
			panelReverse.add(buttonNewDossRevLineHalf3);
			panelReverse.add(buttonNewDossRevLineHalf2);
			panelReverse.add(buttonNewDossRevLineHalf1);
			
			
			buttonNewDossRevLineFull6.setIcon(lineFull);
			buttonNewDossRevLineFull5.setIcon(lineFull);
			buttonNewDossRevLineFull4.setIcon(lineFull);
			buttonNewDossRevLineFull3.setIcon(lineFull);
			buttonNewDossRevLineFull2.setIcon(lineFull);
			buttonNewDossRevLineFull1.setIcon(lineFull);
			buttonNewDossRevLineHalf6.setIcon(lineUnterbrochen);
			buttonNewDossRevLineHalf5.setIcon(lineUnterbrochen);
			buttonNewDossRevLineHalf4.setIcon(lineUnterbrochen);
			buttonNewDossRevLineHalf3.setIcon(lineUnterbrochen);
			buttonNewDossRevLineHalf2.setIcon(lineUnterbrochen);
			buttonNewDossRevLineHalf1.setIcon(lineUnterbrochen);
			
			buttonNewDossRevLineFull6.setBounds(650, 86, 170, 32);
			buttonNewDossRevLineFull5.setBounds(650, 138, 170, 32);
			buttonNewDossRevLineFull4.setBounds(650, 188, 170, 32);
			buttonNewDossRevLineFull3.setBounds(650, 243, 170, 32);
			buttonNewDossRevLineFull2.setBounds(650, 295, 170, 32);
			buttonNewDossRevLineFull1.setBounds(650, 345, 170, 32);
			buttonNewDossRevLineHalf6.setBounds(650, 86, 170, 32);
			buttonNewDossRevLineHalf5.setBounds(650, 138, 170, 32);
			buttonNewDossRevLineHalf4.setBounds(650, 188, 170, 32);
			buttonNewDossRevLineHalf3.setBounds(650, 243, 170, 32);
			buttonNewDossRevLineHalf2.setBounds(650, 295, 170, 32);
			buttonNewDossRevLineHalf1.setBounds(650, 345, 170, 32);
			
			buttonNewDossRevLineFull6.setVisible(false);
			buttonNewDossRevLineFull5.setVisible(false);
			buttonNewDossRevLineFull4.setVisible(false);
			buttonNewDossRevLineFull3.setVisible(false);
			buttonNewDossRevLineFull2.setVisible(false);
			buttonNewDossRevLineFull1.setVisible(false);
			buttonNewDossRevLineHalf6.setVisible(false);
			buttonNewDossRevLineHalf5.setVisible(false);
			buttonNewDossRevLineHalf4.setVisible(false);
			buttonNewDossRevLineHalf3.setVisible(false);
			buttonNewDossRevLineHalf2.setVisible(false);
			buttonNewDossRevLineHalf1.setVisible(false);
			
			
			JLabel labelNewDossReverseTitleHexa = new JLabel("Reverse Hexagram");
			labelNewDossReverseTitleHexa.setFont(new Font("Tahoma", Font.BOLD, 18));
			labelNewDossReverseTitleHexa.setEnabled(true);
			labelNewDossReverseTitleHexa.setBounds(645, 20, 190, 40);
			panelReverse.add(labelNewDossReverseTitleHexa);
			
			JLabel labelNewDossReverseQuestion = new JLabel("Question");
			labelNewDossReverseQuestion.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossReverseQuestion.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossReverseQuestion.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossReverseQuestion.setBounds(39, 33, 76, 23);
			panelReverse.add(labelNewDossReverseQuestion);
			
			JScrollPane scrollPaneNewDossReverseQuestion = new JScrollPane();
			scrollPaneNewDossReverseQuestion.setBounds(39, 58, 564, 72);
			panelReverse.add(scrollPaneNewDossReverseQuestion);
			
			scrollPaneNewDossReverseQuestion.setViewportView(textAreaNewDossReverseQuest);
			textAreaNewDossReverseQuest.setWrapStyleWord(true);
			textAreaNewDossReverseQuest.setLineWrap(true);
			textAreaNewDossReverseQuest.setEditable(false);
			
			JLabel labelNewDossReverseComment = new JLabel("User comment");
			labelNewDossReverseComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossReverseComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossReverseComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossReverseComment.setBounds(39, 141, 128, 23);
			panelReverse.add(labelNewDossReverseComment);
			
			JScrollPane scrollPaneNewDossReverseComment = new JScrollPane();
			scrollPaneNewDossReverseComment.setBounds(39, 169, 564, 98);
			panelReverse.add(scrollPaneNewDossReverseComment);
			
			textAreaNewDossReverseComment.setWrapStyleWord(true);
			textAreaNewDossReverseComment.setLineWrap(true);
			textAreaNewDossReverseComment.setEditable(false);
			scrollPaneNewDossReverseComment.setViewportView(textAreaNewDossReverseComment);
			
			JLabel labelNewDossReverseJudgement = new JLabel("Judgement");
			labelNewDossReverseJudgement.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossReverseJudgement.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossReverseJudgement.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossReverseJudgement.setBounds(39, 278, 97, 23);
			panelReverse.add(labelNewDossReverseJudgement);
			
			JScrollPane scrollPaneNewDossReverseSynthesis = new JScrollPane();
			scrollPaneNewDossReverseSynthesis.setBounds(39, 312, 564, 92);
			panelReverse.add(scrollPaneNewDossReverseSynthesis);
			
			textAreaNewDossReverseJudgement.setWrapStyleWord(true);
			textAreaNewDossReverseJudgement.setLineWrap(true);
			scrollPaneNewDossReverseSynthesis.setViewportView(textAreaNewDossReverseJudgement);
			
			labelNewDossRevLineComLineNr.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComLineNr.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComLineNr.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComLineNr.setBounds(887, 26, 131, 25);
			panelReverse.add(labelNewDossRevLineComLineNr);
			
			panelLineCommentRevLine1.setLayout(null);
			panelLineCommentRevLine1.setForeground(Color.BLACK);
			panelLineCommentRevLine1.setBounds(877, 44, 340, 401);
			panelReverse.add(panelLineCommentRevLine1);
			
			JLabel labelNewDossRevLineComDiscrLine1 = new JLabel("Line discription");
			labelNewDossRevLineComDiscrLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComDiscrLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComDiscrLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComDiscrLine1.setBounds(10, 11, 131, 25);
			panelLineCommentRevLine1.add(labelNewDossRevLineComDiscrLine1);
			
			JScrollPane scrollPaneNewDossRevLineComDiscrLine1 = new JScrollPane();
			scrollPaneNewDossRevLineComDiscrLine1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossRevLineComDiscrLine1.setBounds(10, 36, 320, 156);
			panelLineCommentRevLine1.add(scrollPaneNewDossRevLineComDiscrLine1);
			
			JTextArea textAreaNewDossRevLineComDiscrLine1 = new JTextArea();
			textAreaNewDossRevLineComDiscrLine1.setWrapStyleWord(true);
			textAreaNewDossRevLineComDiscrLine1.setLineWrap(true);
			textAreaNewDossRevLineComDiscrLine1.setEditable(false);
			scrollPaneNewDossRevLineComDiscrLine1.setViewportView(textAreaNewDossRevLineComDiscrLine1);
			
			JLabel labelNewDossRevLineComComLine1 = new JLabel("Line comment");
			labelNewDossRevLineComComLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComComLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComComLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComComLine1.setBounds(12, 209, 106, 25);
			panelLineCommentRevLine1.add(labelNewDossRevLineComComLine1);
			
			JScrollPane scrollPaneNewDossRevLineComComLine1 = new JScrollPane();
			scrollPaneNewDossRevLineComComLine1.setBounds(10, 234, 318, 156);
			panelLineCommentRevLine1.add(scrollPaneNewDossRevLineComComLine1);
			
			final JTextArea textAreaNewDossRevLineComComLine1 = new JTextArea();
			textAreaNewDossRevLineComComLine1.setWrapStyleWord(true);
			textAreaNewDossRevLineComComLine1.setLineWrap(true);
			scrollPaneNewDossRevLineComComLine1.setViewportView(textAreaNewDossRevLineComComLine1);
			
			panelLineCommentRevLine2.setLayout(null);
			panelLineCommentRevLine2.setForeground(Color.BLACK);
			panelLineCommentRevLine2.setBounds(877, 44, 340, 401);
			panelReverse.add(panelLineCommentRevLine2);
			
			JLabel labelNewDossRevLineComDiscrLine2 = new JLabel("Line discription");
			labelNewDossRevLineComDiscrLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComDiscrLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComDiscrLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComDiscrLine2.setBounds(10, 11, 131, 25);
			panelLineCommentRevLine2.add(labelNewDossRevLineComDiscrLine2);
			
			JScrollPane scrollPaneNewDossRevLineComDiscrLine2 = new JScrollPane();
			scrollPaneNewDossRevLineComDiscrLine2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossRevLineComDiscrLine2.setBounds(10, 36, 320, 156);
			panelLineCommentRevLine2.add(scrollPaneNewDossRevLineComDiscrLine2);
			
			JTextArea textAreaNewDossRevLineComDiscrLine2 = new JTextArea();
			textAreaNewDossRevLineComDiscrLine2.setWrapStyleWord(true);
			textAreaNewDossRevLineComDiscrLine2.setLineWrap(true);
			textAreaNewDossRevLineComDiscrLine2.setEditable(false);
			scrollPaneNewDossRevLineComDiscrLine2.setViewportView(textAreaNewDossRevLineComDiscrLine2);
			
			JLabel labelNewDossRevLineComComLine2 = new JLabel("Line comment");
			labelNewDossRevLineComComLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComComLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComComLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComComLine2.setBounds(12, 209, 106, 25);
			panelLineCommentRevLine2.add(labelNewDossRevLineComComLine2);
			
			JScrollPane scrollPaneNewDossRevLineComComLine2 = new JScrollPane();
			scrollPaneNewDossRevLineComComLine2.setBounds(10, 234, 318, 156);
			panelLineCommentRevLine2.add(scrollPaneNewDossRevLineComComLine2);
			
			final JTextArea textAreaNewDossRevLineComComLine2 = new JTextArea();
			textAreaNewDossRevLineComComLine2.setWrapStyleWord(true);
			textAreaNewDossRevLineComComLine2.setLineWrap(true);
			scrollPaneNewDossRevLineComComLine2.setViewportView(textAreaNewDossRevLineComComLine2);
			
			panelLineCommentRevLine3.setLayout(null);
			panelLineCommentRevLine3.setForeground(Color.BLACK);
			panelLineCommentRevLine3.setBounds(877, 44, 340, 401);
			panelReverse.add(panelLineCommentRevLine3);
			
			JLabel labelNewDossRevLineComDiscrLine3 = new JLabel("Line discription");
			labelNewDossRevLineComDiscrLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComDiscrLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComDiscrLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComDiscrLine3.setBounds(10, 11, 131, 25);
			panelLineCommentRevLine3.add(labelNewDossRevLineComDiscrLine3);
			
			JScrollPane scrollPaneNewDossRevLineComDiscrLine3 = new JScrollPane();
			scrollPaneNewDossRevLineComDiscrLine3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossRevLineComDiscrLine3.setBounds(10, 36, 320, 156);
			panelLineCommentRevLine3.add(scrollPaneNewDossRevLineComDiscrLine3);
			
			JTextArea textAreaNewDossRevLineComDiscrLine3 = new JTextArea();
			textAreaNewDossRevLineComDiscrLine3.setWrapStyleWord(true);
			textAreaNewDossRevLineComDiscrLine3.setLineWrap(true);
			textAreaNewDossRevLineComDiscrLine3.setEditable(false);
			scrollPaneNewDossRevLineComDiscrLine3.setViewportView(textAreaNewDossRevLineComDiscrLine3);
			
			JLabel labelNewDossRevLineComComLine3 = new JLabel("Line comment");
			labelNewDossRevLineComComLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComComLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComComLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComComLine3.setBounds(12, 209, 106, 25);
			panelLineCommentRevLine3.add(labelNewDossRevLineComComLine3);
			
			JScrollPane scrollPaneNewDossRevLineComComLine3 = new JScrollPane();
			scrollPaneNewDossRevLineComComLine3.setBounds(10, 234, 318, 156);
			panelLineCommentRevLine3.add(scrollPaneNewDossRevLineComComLine3);
			
			final JTextArea textAreaNewDossRevLineComComLine3 = new JTextArea();
			textAreaNewDossRevLineComComLine3.setWrapStyleWord(true);
			textAreaNewDossRevLineComComLine3.setLineWrap(true);
			scrollPaneNewDossRevLineComComLine3.setViewportView(textAreaNewDossRevLineComComLine3);
			
			panelLineCommentRevLine4.setLayout(null);
			panelLineCommentRevLine4.setForeground(Color.BLACK);
			panelLineCommentRevLine4.setBounds(877, 44, 340, 401);
			panelReverse.add(panelLineCommentRevLine4);
			
			JLabel labelNewDossRevLineComDiscrLine4 = new JLabel("Line discription");
			labelNewDossRevLineComDiscrLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComDiscrLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComDiscrLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComDiscrLine4.setBounds(10, 11, 131, 25);
			panelLineCommentRevLine4.add(labelNewDossRevLineComDiscrLine4);
			
			JScrollPane scrollPaneNewDossRevLineComDiscrLine4 = new JScrollPane();
			scrollPaneNewDossRevLineComDiscrLine4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossRevLineComDiscrLine4.setBounds(10, 36, 320, 156);
			panelLineCommentRevLine4.add(scrollPaneNewDossRevLineComDiscrLine4);
			
			JTextArea textAreaNewDossRevLineComDiscrLine4 = new JTextArea();
			textAreaNewDossRevLineComDiscrLine4.setWrapStyleWord(true);
			textAreaNewDossRevLineComDiscrLine4.setLineWrap(true);
			textAreaNewDossRevLineComDiscrLine4.setEditable(false);
			scrollPaneNewDossRevLineComDiscrLine4.setViewportView(textAreaNewDossRevLineComDiscrLine4);
			
			JLabel labelNewDossRevLineComComLine4 = new JLabel("Line comment");
			labelNewDossRevLineComComLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComComLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComComLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComComLine4.setBounds(12, 209, 106, 25);
			panelLineCommentRevLine4.add(labelNewDossRevLineComComLine4);
			
			JScrollPane scrollPaneNewDossRevLineComComLine4 = new JScrollPane();
			scrollPaneNewDossRevLineComComLine4.setBounds(10, 234, 318, 156);
			panelLineCommentRevLine4.add(scrollPaneNewDossRevLineComComLine4);
			
			final JTextArea textAreaNewDossRevLineComComLine4 = new JTextArea();
			textAreaNewDossRevLineComComLine4.setWrapStyleWord(true);
			textAreaNewDossRevLineComComLine4.setLineWrap(true);
			scrollPaneNewDossRevLineComComLine4.setViewportView(textAreaNewDossRevLineComComLine4);
			
			panelLineCommentRevLine5.setLayout(null);
			panelLineCommentRevLine5.setForeground(Color.BLACK);
			panelLineCommentRevLine5.setBounds(877, 44, 340, 401);
			panelReverse.add(panelLineCommentRevLine5);
			
			JLabel labelNewDossRevLineComDiscrLine5 = new JLabel("Line discription");
			labelNewDossRevLineComDiscrLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComDiscrLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComDiscrLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComDiscrLine5.setBounds(10, 11, 131, 25);
			panelLineCommentRevLine5.add(labelNewDossRevLineComDiscrLine5);
			
			JScrollPane scrollPaneNewDossRevLineComDiscrLine5 = new JScrollPane();
			scrollPaneNewDossRevLineComDiscrLine5.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossRevLineComDiscrLine5.setBounds(10, 36, 320, 156);
			panelLineCommentRevLine5.add(scrollPaneNewDossRevLineComDiscrLine5);
			
			JTextArea textAreaNewDossRevLineComDiscrLine5 = new JTextArea();
			textAreaNewDossRevLineComDiscrLine5.setWrapStyleWord(true);
			textAreaNewDossRevLineComDiscrLine5.setLineWrap(true);
			textAreaNewDossRevLineComDiscrLine5.setEditable(false);
			scrollPaneNewDossRevLineComDiscrLine5.setViewportView(textAreaNewDossRevLineComDiscrLine5);
			
			JLabel labelNewDossRevLineComComLine5 = new JLabel("Line comment");
			labelNewDossRevLineComComLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComComLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComComLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComComLine5.setBounds(12, 209, 106, 25);
			panelLineCommentRevLine5.add(labelNewDossRevLineComComLine5);
			
			JScrollPane scrollPaneNewDossRevLineComComLine5 = new JScrollPane();
			scrollPaneNewDossRevLineComComLine5.setBounds(10, 234, 318, 156);
			panelLineCommentRevLine5.add(scrollPaneNewDossRevLineComComLine5);
			
			final JTextArea textAreaNewDossRevLineComComLine5 = new JTextArea();
			textAreaNewDossRevLineComComLine5.setWrapStyleWord(true);
			textAreaNewDossRevLineComComLine5.setLineWrap(true);
			scrollPaneNewDossRevLineComComLine5.setViewportView(textAreaNewDossRevLineComComLine5);
			
			panelLineCommentRevLine6.setLayout(null);
			panelLineCommentRevLine6.setForeground(Color.BLACK);
			panelLineCommentRevLine6.setBounds(877, 44, 340, 401);
			panelReverse.add(panelLineCommentRevLine6);
			
			JLabel labelNewDossRevLineComDiscrLine6 = new JLabel("Line discription");
			labelNewDossRevLineComDiscrLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComDiscrLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComDiscrLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComDiscrLine6.setBounds(10, 11, 131, 25);
			panelLineCommentRevLine6.add(labelNewDossRevLineComDiscrLine6);
			
			JScrollPane scrollPaneNewDossRevLineComDiscrLine6 = new JScrollPane();
			scrollPaneNewDossRevLineComDiscrLine6.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossRevLineComDiscrLine6.setBounds(10, 36, 320, 156);
			panelLineCommentRevLine6.add(scrollPaneNewDossRevLineComDiscrLine6);
			
			JTextArea textAreaNewDossRevLineComDiscrLine6 = new JTextArea();
			textAreaNewDossRevLineComDiscrLine6.setWrapStyleWord(true);
			textAreaNewDossRevLineComDiscrLine6.setLineWrap(true);
			textAreaNewDossRevLineComDiscrLine6.setEditable(false);
			scrollPaneNewDossRevLineComDiscrLine6.setViewportView(textAreaNewDossRevLineComDiscrLine6);
			
			JLabel labelNewDossRevLineComComLine6 = new JLabel("Line comment");
			labelNewDossRevLineComComLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossRevLineComComLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossRevLineComComLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossRevLineComComLine6.setBounds(12, 209, 106, 25);
			panelLineCommentRevLine6.add(labelNewDossRevLineComComLine6);
			
			JScrollPane scrollPaneNewDossRevLineComComLine6 = new JScrollPane();
			scrollPaneNewDossRevLineComComLine6.setBounds(10, 234, 318, 156);
			panelLineCommentRevLine6.add(scrollPaneNewDossRevLineComComLine6);
			
			final JTextArea textAreaNewDossRevLineComComLine6 = new JTextArea();
			textAreaNewDossRevLineComComLine6.setWrapStyleWord(true);
			textAreaNewDossRevLineComComLine6.setLineWrap(true);
			scrollPaneNewDossRevLineComComLine6.setViewportView(textAreaNewDossRevLineComComLine6);
			
			JLabel labelNewDossReverseImage = new JLabel("Image");
			labelNewDossReverseImage.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossReverseImage.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossReverseImage.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossReverseImage.setBounds(39, 457, 73, 23);
			panelReverse.add(labelNewDossReverseImage);
			
			JScrollPane scrollPane_7 = new JScrollPane();
			scrollPane_7.setBounds(39, 491, 564, 167);
			panelReverse.add(scrollPane_7);
			
			final JTextArea textAreaNewDossReverseImage = new JTextArea();
			textAreaNewDossReverseImage.setWrapStyleWord(true);
			textAreaNewDossReverseImage.setLineWrap(true);
			scrollPane_7.setViewportView(textAreaNewDossReverseImage);
			
			JLabel labelNewDossReverseHexagramComment = new JLabel("Reverse hexagram comment");
			labelNewDossReverseHexagramComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossReverseHexagramComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossReverseHexagramComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossReverseHexagramComment.setBounds(641, 457, 226, 23);
			panelReverse.add(labelNewDossReverseHexagramComment);
			
			JScrollPane scrollPane_8 = new JScrollPane();
			scrollPane_8.setBounds(638, 491, 562, 165);
			panelReverse.add(scrollPane_8);
			
			final JTextArea textAreaNewDossReverseHexagramComment = new JTextArea();
			textAreaNewDossReverseHexagramComment.setWrapStyleWord(true);
			textAreaNewDossReverseHexagramComment.setLineWrap(true);
			scrollPane_8.setViewportView(textAreaNewDossReverseHexagramComment);
			
			
			panelLineCommentRevLine1.setVisible(false);
			panelLineCommentRevLine2.setVisible(false);
			panelLineCommentRevLine3.setVisible(false);
			panelLineCommentRevLine4.setVisible(false);
			panelLineCommentRevLine5.setVisible(false);
			panelLineCommentRevLine6.setVisible(false);
			
			
		
			//********************************************************************************************************************************************
			//
			//Initialisierung New Dossier - Fuxi
			
			panelFuxi.add(buttonNewDossFuxiLineFull6);
			panelFuxi.add(buttonNewDossFuxiLineFull5);
			panelFuxi.add(buttonNewDossFuxiLineFull4);
			panelFuxi.add(buttonNewDossFuxiLineFull3);
			panelFuxi.add(buttonNewDossFuxiLineFull2);
			panelFuxi.add(buttonNewDossFuxiLineFull1);
			panelFuxi.add(buttonNewDossFuxiLineHalf6);
			panelFuxi.add(buttonNewDossFuxiLineHalf5);
			panelFuxi.add(buttonNewDossFuxiLineHalf4);
			panelFuxi.add(buttonNewDossFuxiLineHalf3);
			panelFuxi.add(buttonNewDossFuxiLineHalf2);
			panelFuxi.add(buttonNewDossFuxiLineHalf1);
			
			buttonNewDossFuxiLineFull6.setIcon(lineFull);
			buttonNewDossFuxiLineFull5.setIcon(lineFull);
			buttonNewDossFuxiLineFull4.setIcon(lineFull);
			buttonNewDossFuxiLineFull3.setIcon(lineFull);
			buttonNewDossFuxiLineFull2.setIcon(lineFull);
			buttonNewDossFuxiLineFull1.setIcon(lineFull);
			buttonNewDossFuxiLineHalf6.setIcon(lineUnterbrochen);
			buttonNewDossFuxiLineHalf5.setIcon(lineUnterbrochen);
			buttonNewDossFuxiLineHalf4.setIcon(lineUnterbrochen);
			buttonNewDossFuxiLineHalf3.setIcon(lineUnterbrochen);
			buttonNewDossFuxiLineHalf2.setIcon(lineUnterbrochen);
			buttonNewDossFuxiLineHalf1.setIcon(lineUnterbrochen);
			
			buttonNewDossFuxiLineFull6.setBounds(650, 86, 170, 32);
			buttonNewDossFuxiLineFull5.setBounds(650, 138, 170, 32);
			buttonNewDossFuxiLineFull4.setBounds(650, 188, 170, 32);
			buttonNewDossFuxiLineFull3.setBounds(650, 243, 170, 32);
			buttonNewDossFuxiLineFull2.setBounds(650, 295, 170, 32);
			buttonNewDossFuxiLineFull1.setBounds(650, 345, 170, 32);
			buttonNewDossFuxiLineHalf6.setBounds(650, 86, 170, 32);
			buttonNewDossFuxiLineHalf5.setBounds(650, 138, 170, 32);
			buttonNewDossFuxiLineHalf4.setBounds(650, 188, 170, 32);
			buttonNewDossFuxiLineHalf3.setBounds(650, 243, 170, 32);
			buttonNewDossFuxiLineHalf2.setBounds(650, 295, 170, 32);
			buttonNewDossFuxiLineHalf1.setBounds(650, 345, 170, 32);
			
			buttonNewDossFuxiLineFull6.setVisible(false);
			buttonNewDossFuxiLineFull5.setVisible(false);
			buttonNewDossFuxiLineFull4.setVisible(false);
			buttonNewDossFuxiLineFull3.setVisible(false);
			buttonNewDossFuxiLineFull2.setVisible(false);
			buttonNewDossFuxiLineFull1.setVisible(false);
			buttonNewDossFuxiLineHalf6.setVisible(false);
			buttonNewDossFuxiLineHalf5.setVisible(false);
			buttonNewDossFuxiLineHalf4.setVisible(false);
			buttonNewDossFuxiLineHalf3.setVisible(false);
			buttonNewDossFuxiLineHalf2.setVisible(false);
			buttonNewDossFuxiLineHalf1.setVisible(false);
			
			JLabel labelNewDossFuxiTitleHexa = new JLabel("Fuxi Hexagram");
			labelNewDossFuxiTitleHexa.setFont(new Font("Tahoma", Font.BOLD, 18));
			labelNewDossFuxiTitleHexa.setEnabled(true);
			labelNewDossFuxiTitleHexa.setBounds(645, 20, 190, 40);
			panelFuxi.add(labelNewDossFuxiTitleHexa);
			
			JLabel labelNewDossFuxiQuestion = new JLabel("Question");
			labelNewDossFuxiQuestion.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiQuestion.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiQuestion.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiQuestion.setBounds(39, 33, 76, 23);
			panelFuxi.add(labelNewDossFuxiQuestion);
			
			JScrollPane scrollPaneNewDossFuxiQuestion = new JScrollPane();
			scrollPaneNewDossFuxiQuestion.setBounds(39, 58, 564, 72);
			panelFuxi.add(scrollPaneNewDossFuxiQuestion);
			
			scrollPaneNewDossFuxiQuestion.setViewportView(textAreaNewDossFuxiQuest);
			textAreaNewDossFuxiQuest.setWrapStyleWord(true);
			textAreaNewDossFuxiQuest.setLineWrap(true);
			textAreaNewDossFuxiQuest.setEditable(false);
			
			JLabel labelNewDossFuxiComment = new JLabel("User comment");
			labelNewDossFuxiComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiComment.setBounds(39, 141, 128, 23);
			panelFuxi.add(labelNewDossFuxiComment);
			
			JScrollPane scrollPaneNewDossFuxiComment = new JScrollPane();
			scrollPaneNewDossFuxiComment.setBounds(39, 169, 564, 98);
			panelFuxi.add(scrollPaneNewDossFuxiComment);
			
			textAreaNewDossFuxiComment.setWrapStyleWord(true);
			textAreaNewDossFuxiComment.setLineWrap(true);
			textAreaNewDossFuxiComment.setEditable(false);
			scrollPaneNewDossFuxiComment.setViewportView(textAreaNewDossFuxiComment);
			
			JLabel labelNewDossFuxiJudgement = new JLabel("Judgement");
			labelNewDossFuxiJudgement.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiJudgement.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiJudgement.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiJudgement.setBounds(39, 278, 97, 23);
			panelFuxi.add(labelNewDossFuxiJudgement);
			
			JScrollPane scrollPaneNewDossFuxiSynthesis = new JScrollPane();
			scrollPaneNewDossFuxiSynthesis.setBounds(39, 312, 564, 92);
			panelFuxi.add(scrollPaneNewDossFuxiSynthesis);
			
			textAreaNewDossFuxiJudgement.setWrapStyleWord(true);
			textAreaNewDossFuxiJudgement.setLineWrap(true);
			scrollPaneNewDossFuxiSynthesis.setViewportView(textAreaNewDossFuxiJudgement);
			
			labelNewDossFuxiLineComLineNr.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComLineNr.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComLineNr.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComLineNr.setBounds(887, 26, 131, 25);
			panelFuxi.add(labelNewDossFuxiLineComLineNr);
			
			panelLineCommentFuxiLine1.setLayout(null);
			panelLineCommentFuxiLine1.setForeground(Color.BLACK);
			panelLineCommentFuxiLine1.setBounds(877, 44, 340, 404);
			panelFuxi.add(panelLineCommentFuxiLine1);
			
			JLabel labelNewDossFuxiLineComDiscrLine1 = new JLabel("Line discription");
			labelNewDossFuxiLineComDiscrLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComDiscrLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComDiscrLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComDiscrLine1.setBounds(10, 11, 131, 25);
			panelLineCommentFuxiLine1.add(labelNewDossFuxiLineComDiscrLine1);
			
			JScrollPane scrollPaneNewDossFuxiLineComDiscrLine1 = new JScrollPane();
			scrollPaneNewDossFuxiLineComDiscrLine1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossFuxiLineComDiscrLine1.setBounds(10, 36, 320, 156);
			panelLineCommentFuxiLine1.add(scrollPaneNewDossFuxiLineComDiscrLine1);
			
			JTextArea textAreaNewDossFuxiLineComDiscrLine1 = new JTextArea();
			textAreaNewDossFuxiLineComDiscrLine1.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComDiscrLine1.setLineWrap(true);
			textAreaNewDossFuxiLineComDiscrLine1.setEditable(false);
			scrollPaneNewDossFuxiLineComDiscrLine1.setViewportView(textAreaNewDossFuxiLineComDiscrLine1);
			
			JLabel labelNewDossFuxiLineComComLine1 = new JLabel("Line comment");
			labelNewDossFuxiLineComComLine1.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComComLine1.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComComLine1.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComComLine1.setBounds(12, 209, 106, 25);
			panelLineCommentFuxiLine1.add(labelNewDossFuxiLineComComLine1);
			
			JScrollPane scrollPaneNewDossFuxiLineComComLine1 = new JScrollPane();
			scrollPaneNewDossFuxiLineComComLine1.setBounds(10, 234, 318, 156);
			panelLineCommentFuxiLine1.add(scrollPaneNewDossFuxiLineComComLine1);
			
			final JTextArea textAreaNewDossFuxiLineComComLine1 = new JTextArea();
			textAreaNewDossFuxiLineComComLine1.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComComLine1.setLineWrap(true);
			scrollPaneNewDossFuxiLineComComLine1.setViewportView(textAreaNewDossFuxiLineComComLine1);
			
			panelLineCommentFuxiLine2.setLayout(null);
			panelLineCommentFuxiLine2.setForeground(Color.BLACK);
			panelLineCommentFuxiLine2.setBounds(877, 44, 340, 404);
			panelFuxi.add(panelLineCommentFuxiLine2);
			
			JLabel labelNewDossFuxiLineComDiscrLine2 = new JLabel("Line discription");
			labelNewDossFuxiLineComDiscrLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComDiscrLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComDiscrLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComDiscrLine2.setBounds(10, 11, 131, 25);
			panelLineCommentFuxiLine2.add(labelNewDossFuxiLineComDiscrLine2);
			
			JScrollPane scrollPaneNewDossFuxiLineComDiscrLine2 = new JScrollPane();
			scrollPaneNewDossFuxiLineComDiscrLine2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossFuxiLineComDiscrLine2.setBounds(10, 36, 320, 156);
			panelLineCommentFuxiLine2.add(scrollPaneNewDossFuxiLineComDiscrLine2);
			
			JTextArea textAreaNewDossFuxiLineComDiscrLine2 = new JTextArea();
			textAreaNewDossFuxiLineComDiscrLine2.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComDiscrLine2.setLineWrap(true);
			textAreaNewDossFuxiLineComDiscrLine2.setEditable(false);
			scrollPaneNewDossFuxiLineComDiscrLine2.setViewportView(textAreaNewDossFuxiLineComDiscrLine2);
			
			JLabel labelNewDossFuxiLineComComLine2 = new JLabel("Line comment");
			labelNewDossFuxiLineComComLine2.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComComLine2.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComComLine2.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComComLine2.setBounds(12, 209, 106, 25);
			panelLineCommentFuxiLine2.add(labelNewDossFuxiLineComComLine2);
			
			JScrollPane scrollPaneNewDossFuxiLineComComLine2 = new JScrollPane();
			scrollPaneNewDossFuxiLineComComLine2.setBounds(10, 234, 318, 156);
			panelLineCommentFuxiLine2.add(scrollPaneNewDossFuxiLineComComLine2);
			
			final JTextArea textAreaNewDossFuxiLineComComLine2 = new JTextArea();
			textAreaNewDossFuxiLineComComLine2.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComComLine2.setLineWrap(true);
			scrollPaneNewDossFuxiLineComComLine2.setViewportView(textAreaNewDossFuxiLineComComLine2);
			
			panelLineCommentFuxiLine3.setLayout(null);
			panelLineCommentFuxiLine3.setForeground(Color.BLACK);
			panelLineCommentFuxiLine3.setBounds(877, 44, 340, 404);
			panelFuxi.add(panelLineCommentFuxiLine3);
			
			JLabel labelNewDossFuxiLineComDiscrLine3 = new JLabel("Line discription");
			labelNewDossFuxiLineComDiscrLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComDiscrLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComDiscrLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComDiscrLine3.setBounds(10, 11, 131, 25);
			panelLineCommentFuxiLine3.add(labelNewDossFuxiLineComDiscrLine3);
			
			JScrollPane scrollPaneNewDossFuxiLineComDiscrLine3 = new JScrollPane();
			scrollPaneNewDossFuxiLineComDiscrLine3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossFuxiLineComDiscrLine3.setBounds(10, 36, 320, 156);
			panelLineCommentFuxiLine3.add(scrollPaneNewDossFuxiLineComDiscrLine3);
			
			JTextArea textAreaNewDossFuxiLineComDiscrLine3 = new JTextArea();
			textAreaNewDossFuxiLineComDiscrLine3.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComDiscrLine3.setLineWrap(true);
			textAreaNewDossFuxiLineComDiscrLine3.setEditable(false);
			scrollPaneNewDossFuxiLineComDiscrLine3.setViewportView(textAreaNewDossFuxiLineComDiscrLine3);
			
			JLabel labelNewDossFuxiLineComComLine3 = new JLabel("Line comment");
			labelNewDossFuxiLineComComLine3.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComComLine3.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComComLine3.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComComLine3.setBounds(12, 209, 106, 25);
			panelLineCommentFuxiLine3.add(labelNewDossFuxiLineComComLine3);
			
			JScrollPane scrollPaneNewDossFuxiLineComComLine3 = new JScrollPane();
			scrollPaneNewDossFuxiLineComComLine3.setBounds(10, 234, 318, 156);
			panelLineCommentFuxiLine3.add(scrollPaneNewDossFuxiLineComComLine3);
			
			final JTextArea textAreaNewDossFuxiLineComComLine3 = new JTextArea();
			textAreaNewDossFuxiLineComComLine3.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComComLine3.setLineWrap(true);
			scrollPaneNewDossFuxiLineComComLine3.setViewportView(textAreaNewDossFuxiLineComComLine3);
			
			panelLineCommentFuxiLine4.setLayout(null);
			panelLineCommentFuxiLine4.setForeground(Color.BLACK);
			panelLineCommentFuxiLine4.setBounds(877, 44, 340, 404);
			panelFuxi.add(panelLineCommentFuxiLine4);
			
			JLabel labelNewDossFuxiLineComDiscrLine4 = new JLabel("Line discription");
			labelNewDossFuxiLineComDiscrLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComDiscrLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComDiscrLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComDiscrLine4.setBounds(10, 11, 131, 25);
			panelLineCommentFuxiLine4.add(labelNewDossFuxiLineComDiscrLine4);
			
			JScrollPane scrollPaneNewDossFuxiLineComDiscrLine4 = new JScrollPane();
			scrollPaneNewDossFuxiLineComDiscrLine4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossFuxiLineComDiscrLine4.setBounds(10, 36, 320, 156);
			panelLineCommentFuxiLine4.add(scrollPaneNewDossFuxiLineComDiscrLine4);
			
			JTextArea textAreaNewDossFuxiLineComDiscrLine4 = new JTextArea();
			textAreaNewDossFuxiLineComDiscrLine4.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComDiscrLine4.setLineWrap(true);
			textAreaNewDossFuxiLineComDiscrLine4.setEditable(false);
			scrollPaneNewDossFuxiLineComDiscrLine4.setViewportView(textAreaNewDossFuxiLineComDiscrLine4);
			
			JLabel labelNewDossFuxiLineComComLine4 = new JLabel("Line comment");
			labelNewDossFuxiLineComComLine4.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComComLine4.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComComLine4.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComComLine4.setBounds(12, 209, 106, 25);
			panelLineCommentFuxiLine4.add(labelNewDossFuxiLineComComLine4);
			
			JScrollPane scrollPaneNewDossFuxiLineComComLine4 = new JScrollPane();
			scrollPaneNewDossFuxiLineComComLine4.setBounds(10, 234, 318, 156);
			panelLineCommentFuxiLine4.add(scrollPaneNewDossFuxiLineComComLine4);
			
			final JTextArea textAreaNewDossFuxiLineComComLine4 = new JTextArea();
			textAreaNewDossFuxiLineComComLine4.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComComLine4.setLineWrap(true);
			scrollPaneNewDossFuxiLineComComLine4.setViewportView(textAreaNewDossFuxiLineComComLine4);
			
			panelLineCommentFuxiLine5.setLayout(null);
			panelLineCommentFuxiLine5.setForeground(Color.BLACK);
			panelLineCommentFuxiLine5.setBounds(877, 44, 340, 404);
			panelFuxi.add(panelLineCommentFuxiLine5);
			
			JLabel labelNewDossFuxiLineComDiscrLine5 = new JLabel("Line discription");
			labelNewDossFuxiLineComDiscrLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComDiscrLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComDiscrLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComDiscrLine5.setBounds(10, 11, 131, 25);
			panelLineCommentFuxiLine5.add(labelNewDossFuxiLineComDiscrLine5);
			
			JScrollPane scrollPaneNewDossFuxiLineComDiscrLine5 = new JScrollPane();
			scrollPaneNewDossFuxiLineComDiscrLine5.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossFuxiLineComDiscrLine5.setBounds(10, 36, 320, 156);
			panelLineCommentFuxiLine5.add(scrollPaneNewDossFuxiLineComDiscrLine5);
			
			JTextArea textAreaNewDossFuxiLineComDiscrLine5 = new JTextArea();
			textAreaNewDossFuxiLineComDiscrLine5.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComDiscrLine5.setLineWrap(true);
			textAreaNewDossFuxiLineComDiscrLine5.setEditable(false);
			scrollPaneNewDossFuxiLineComDiscrLine5.setViewportView(textAreaNewDossFuxiLineComDiscrLine5);
			
			JLabel labelNewDossFuxiLineComComLine5 = new JLabel("Line comment");
			labelNewDossFuxiLineComComLine5.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComComLine5.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComComLine5.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComComLine5.setBounds(12, 209, 106, 25);
			panelLineCommentFuxiLine5.add(labelNewDossFuxiLineComComLine5);
			
			JScrollPane scrollPaneNewDossFuxiLineComComLine5 = new JScrollPane();
			scrollPaneNewDossFuxiLineComComLine5.setBounds(10, 234, 318, 156);
			panelLineCommentFuxiLine5.add(scrollPaneNewDossFuxiLineComComLine5);
			
			final JTextArea textAreaNewDossFuxiLineComComLine5 = new JTextArea();
			textAreaNewDossFuxiLineComComLine5.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComComLine5.setLineWrap(true);
			scrollPaneNewDossFuxiLineComComLine5.setViewportView(textAreaNewDossFuxiLineComComLine5);
			
			panelLineCommentFuxiLine6.setLayout(null);
			panelLineCommentFuxiLine6.setForeground(Color.BLACK);
			panelLineCommentFuxiLine6.setBounds(877, 44, 340, 404);
			panelFuxi.add(panelLineCommentFuxiLine6);
			
			JLabel labelNewDossFuxiLineComDiscrLine6 = new JLabel("Line discription");
			labelNewDossFuxiLineComDiscrLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComDiscrLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComDiscrLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComDiscrLine6.setBounds(10, 11, 131, 25);
			panelLineCommentFuxiLine6.add(labelNewDossFuxiLineComDiscrLine6);
			
			JScrollPane scrollPaneNewDossFuxiLineComDiscrLine6 = new JScrollPane();
			scrollPaneNewDossFuxiLineComDiscrLine6.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneNewDossFuxiLineComDiscrLine6.setBounds(10, 36, 320, 156);
			panelLineCommentFuxiLine6.add(scrollPaneNewDossFuxiLineComDiscrLine6);
			
			JTextArea textAreaNewDossFuxiLineComDiscrLine6 = new JTextArea();
			textAreaNewDossFuxiLineComDiscrLine6.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComDiscrLine6.setLineWrap(true);
			textAreaNewDossFuxiLineComDiscrLine6.setEditable(false);
			scrollPaneNewDossFuxiLineComDiscrLine6.setViewportView(textAreaNewDossFuxiLineComDiscrLine6);
			
			JLabel labelNewDossFuxiLineComComLine6 = new JLabel("Line comment");
			labelNewDossFuxiLineComComLine6.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiLineComComLine6.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiLineComComLine6.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiLineComComLine6.setBounds(12, 209, 106, 25);
			panelLineCommentFuxiLine6.add(labelNewDossFuxiLineComComLine6);
			
			JScrollPane scrollPaneNewDossFuxiLineComComLine6 = new JScrollPane();
			scrollPaneNewDossFuxiLineComComLine6.setBounds(10, 234, 318, 156);
			panelLineCommentFuxiLine6.add(scrollPaneNewDossFuxiLineComComLine6);
			
			final JTextArea textAreaNewDossFuxiLineComComLine6 = new JTextArea();
			textAreaNewDossFuxiLineComComLine6.setWrapStyleWord(true);
			textAreaNewDossFuxiLineComComLine6.setLineWrap(true);
			scrollPaneNewDossFuxiLineComComLine6.setViewportView(textAreaNewDossFuxiLineComComLine6);
			
			JLabel labelNewDossFuxiImage = new JLabel("Image");
			labelNewDossFuxiImage.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiImage.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiImage.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiImage.setBounds(39, 457, 73, 23);
			panelFuxi.add(labelNewDossFuxiImage);
			
			JScrollPane scrollPane_9 = new JScrollPane();
			scrollPane_9.setBounds(39, 491, 564, 167);
			panelFuxi.add(scrollPane_9);
			
			final JTextArea textAreaNewDossFuxiImage = new JTextArea();
			textAreaNewDossFuxiImage.setWrapStyleWord(true);
			textAreaNewDossFuxiImage.setLineWrap(true);
			scrollPane_9.setViewportView(textAreaNewDossFuxiImage);
			
			JLabel labelNewDossFuxiHexagramComment = new JLabel("Fuxi hexagram comment");
			labelNewDossFuxiHexagramComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossFuxiHexagramComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossFuxiHexagramComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossFuxiHexagramComment.setBounds(641, 457, 226, 23);
			panelFuxi.add(labelNewDossFuxiHexagramComment);
			
			JScrollPane scrollPane_10 = new JScrollPane();
			scrollPane_10.setBounds(638, 491, 562, 165);
			panelFuxi.add(scrollPane_10);
			
			final JTextArea textAreaNewDossFuxiHexagramComment = new JTextArea();
			textAreaNewDossFuxiHexagramComment.setWrapStyleWord(true);
			textAreaNewDossFuxiHexagramComment.setLineWrap(true);
			scrollPane_10.setViewportView(textAreaNewDossFuxiHexagramComment);
			
			
			panelLineCommentFuxiLine1.setVisible(false);
			panelLineCommentFuxiLine2.setVisible(false);
			panelLineCommentFuxiLine3.setVisible(false);
			panelLineCommentFuxiLine4.setVisible(false);
			panelLineCommentFuxiLine5.setVisible(false);
			panelLineCommentFuxiLine6.setVisible(false);
			

		
			
			//********************************************************************************************************************************************
			//
			//Initialisierung New Dossier - Line
			
			//HexagrammVisualisierung Linke Seite
			panelLine.add(buttonNewDossLineLineFullLeft6);
			panelLine.add(buttonNewDossLineLineFullLeft5);
			panelLine.add(buttonNewDossLineLineFullLeft4);
			panelLine.add(buttonNewDossLineLineFullLeft3);
			panelLine.add(buttonNewDossLineLineFullLeft2);
			panelLine.add(buttonNewDossLineLineFullLeft1);
			panelLine.add(buttonNewDossLineLineHalfLeft6);
			panelLine.add(buttonNewDossLineLineHalfLeft5);
			panelLine.add(buttonNewDossLineLineHalfLeft4);
			panelLine.add(buttonNewDossLineLineHalfLeft3);
			panelLine.add(buttonNewDossLineLineHalfLeft2);
			panelLine.add(buttonNewDossLineLineHalfLeft1);
			panelLine.add(buttonNewDossLineLineCrossLeft6);
			panelLine.add(buttonNewDossLineLineCrossLeft5);
			panelLine.add(buttonNewDossLineLineCrossLeft4);
			panelLine.add(buttonNewDossLineLineCrossLeft3);
			panelLine.add(buttonNewDossLineLineCrossLeft2);
			panelLine.add(buttonNewDossLineLineCrossLeft1);
			panelLine.add(buttonNewDossLineLineCircleLeft6);
			panelLine.add(buttonNewDossLineLineCircleLeft5);
			panelLine.add(buttonNewDossLineLineCircleLeft4);
			panelLine.add(buttonNewDossLineLineCircleLeft3);
			panelLine.add(buttonNewDossLineLineCircleLeft2);
			panelLine.add(buttonNewDossLineLineCircleLeft1);
			
			buttonNewDossLineLineFullLeft6.setIcon(lineFull);
			buttonNewDossLineLineFullLeft5.setIcon(lineFull);
			buttonNewDossLineLineFullLeft4.setIcon(lineFull);
			buttonNewDossLineLineFullLeft3.setIcon(lineFull);
			buttonNewDossLineLineFullLeft2.setIcon(lineFull);
			buttonNewDossLineLineFullLeft1.setIcon(lineFull);
			buttonNewDossLineLineHalfLeft6.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfLeft5.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfLeft4.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfLeft3.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfLeft2.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfLeft1.setIcon(lineUnterbrochen);
			buttonNewDossLineLineCrossLeft6.setIcon(lineKreuz);
			buttonNewDossLineLineCrossLeft5.setIcon(lineKreuz);
			buttonNewDossLineLineCrossLeft4.setIcon(lineKreuz);
			buttonNewDossLineLineCrossLeft3.setIcon(lineKreuz);
			buttonNewDossLineLineCrossLeft2.setIcon(lineKreuz);
			buttonNewDossLineLineCrossLeft1.setIcon(lineKreuz);
			buttonNewDossLineLineCircleLeft6.setIcon(lineKreis);
			buttonNewDossLineLineCircleLeft5.setIcon(lineKreis);
			buttonNewDossLineLineCircleLeft4.setIcon(lineKreis);
			buttonNewDossLineLineCircleLeft3.setIcon(lineKreis);
			buttonNewDossLineLineCircleLeft2.setIcon(lineKreis);
			buttonNewDossLineLineCircleLeft1.setIcon(lineKreis);
			
			buttonNewDossLineLineFullLeft6.setBounds(137, 280, 170, 32);
			buttonNewDossLineLineFullLeft5.setBounds(137, 332, 170, 32);
			buttonNewDossLineLineFullLeft4.setBounds(137, 382, 170, 32);
			buttonNewDossLineLineFullLeft3.setBounds(137, 437, 170, 32);
			buttonNewDossLineLineFullLeft2.setBounds(137, 489, 170, 32);
			buttonNewDossLineLineFullLeft1.setBounds(137, 539, 170, 32);
			buttonNewDossLineLineHalfLeft6.setBounds(137, 280, 170, 32);
			buttonNewDossLineLineHalfLeft5.setBounds(137, 332, 170, 32);
			buttonNewDossLineLineHalfLeft4.setBounds(137, 382, 170, 32);
			buttonNewDossLineLineHalfLeft3.setBounds(137, 437, 170, 32);
			buttonNewDossLineLineHalfLeft2.setBounds(137, 489, 170, 32);
			buttonNewDossLineLineHalfLeft1.setBounds(137, 539, 170, 32);
			buttonNewDossLineLineCrossLeft6.setBounds(137, 280, 170, 32);
			buttonNewDossLineLineCrossLeft5.setBounds(137, 332, 170, 32);
			buttonNewDossLineLineCrossLeft4.setBounds(137, 382, 170, 32);
			buttonNewDossLineLineCrossLeft3.setBounds(137, 437, 170, 32);
			buttonNewDossLineLineCrossLeft2.setBounds(137, 489, 170, 32);
			buttonNewDossLineLineCrossLeft1.setBounds(137, 539, 170, 32);
			buttonNewDossLineLineCircleLeft6.setBounds(137, 280, 170, 32);
			buttonNewDossLineLineCircleLeft5.setBounds(137, 332, 170, 32);
			buttonNewDossLineLineCircleLeft4.setBounds(137, 382, 170, 32);
			buttonNewDossLineLineCircleLeft3.setBounds(137, 437, 170, 32);
			buttonNewDossLineLineCircleLeft2.setBounds(137, 489, 170, 32);
			buttonNewDossLineLineCircleLeft1.setBounds(137, 539, 170, 32);
			
			buttonNewDossLineLineFullLeft6.setVisible(false);
			buttonNewDossLineLineFullLeft5.setVisible(false);
			buttonNewDossLineLineFullLeft4.setVisible(false);
			buttonNewDossLineLineFullLeft3.setVisible(false);
			buttonNewDossLineLineFullLeft2.setVisible(false);
			buttonNewDossLineLineFullLeft1.setVisible(false);
			buttonNewDossLineLineHalfLeft6.setVisible(false);
			buttonNewDossLineLineHalfLeft5.setVisible(false);
			buttonNewDossLineLineHalfLeft4.setVisible(false);
			buttonNewDossLineLineHalfLeft3.setVisible(false);
			buttonNewDossLineLineHalfLeft2.setVisible(false);
			buttonNewDossLineLineHalfLeft1.setVisible(false);
			buttonNewDossLineLineCrossLeft6.setVisible(false);
			buttonNewDossLineLineCrossLeft5.setVisible(false);
			buttonNewDossLineLineCrossLeft4.setVisible(false);
			buttonNewDossLineLineCrossLeft3.setVisible(false);
			buttonNewDossLineLineCrossLeft2.setVisible(false);
			buttonNewDossLineLineCrossLeft1.setVisible(false);
			buttonNewDossLineLineCircleLeft6.setVisible(false);
			buttonNewDossLineLineCircleLeft5.setVisible(false);
			buttonNewDossLineLineCircleLeft4.setVisible(false);
			buttonNewDossLineLineCircleLeft3.setVisible(false);
			buttonNewDossLineLineCircleLeft2.setVisible(false);
			buttonNewDossLineLineCircleLeft1.setVisible(false);
			
			
			//HexagrammVisualisierung Rechte Seite
			panelLine.add(buttonNewDossLineLineFullRight6);
			panelLine.add(buttonNewDossLineLineFullRight5);
			panelLine.add(buttonNewDossLineLineFullRight4);
			panelLine.add(buttonNewDossLineLineFullRight3);
			panelLine.add(buttonNewDossLineLineFullRight2);
			panelLine.add(buttonNewDossLineLineFullRight1);
			panelLine.add(buttonNewDossLineLineHalfRight6);
			panelLine.add(buttonNewDossLineLineHalfRight5);
			panelLine.add(buttonNewDossLineLineHalfRight4);
			panelLine.add(buttonNewDossLineLineHalfRight3);
			panelLine.add(buttonNewDossLineLineHalfRight2);
			panelLine.add(buttonNewDossLineLineHalfRight1);
			
			buttonNewDossLineLineFullRight6.setIcon(lineFull);
			buttonNewDossLineLineFullRight5.setIcon(lineFull);
			buttonNewDossLineLineFullRight4.setIcon(lineFull);
			buttonNewDossLineLineFullRight3.setIcon(lineFull);
			buttonNewDossLineLineFullRight2.setIcon(lineFull);
			buttonNewDossLineLineFullRight1.setIcon(lineFull);
			buttonNewDossLineLineHalfRight6.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfRight5.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfRight4.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfRight3.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfRight2.setIcon(lineUnterbrochen);
			buttonNewDossLineLineHalfRight1.setIcon(lineUnterbrochen);
			
			buttonNewDossLineLineFullRight6.setBounds(536, 280, 170, 32);
			buttonNewDossLineLineFullRight5.setBounds(536, 332, 170, 32);
			buttonNewDossLineLineFullRight4.setBounds(536, 382, 170, 32);
			buttonNewDossLineLineFullRight3.setBounds(536, 437, 170, 32);
			buttonNewDossLineLineFullRight2.setBounds(536, 489, 170, 32);
			buttonNewDossLineLineFullRight1.setBounds(536, 539, 170, 32);
			buttonNewDossLineLineHalfRight6.setBounds(536, 280, 170, 32);
			buttonNewDossLineLineHalfRight5.setBounds(536, 332, 170, 32);
			buttonNewDossLineLineHalfRight4.setBounds(536, 382, 170, 32);
			buttonNewDossLineLineHalfRight3.setBounds(536, 437, 170, 32);
			buttonNewDossLineLineHalfRight2.setBounds(536, 489, 170, 32);
			buttonNewDossLineLineHalfRight1.setBounds(536, 539, 170, 32);
			
			buttonNewDossLineLineFullRight6.setVisible(false);
			buttonNewDossLineLineFullRight5.setVisible(false);
			buttonNewDossLineLineFullRight4.setVisible(false);
			buttonNewDossLineLineFullRight3.setVisible(false);
			buttonNewDossLineLineFullRight2.setVisible(false);
			buttonNewDossLineLineFullRight1.setVisible(false);
			buttonNewDossLineLineHalfRight6.setVisible(false);
			buttonNewDossLineLineHalfRight5.setVisible(false);
			buttonNewDossLineLineHalfRight4.setVisible(false);
			buttonNewDossLineLineHalfRight3.setVisible(false);
			buttonNewDossLineLineHalfRight2.setVisible(false);
			buttonNewDossLineLineHalfRight1.setVisible(false);
			
			JLabel labelNewDossLineTitleHexa = new JLabel("Line Hexagram");
			labelNewDossLineTitleHexa.setFont(new Font("Tahoma", Font.BOLD, 18));
			labelNewDossLineTitleHexa.setEnabled(true);
			labelNewDossLineTitleHexa.setBounds(645, 20, 190, 40);
			panelLine.add(labelNewDossLineTitleHexa);
			
			JLabel labelNewDossLineComment = new JLabel("User comment");
			labelNewDossLineComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossLineComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossLineComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossLineComment.setBounds(45, 43, 109, 23);
			panelLine.add(labelNewDossLineComment);
			
			JScrollPane scrollPaneNewDossLineComment = new JScrollPane();
			scrollPaneNewDossLineComment.setBounds(45, 71, 345, 98);
			panelLine.add(scrollPaneNewDossLineComment);
			
			textAreaNewDossLineComment.setWrapStyleWord(true);
			textAreaNewDossLineComment.setLineWrap(true);
			textAreaNewDossLineComment.setEditable(false);
			scrollPaneNewDossLineComment.setViewportView(textAreaNewDossLineComment);
			
			JLabel labelNewDossLineSynthesis = new JLabel("Synthesis");
			labelNewDossLineSynthesis.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossLineSynthesis.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossLineSynthesis.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossLineSynthesis.setBounds(430, 43, 65, 23);
			panelLine.add(labelNewDossLineSynthesis);
			
			JScrollPane scrollPaneNewDossLineSynthesis = new JScrollPane();
			scrollPaneNewDossLineSynthesis.setBounds(430, 71, 345, 98);
			panelLine.add(scrollPaneNewDossLineSynthesis);
			
			textAreaNewDossLineSynth.setWrapStyleWord(true);
			textAreaNewDossLineSynth.setLineWrap(true);
			textAreaNewDossLineSynth.setEditable(false);
			scrollPaneNewDossLineSynthesis.setViewportView(textAreaNewDossLineSynth);
			
			JLabel labelNewDossLineImage = new JLabel("Image");
			labelNewDossLineImage.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossLineImage.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossLineImage.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossLineImage.setBounds(817, 43, 125, 23);
			panelLine.add(labelNewDossLineImage);
			
			JScrollPane scrollPaneNewDossLineReflection = new JScrollPane();
			scrollPaneNewDossLineReflection.setBounds(817, 71, 345, 240);
			panelLine.add(scrollPaneNewDossLineReflection);
			
			final JTextArea textAreaNewDossLineImage = new JTextArea();
			scrollPaneNewDossLineReflection.setViewportView(textAreaNewDossLineImage);
			textAreaNewDossLineImage.setWrapStyleWord(true);
			textAreaNewDossLineImage.setLineWrap(true);
			
			JLabel labelNewDossLineHexagramComment = new JLabel("Hexagram Comment");
			labelNewDossLineHexagramComment.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossLineHexagramComment.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossLineHexagramComment.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossLineHexagramComment.setBounds(817, 335, 141, 23);
			panelLine.add(labelNewDossLineHexagramComment);
			
			JScrollPane scrollPaneNewDossLineLineComment = new JScrollPane();
			scrollPaneNewDossLineLineComment.setBounds(817, 358, 345, 240);
			panelLine.add(scrollPaneNewDossLineLineComment);
			
			JTextArea textAreaNewDossLineLineComment = new JTextArea();
			scrollPaneNewDossLineLineComment.setViewportView(textAreaNewDossLineLineComment);
			textAreaNewDossLineLineComment.setWrapStyleWord(true);
			textAreaNewDossLineLineComment.setLineWrap(true);
				
			JLabel labelNewDossLineSituationHexagram = new JLabel("Situation Hexagram");
			labelNewDossLineSituationHexagram.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossLineSituationHexagram.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossLineSituationHexagram.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossLineSituationHexagram.setBounds(147, 232, 147, 23);
			panelLine.add(labelNewDossLineSituationHexagram);
			
			JLabel labelNewDossLineMutationHexagram = new JLabel("Line Mutation");
			labelNewDossLineMutationHexagram.setVerticalAlignment(SwingConstants.TOP);
			labelNewDossLineMutationHexagram.setHorizontalAlignment(SwingConstants.LEFT);
			labelNewDossLineMutationHexagram.setFont(new Font("Tahoma", Font.BOLD, 13));
			labelNewDossLineMutationHexagram.setBounds(559, 232, 147, 23);
			panelLine.add(labelNewDossLineMutationHexagram);
			

		
		//********************************************************************************************************************************************
		//	
		//Initialisierung LinkDossier
		//

					
					
		JLabel labelLinkDossLinkDoss = new JLabel("Link dossier");
		labelLinkDossLinkDoss.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelLinkDossLinkDoss.setForeground(new Color(0, 0, 0));
		labelLinkDossLinkDoss.setBounds(785, 23, 159, 21);
		panelLinkDossier.add(labelLinkDossLinkDoss);			
		
		JLabel labelLinkDossDoss = new JLabel("Dossier");
		labelLinkDossDoss.setVerticalAlignment(SwingConstants.TOP);
		labelLinkDossDoss.setHorizontalAlignment(SwingConstants.LEFT);
		labelLinkDossDoss.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelLinkDossDoss.setBounds(72, 27, 76, 23);
		panelLinkDossier.add(labelLinkDossDoss);
		
		JScrollPane scrollPaneLinkDossDoss = new JScrollPane();
		scrollPaneLinkDossDoss.setBounds(73, 61, 825, 251);
		panelLinkDossier.add(scrollPaneLinkDossDoss);
		scrollPaneLinkDossDoss.setViewportView(listLinkDossDoss);
		
		JLabel labelLinkDossDossData = new JLabel("DataLabel ----");
		scrollPaneLinkDossDoss.setRowHeaderView(labelLinkDossDossData);
		
		JLabel labelLinkDossRelation = new JLabel("Relational dossier");
		labelLinkDossRelation.setVerticalAlignment(SwingConstants.TOP);
		labelLinkDossRelation.setHorizontalAlignment(SwingConstants.LEFT);
		labelLinkDossRelation.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelLinkDossRelation.setBounds(73, 340, 152, 23);
		panelLinkDossier.add(labelLinkDossRelation);
		
		JScrollPane scrollPaneLinkDossRelation = new JScrollPane();
		scrollPaneLinkDossRelation.setBounds(73, 376, 825, 251);
		panelLinkDossier.add(scrollPaneLinkDossRelation);
	
		
		
		scrollPaneLinkDossRelation.setViewportView(listLinkDossRelation);
		
		JLabel labelLinkDossRelationData = new JLabel("DataLabel ----");
		scrollPaneLinkDossRelation.setRowHeaderView(labelLinkDossRelationData);
		
		buttonLinkDossLinkTogether = new JButton("Link together");
		buttonLinkDossLinkTogether.setFont(new Font("Tahoma", Font.BOLD, 13));
		buttonLinkDossLinkTogether.setBounds(73, 655, 129, 23);
		panelLinkDossier.add(buttonLinkDossLinkTogether);

		//********************************************************************************************************************************************
		//
		//Initialisierung Show Dossier
		//
		
		JLabel labelShowDossShowDoss = new JLabel("Show Dossier");
		labelShowDossShowDoss.setForeground(Color.BLACK);
		labelShowDossShowDoss.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelShowDossShowDoss.setBounds(774, 23, 159, 21);
		panelShowDossier.add(labelShowDossShowDoss);
		
		JLabel labelShowDossChose = new JLabel("Choose dossier");
		labelShowDossChose.setVerticalAlignment(SwingConstants.TOP);
		labelShowDossChose.setHorizontalAlignment(SwingConstants.LEFT);
		labelShowDossChose.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelShowDossChose.setBounds(72, 27, 200, 23);
		panelShowDossier.add(labelShowDossChose);
		
		JScrollPane scrollPaneShowDossChoseDoss = new JScrollPane();
		scrollPaneShowDossChoseDoss.setBounds(72, 61, 825, 518);
		panelShowDossier.add(scrollPaneShowDossChoseDoss);
		
		labelShowDossDataRight = new JLabel("Dossier Data -----");
		scrollPaneShowDossChoseDoss.setRowHeaderView(labelShowDossDataRight);
		
		scrollPaneShowDossChoseDoss.setViewportView(listShowDossDataRight);
		
		JButton buttonShowDossShow = new JButton("Show dossier");

		buttonShowDossShow.setFont(new Font("Tahoma", Font.BOLD, 13));
		buttonShowDossShow.setBounds(72, 655, 129, 23);
		panelShowDossier.add(buttonShowDossShow);
		
		JButton buttonShowDossExport = new JButton("export dossier");
		buttonShowDossExport.setFont(new Font("Tahoma", Font.BOLD, 13));
		buttonShowDossExport.setBounds(216, 656, 129, 23);
		panelShowDossier.add(buttonShowDossExport);
		
		
		
		//********************************************************************************************************************************************
		//
		//Initialisierung Edit Profile
		//

		JLabel labelEditProfileEditProfile = new JLabel("Edit Profile");
		labelEditProfileEditProfile.setForeground(Color.BLACK);
		labelEditProfileEditProfile.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelEditProfileEditProfile.setBounds(800, 23, 159, 21);
		panelEditProfile.add(labelEditProfileEditProfile);
		
		JLabel labelEditProfFirstName = new JLabel("First name");
		labelEditProfFirstName.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelEditProfFirstName.setBounds(63, 76, 91, 14);
		panelEditProfile.add(labelEditProfFirstName);
		
		JLabel labelEditProfLastName = new JLabel("Last name");
		labelEditProfLastName.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelEditProfLastName.setBounds(63, 105, 91, 14);
		panelEditProfile.add(labelEditProfLastName);
		
		JLabel labelEditProfEmail = new JLabel("E-Mail");
		labelEditProfEmail.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelEditProfEmail.setBounds(63, 130, 91, 14);
		panelEditProfile.add(labelEditProfEmail);
		
		JLabel labelEditProfPassword = new JLabel("Password");
		labelEditProfPassword.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelEditProfPassword.setBounds(63, 157, 91, 14);
		panelEditProfile.add(labelEditProfPassword);
		
		JLabel labelEditProfConfirm = new JLabel("Confirm");
		labelEditProfConfirm.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelEditProfConfirm.setBounds(63, 182, 91, 14);
		panelEditProfile.add(labelEditProfConfirm);
		
		textFieldEditProfFirstName.setColumns(10);
		textFieldEditProfFirstName.setBounds(164, 76, 126, 20);
		panelEditProfile.add(textFieldEditProfFirstName);
		

		textFieldEditProfLastName.setColumns(10);
		textFieldEditProfLastName.setBounds(164, 105, 126, 20);
		panelEditProfile.add(textFieldEditProfLastName);
		
		textFieldEditProfEmail.setColumns(10);
		textFieldEditProfEmail.setBounds(164, 130, 126, 20);
		panelEditProfile.add(textFieldEditProfEmail);
		
		passwordFieldEditProfPassword.setBounds(164, 157, 126, 20);
		panelEditProfile.add(passwordFieldEditProfPassword);
		
		JPasswordField passwordFieldEditProfConfrim = new JPasswordField();
		passwordFieldEditProfConfrim.setBounds(164, 182, 126, 20);
		panelEditProfile.add(passwordFieldEditProfConfrim);
		
		JButton buttonEditProfileSave = new JButton("Save changes");
		buttonEditProfileSave.setFont(new Font("Tahoma", Font.BOLD, 13));
		buttonEditProfileSave.setBounds(63, 223, 139, 23);
		panelEditProfile.add(buttonEditProfileSave);
		
		JScrollPane TablePane = new JScrollPane();
		TablePane.setBounds(531, 337, 185, -141);
		panelEditProfile.add(TablePane);
		

		
	
		//********************************************************************************************************************************************
		//Listeners
		//********************************************************************************************************************************************
		
	//Visualisierung des Situationshexagrammes
		
		
		/**
		 * Action Listener Gruppe welche anhand des gewählten Wertes in der Combobox direkt
		 * eine Hexagrammlinie Visualisert
		 * 
		 * @author ***anonymized***
		 *
		 *///- initialisierung der Visualisierung SituationsHexagramm, PerspektivHexagramm Linie 1
		comboBoxNewDossSitu1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {

				if(comboBoxNewDossSitu1.getSelectedItem().equals("6")){
					//Buttons für SituationsHexagramm - paelSitu
					buttonNewDossSituLineCross1.setBounds(680, 345, 170, 32);
					buttonNewDossSituLineHalf1.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle1.setBounds(0,0,0,0);
					buttonNewDossSituLineFull1.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft1.setVisible(true);
					buttonNewDossLineLineHalfLeft1.setVisible(false);
					buttonNewDossLineLineCircleLeft1.setVisible(false);
					buttonNewDossLineLineFullLeft1.setVisible(false);
					
					//Buttons für PerspektivHexagram

					buttonNewDossPersLineFull1.setBounds(650, 345, 170, 32);
					buttonNewDossPersLineHalf1.setBounds(0,0,0,0);
				
			
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("7")){
					//Buttons für SituationsHexagramm - paelSitu
					buttonNewDossSituLineCross1.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf1.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle1.setBounds(0,0,0,0);
					buttonNewDossSituLineFull1.setBounds(680, 345, 170, 32);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft1.setVisible(false);
					buttonNewDossLineLineHalfLeft1.setVisible(false);
					buttonNewDossLineLineCircleLeft1.setVisible(false);
					buttonNewDossLineLineFullLeft1.setVisible(true);		
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull1.setBounds(650, 345, 170, 32);
					buttonNewDossPersLineHalf1.setBounds(0,0,0,0);

				}
				
				if(comboBoxNewDossSitu1.getSelectedItem().equals("8")){
					//Buttons für SituationsHexagramm - paelSitu
					buttonNewDossSituLineCross1.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf1.setBounds(680, 345, 170, 32);
					buttonNewDossSituLineCircle1.setBounds(0,0,0,0);
					buttonNewDossSituLineFull1.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft1.setVisible(false);
					buttonNewDossLineLineHalfLeft1.setVisible(true);
					buttonNewDossLineLineCircleLeft1.setVisible(false);
					buttonNewDossLineLineFullLeft1.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull1.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf1.setBounds(650, 345, 170, 32);
					
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("9")){
					//Buttons für SituationsHexagramm - paelSitu
					buttonNewDossSituLineCross1.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf1.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle1.setBounds(680, 345, 170, 32);
					buttonNewDossSituLineFull1.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft1.setVisible(false);
					buttonNewDossLineLineHalfLeft1.setVisible(false);
					buttonNewDossLineLineCircleLeft1.setVisible(true);
					buttonNewDossLineLineFullLeft1.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull1.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf1.setBounds(650, 345, 170, 32);
				
				}
			}
		});

		
		//- initialisierung der Visualisierung SituationsHexagramm, PerspektivHexagramm Linie 2
		comboBoxNewDossSitu2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(comboBoxNewDossSitu2.getSelectedItem().equals("6")){
					//Buttons für SituationsHexagramm - panelSitu
					buttonNewDossSituLineCross2.setBounds(680, 295, 170, 32);
					buttonNewDossSituLineHalf2.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle2.setBounds(0,0,0,0);
					buttonNewDossSituLineFull2.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft2.setVisible(true);		
					buttonNewDossLineLineHalfLeft2.setVisible(false);
					buttonNewDossLineLineCircleLeft2.setVisible(false);
					buttonNewDossLineLineFullLeft2.setVisible(false);
					
					//Buttons für PerspektivHexagram

					buttonNewDossPersLineFull2.setBounds(650, 295, 170, 32);
					buttonNewDossPersLineHalf2.setBounds(0,0,0,0);
					
			
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("7")){
					//Buttons für SituationsHexagramm - panelSitu
					buttonNewDossSituLineCross2.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf2.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle2.setBounds(0,0,0,0);
					buttonNewDossSituLineFull2.setBounds(680, 295, 170, 32);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft2.setVisible(false);		
					buttonNewDossLineLineHalfLeft2.setVisible(false);
					buttonNewDossLineLineCircleLeft2.setVisible(false);
					buttonNewDossLineLineFullLeft2.setVisible(true);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull2.setBounds(650, 295, 170, 32);
					buttonNewDossPersLineHalf2.setBounds(0,0,0,0);

				}
				
				if(comboBoxNewDossSitu2.getSelectedItem().equals("8")){
					//Buttons für SituationsHexagramm - panelSitu
					buttonNewDossSituLineCross2.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf2.setBounds(680, 295, 170, 32);
					buttonNewDossSituLineCircle2.setBounds(0,0,0,0);
					buttonNewDossSituLineFull2.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft2.setVisible(false);		
					buttonNewDossLineLineHalfLeft2.setVisible(true);
					buttonNewDossLineLineCircleLeft2.setVisible(false);
					buttonNewDossLineLineFullLeft2.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull2.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf2.setBounds(650, 295, 170, 32);
					
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("9")){
					//Buttons für SituationsHexagramm - panelSitu
					buttonNewDossSituLineCross2.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf2.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle2.setBounds(680, 295, 170, 32);
					buttonNewDossSituLineFull2.setBounds(0,0,0,0);

					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft2.setVisible(false);		
					buttonNewDossLineLineHalfLeft2.setVisible(false);
					buttonNewDossLineLineCircleLeft2.setVisible(true);
					buttonNewDossLineLineFullLeft2.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull2.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf2.setBounds(650, 295, 170, 32);
				
				}
			}
		});
		
		
		//- initialisierung der Visualisierung SituationsHexagramm, PerspektivHexagramm Linie 3
		comboBoxNewDossSitu3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(comboBoxNewDossSitu3.getSelectedItem().equals("6")){
					//Buttons für SituationsHexagramm - panelSitu
					buttonNewDossSituLineCross3.setBounds(680, 243, 170, 32);
					buttonNewDossSituLineHalf3.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle3.setBounds(0,0,0,0);
					buttonNewDossSituLineFull3.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft3.setVisible(true);		
					buttonNewDossLineLineHalfLeft3.setVisible(false);
					buttonNewDossLineLineCircleLeft3.setVisible(false);
					buttonNewDossLineLineFullLeft3.setVisible(false);
					
					//Buttons für PerspektivHexagram

					buttonNewDossPersLineFull3.setBounds(650, 243, 170, 32);
					buttonNewDossPersLineHalf3.setBounds(0,0,0,0);
					
			
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("7")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross3.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf3.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle3.setBounds(0,0,0,0);
					buttonNewDossSituLineFull3.setBounds(680, 243, 170, 32);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft3.setVisible(false);		
					buttonNewDossLineLineHalfLeft3.setVisible(false);
					buttonNewDossLineLineCircleLeft3.setVisible(false);
					buttonNewDossLineLineFullLeft3.setVisible(true);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull3.setBounds(650, 243, 170, 32);
					buttonNewDossPersLineHalf3.setBounds(0,0,0,0);

				}
				
				if(comboBoxNewDossSitu3.getSelectedItem().equals("8")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross3.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf3.setBounds(680, 243, 170, 32);
					buttonNewDossSituLineCircle3.setBounds(0,0,0,0);
					buttonNewDossSituLineFull3.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft3.setVisible(false);		
					buttonNewDossLineLineHalfLeft3.setVisible(true);
					buttonNewDossLineLineCircleLeft3.setVisible(false);
					buttonNewDossLineLineFullLeft3.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull3.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf3.setBounds(650, 243, 170, 32);
					
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("9")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross3.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf3.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle3.setBounds(680, 243, 170, 32);
					buttonNewDossSituLineFull3.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft3.setVisible(false);		
					buttonNewDossLineLineHalfLeft3.setVisible(false);
					buttonNewDossLineLineCircleLeft3.setVisible(true);
					buttonNewDossLineLineFullLeft3.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull3.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf3.setBounds(650, 243, 170, 32);
				
				}
			}
		});

		
		//- initialisierung der Visualisierung SituationsHexagramm, PerspektivHexagramm Linie 4
		comboBoxNewDossSitu4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
				if(comboBoxNewDossSitu4.getSelectedItem().equals("6")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross4.setBounds(680, 188, 170, 32);
					buttonNewDossSituLineHalf4.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle4.setBounds(0,0,0,0);
					buttonNewDossSituLineFull4.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft4.setVisible(true);		
					buttonNewDossLineLineHalfLeft4.setVisible(false);
					buttonNewDossLineLineCircleLeft4.setVisible(false);
					buttonNewDossLineLineFullLeft4.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull4.setBounds(650, 188, 170, 32);
					buttonNewDossPersLineHalf4.setBounds(0,0,0,0);
							
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("7")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross4.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf4.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle4.setBounds(0,0,0,0);
					buttonNewDossSituLineFull4.setBounds(680, 188, 170, 32);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft4.setVisible(false);		
					buttonNewDossLineLineHalfLeft4.setVisible(false);
					buttonNewDossLineLineCircleLeft4.setVisible(false);
					buttonNewDossLineLineFullLeft4.setVisible(true);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull4.setBounds(650, 188, 170, 32);
					buttonNewDossPersLineHalf4.setBounds(0,0,0,0);

				}
				
				if(comboBoxNewDossSitu4.getSelectedItem().equals("8")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross4.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf4.setBounds(680, 188, 170, 32);
					buttonNewDossSituLineCircle4.setBounds(0,0,0,0);
					buttonNewDossSituLineFull4.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft4.setVisible(false);		
					buttonNewDossLineLineHalfLeft4.setVisible(true);
					buttonNewDossLineLineCircleLeft4.setVisible(false);
					buttonNewDossLineLineFullLeft4.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull4.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf4.setBounds(650, 188, 170, 32);
					
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("9")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross4.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf4.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle4.setBounds(680, 188, 170, 32);
					buttonNewDossSituLineFull4.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft4.setVisible(false);		
					buttonNewDossLineLineHalfLeft4.setVisible(false);
					buttonNewDossLineLineCircleLeft4.setVisible(true);
					buttonNewDossLineLineFullLeft4.setVisible(false);

					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull4.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf4.setBounds(650, 188, 170, 32);
					
				}
			}
		});


		//- initialisierung der Visualisierung SituationsHexagramm, PerspektivHexagramm Linie 5
		comboBoxNewDossSitu5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboBoxNewDossSitu5.getSelectedItem().equals("6")){
					//Buttons für Situationshexagramm
					buttonNewDossSituLineCross5.setBounds(680, 138, 170, 32);
					buttonNewDossSituLineHalf5.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle5.setBounds(0,0,0,0);
					buttonNewDossSituLineFull5.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft5.setVisible(true);		
					buttonNewDossLineLineHalfLeft5.setVisible(false);
					buttonNewDossLineLineCircleLeft5.setVisible(false);
					buttonNewDossLineLineFullLeft5.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull5.setBounds(650, 138, 170, 32);
					buttonNewDossPersLineHalf5.setBounds(0,0,0,0);
					
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("7")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross5.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf5.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle5.setBounds(0,0,0,0);
					buttonNewDossSituLineFull5.setBounds(680, 138, 170, 32);					
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft5.setVisible(false);		
					buttonNewDossLineLineHalfLeft5.setVisible(false);
					buttonNewDossLineLineCircleLeft5.setVisible(false);
					buttonNewDossLineLineFullLeft5.setVisible(true);
								
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull5.setBounds(650, 138, 170, 32);
					buttonNewDossPersLineHalf5.setBounds(0,0,0,0);

				}
				
				if(comboBoxNewDossSitu5.getSelectedItem().equals("8")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross5.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf5.setBounds(680, 138, 170, 32);
					buttonNewDossSituLineCircle5.setBounds(0,0,0,0);
					buttonNewDossSituLineFull5.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft5.setVisible(false);		
					buttonNewDossLineLineHalfLeft5.setVisible(true);
					buttonNewDossLineLineCircleLeft5.setVisible(false);
					buttonNewDossLineLineFullLeft5.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull5.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf5.setBounds(650, 138, 170, 32);
					
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("9")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross5.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf5.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle5.setBounds(680, 138, 170, 32);
					buttonNewDossSituLineFull5.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft5.setVisible(false);		
					buttonNewDossLineLineHalfLeft5.setVisible(false);
					buttonNewDossLineLineCircleLeft5.setVisible(true);
					buttonNewDossLineLineFullLeft5.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull5.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf5.setBounds(650, 138, 170, 32);
				
				}
			}
		});
		

		//- initialisierung der Visualisierung SituationsHexagramm, PerspektivHexagramm Linie 6	
		comboBoxNewDossSitu6.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {

				if(comboBoxNewDossSitu6.getSelectedItem().equals("6")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross6.setBounds(680, 86, 170, 32);
					buttonNewDossSituLineHalf6.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle6.setBounds(0,0,0,0);
					buttonNewDossSituLineFull6.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft6.setVisible(true);		
					buttonNewDossLineLineHalfLeft6.setVisible(false);
					buttonNewDossLineLineCircleLeft6.setVisible(false);
					buttonNewDossLineLineFullLeft6.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull6.setBounds(650, 86, 170, 32);
					buttonNewDossPersLineHalf6.setBounds(0,0,0,0);
					
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("7")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross6.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf6.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle6.setBounds(0,0,0,0);
					buttonNewDossSituLineFull6.setBounds(680, 86, 170, 32);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft6.setVisible(false);		
					buttonNewDossLineLineHalfLeft6.setVisible(false);
					buttonNewDossLineLineCircleLeft6.setVisible(false);
					buttonNewDossLineLineFullLeft6.setVisible(true);
					
					//Buttons für PerspektivHexagram					
					buttonNewDossPersLineFull6.setBounds(650, 86, 170, 32);
					buttonNewDossPersLineHalf6.setBounds(0,0,0,0);

				}
				
				if(comboBoxNewDossSitu6.getSelectedItem().equals("8")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross6.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf6.setBounds(680, 86, 170, 32);
					buttonNewDossSituLineCircle6.setBounds(0,0,0,0);
					buttonNewDossSituLineFull6.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft6.setVisible(false);		
					buttonNewDossLineLineHalfLeft6.setVisible(true);
					buttonNewDossLineLineCircleLeft6.setVisible(false);
					buttonNewDossLineLineFullLeft6.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull6.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf6.setBounds(650, 86, 170, 32);

				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("9")){
					//Buttons für SituationsHexagramm
					buttonNewDossSituLineCross6.setBounds(0,0,0,0);
					buttonNewDossSituLineHalf6.setBounds(0,0,0,0);
					buttonNewDossSituLineCircle6.setBounds(680, 86, 170, 32);
					buttonNewDossSituLineFull6.setBounds(0,0,0,0);
					
					//Buttons für SituationsHexagramm - panelLine
					buttonNewDossLineLineCrossLeft6.setVisible(false);		
					buttonNewDossLineLineHalfLeft6.setVisible(false);
					buttonNewDossLineLineCircleLeft6.setVisible(true);
					buttonNewDossLineLineFullLeft6.setVisible(false);
					
					//Buttons für PerspektivHexagram
					buttonNewDossPersLineFull6.setBounds(0,0,0,0);
					buttonNewDossPersLineHalf6.setBounds(650, 86, 170, 32);
					
				}
			}
			
			
		
		});
		

		
		//********************************************************************************************************************************************	
		//Visualisierung LineHexagramm - NewDoss Line
		
		//--> siehe	String[] visualizeLineHexa = sendMutationString.split("");
		
		//Line1
		buttonNewDossLineLineCrossLeft1.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {	
//				
				//Standardsetzung Sitationshexagramm
				if(comboBoxNewDossSitu1.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight1.setVisible(true);
					buttonNewDossLineLineHalfRight1.setVisible(false);
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight1.setVisible(true);
					buttonNewDossLineLineHalfRight1.setVisible(false);
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight1.setVisible(false);
					buttonNewDossLineLineHalfRight1.setVisible(true);
					
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight1.setVisible(false);
					buttonNewDossLineLineHalfRight1.setVisible(true);
				}
				//Line2
				if(comboBoxNewDossSitu2.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight2.setVisible(false);
					buttonNewDossLineLineHalfRight2.setVisible(true);
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight2.setVisible(true);
					buttonNewDossLineLineHalfRight2.setVisible(false);
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight2.setVisible(false);
					buttonNewDossLineLineHalfRight2.setVisible(true);
					
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight2.setVisible(true);
					buttonNewDossLineLineHalfRight2.setVisible(false);
				}
				//Line3
				if(comboBoxNewDossSitu3.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight3.setVisible(false);
					buttonNewDossLineLineHalfRight3.setVisible(true);
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight3.setVisible(true);
					buttonNewDossLineLineHalfRight3.setVisible(false);
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight3.setVisible(false);
					buttonNewDossLineLineHalfRight3.setVisible(true);
					
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight3.setVisible(true);
					buttonNewDossLineLineHalfRight3.setVisible(false);
				}
				//Line4
				if(comboBoxNewDossSitu4.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight4.setVisible(false);
					buttonNewDossLineLineHalfRight4.setVisible(true);
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight4.setVisible(true);
					buttonNewDossLineLineHalfRight4.setVisible(false);
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight4.setVisible(false);
					buttonNewDossLineLineHalfRight4.setVisible(true);
					
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight4.setVisible(true);
					buttonNewDossLineLineHalfRight4.setVisible(false);
				}
				//Line5
				if(comboBoxNewDossSitu5.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight5.setVisible(false);
					buttonNewDossLineLineHalfRight5.setVisible(true);
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight5.setVisible(true);
					buttonNewDossLineLineHalfRight5.setVisible(false);
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight5.setVisible(false);
					buttonNewDossLineLineHalfRight5.setVisible(true);
					
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight5.setVisible(true);
					buttonNewDossLineLineHalfRight5.setVisible(false);
				}
				//Line6
				if(comboBoxNewDossSitu6.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight6.setVisible(false);
					buttonNewDossLineLineHalfRight6.setVisible(true);
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight6.setVisible(true);
					buttonNewDossLineLineHalfRight6.setVisible(false);
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight6.setVisible(false);
					buttonNewDossLineLineHalfRight6.setVisible(true);
					
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight6.setVisible(true);
					buttonNewDossLineLineHalfRight6.setVisible(false);
				}

			}
		});	
		
		//Line2
		buttonNewDossLineLineCrossLeft1.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {	
//				
				//Standardsetzung Sitationshexagramm
				if(comboBoxNewDossSitu1.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight1.setVisible(false);
					buttonNewDossLineLineHalfRight1.setVisible(true);
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight1.setVisible(true);
					buttonNewDossLineLineHalfRight1.setVisible(false);
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight1.setVisible(false);
					buttonNewDossLineLineHalfRight1.setVisible(true);
					
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight1.setVisible(true);
					buttonNewDossLineLineHalfRight1.setVisible(false);
				}
				//Line2
				if(comboBoxNewDossSitu2.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight2.setVisible(true);
					buttonNewDossLineLineHalfRight2.setVisible(false);
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight2.setVisible(true);
					buttonNewDossLineLineHalfRight2.setVisible(false);
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight2.setVisible(false);
					buttonNewDossLineLineHalfRight2.setVisible(true);
					
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight2.setVisible(false);
					buttonNewDossLineLineHalfRight2.setVisible(true);
				}
				//Line3
				if(comboBoxNewDossSitu3.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight3.setVisible(false);
					buttonNewDossLineLineHalfRight3.setVisible(true);
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight3.setVisible(true);
					buttonNewDossLineLineHalfRight3.setVisible(false);
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight3.setVisible(false);
					buttonNewDossLineLineHalfRight3.setVisible(true);
					
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight3.setVisible(true);
					buttonNewDossLineLineHalfRight3.setVisible(false);
				}
				//Line4
				if(comboBoxNewDossSitu4.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight4.setVisible(false);
					buttonNewDossLineLineHalfRight4.setVisible(true);
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight4.setVisible(true);
					buttonNewDossLineLineHalfRight4.setVisible(false);
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight4.setVisible(false);
					buttonNewDossLineLineHalfRight4.setVisible(true);
					
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight4.setVisible(true);
					buttonNewDossLineLineHalfRight4.setVisible(false);
				}
				//Line5
				if(comboBoxNewDossSitu5.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight5.setVisible(false);
					buttonNewDossLineLineHalfRight5.setVisible(true);
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight5.setVisible(true);
					buttonNewDossLineLineHalfRight5.setVisible(false);
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight5.setVisible(false);
					buttonNewDossLineLineHalfRight5.setVisible(true);
					
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight5.setVisible(true);
					buttonNewDossLineLineHalfRight5.setVisible(false);
				}
				//Line6
				if(comboBoxNewDossSitu6.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight6.setVisible(false);
					buttonNewDossLineLineHalfRight6.setVisible(true);
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight6.setVisible(true);
					buttonNewDossLineLineHalfRight6.setVisible(false);
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight6.setVisible(false);
					buttonNewDossLineLineHalfRight6.setVisible(true);
					
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight6.setVisible(true);
					buttonNewDossLineLineHalfRight6.setVisible(false);
				}
							
			}
		});	

		
		
		//Line3
		buttonNewDossLineLineCrossLeft1.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {	
//				
				//Standardsetzung Sitationshexagramm
				if(comboBoxNewDossSitu1.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight1.setVisible(false);
					buttonNewDossLineLineHalfRight1.setVisible(true);
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight1.setVisible(true);
					buttonNewDossLineLineHalfRight1.setVisible(false);
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight1.setVisible(false);
					buttonNewDossLineLineHalfRight1.setVisible(true);
					
				}
				if(comboBoxNewDossSitu1.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight1.setVisible(true);
					buttonNewDossLineLineHalfRight1.setVisible(false);
				}
				//Line2
				if(comboBoxNewDossSitu2.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight2.setVisible(false);
					buttonNewDossLineLineHalfRight2.setVisible(true);
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight2.setVisible(true);
					buttonNewDossLineLineHalfRight2.setVisible(false);
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight2.setVisible(false);
					buttonNewDossLineLineHalfRight2.setVisible(true);
					
				}
				if(comboBoxNewDossSitu2.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight2.setVisible(true);
					buttonNewDossLineLineHalfRight2.setVisible(false);
				}
				//Line3
				if(comboBoxNewDossSitu3.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight3.setVisible(true);
					buttonNewDossLineLineHalfRight3.setVisible(false);
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight3.setVisible(true);
					buttonNewDossLineLineHalfRight3.setVisible(false);
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight3.setVisible(false);
					buttonNewDossLineLineHalfRight3.setVisible(true);
					
				}
				if(comboBoxNewDossSitu3.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight3.setVisible(false);
					buttonNewDossLineLineHalfRight3.setVisible(true);
				}
				//Line4
				if(comboBoxNewDossSitu4.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight4.setVisible(false);
					buttonNewDossLineLineHalfRight4.setVisible(true);
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight4.setVisible(true);
					buttonNewDossLineLineHalfRight4.setVisible(false);
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight4.setVisible(false);
					buttonNewDossLineLineHalfRight4.setVisible(true);
					
				}
				if(comboBoxNewDossSitu4.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight4.setVisible(true);
					buttonNewDossLineLineHalfRight4.setVisible(false);
				}
				//Line5
				if(comboBoxNewDossSitu5.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight5.setVisible(false);
					buttonNewDossLineLineHalfRight5.setVisible(true);
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight5.setVisible(true);
					buttonNewDossLineLineHalfRight5.setVisible(false);
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight5.setVisible(false);
					buttonNewDossLineLineHalfRight5.setVisible(true);
					
				}
				if(comboBoxNewDossSitu5.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight5.setVisible(true);
					buttonNewDossLineLineHalfRight5.setVisible(false);
				}
				//Line6
				if(comboBoxNewDossSitu6.getSelectedItem().equals("6")){
					buttonNewDossLineLineFullRight6.setVisible(false);
					buttonNewDossLineLineHalfRight6.setVisible(true);
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("7")){
					buttonNewDossLineLineFullRight6.setVisible(true);
					buttonNewDossLineLineHalfRight6.setVisible(false);
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("8")){
					buttonNewDossLineLineFullRight6.setVisible(false);
					buttonNewDossLineLineHalfRight6.setVisible(true);
					
				}
				if(comboBoxNewDossSitu6.getSelectedItem().equals("9")){
					buttonNewDossLineLineFullRight6.setVisible(true);
					buttonNewDossLineLineHalfRight6.setVisible(false);
				}

			}
		});	

		
		

	
		
		
		
		
		//********************************************************************************************************************************************	
		//Hinzufügen des Mouselisteners auf tabbedPaneLeft
		tabbedPaneLeft.addMouseListener( new MouseListener() {
			
			public void mouseReleased(MouseEvent arg0) {
			}
			/**
			 * Abgleich der Textareas in Maske NewDoss
			 * @author ***anonymized***
			 */
			public void mousePressed(MouseEvent arg0) {	
				//Abgleichen der TextArea-Question
				if(!(textAreaNewDossSituQuest.getText().equals(textAreaNewDossPerspQuest.getText()))){
					//Bereinigen der TextAreas
					textAreaNewDossPerspQuest.setText("");
					textAreaNewDossNucQuest.setText("");
					textAreaNewDossInverseQuest.setText("");
					textAreaNewDossReverseQuest.setText("");
					textAreaNewDossFuxiQuest.setText("");
					
					//Abgleichen der Textares mit "textAreaNewDossSituQuest"
					textAreaNewDossPerspQuest.append(textAreaNewDossSituQuest.getText());		
					textAreaNewDossNucQuest.append(textAreaNewDossSituQuest.getText());
					textAreaNewDossInverseQuest.append(textAreaNewDossSituQuest.getText());
					textAreaNewDossReverseQuest.append(textAreaNewDossSituQuest.getText());
					textAreaNewDossFuxiQuest.append(textAreaNewDossSituQuest.getText());
				}
				
				if(!(textAreaNewDossSituUserComment.getText().equals(textAreaNewDossPerspComment.getText()))){
					//Bereinigen der TextAreas
					textAreaNewDossPerspComment.setText("");
					textAreaNewDossNucComment.setText("");
					textAreaNewDossInverseUserComment.setText("");
					textAreaNewDossReverseComment.setText("");
					textAreaNewDossFuxiComment.setText("");
					textAreaNewDossLineComment.setText("");
					
					//Abgleichen der Textareas mit "textAreaNewDossSituComment"
					textAreaNewDossPerspComment.append(textAreaNewDossSituUserComment.getText());
					textAreaNewDossNucComment.append(textAreaNewDossSituUserComment.getText());
					textAreaNewDossInverseUserComment.append(textAreaNewDossSituUserComment.getText());
					textAreaNewDossReverseComment.append(textAreaNewDossSituUserComment.getText());
					textAreaNewDossFuxiComment.append(textAreaNewDossSituUserComment.getText());
					textAreaNewDossLineComment.append(textAreaNewDossSituUserComment.getText());
				}
				
			}
			
			public void mouseExited(MouseEvent arg0) {
			}

			/**
			 * Sendet einen Identifikatoren String zur Datenbank welche für
			 * 		jedes Hexagramm einen String wert Retourniert
			 * @author ***anonymized***
			 */
			public void mouseEntered(MouseEvent arg0) {

				String var1 = comboBoxNewDossSitu1.getSelectedItem().toString();
				String var2 = comboBoxNewDossSitu2.getSelectedItem().toString();
				String var3 = comboBoxNewDossSitu3.getSelectedItem().toString();
				String var4 = comboBoxNewDossSitu4.getSelectedItem().toString();
				String var5 = comboBoxNewDossSitu5.getSelectedItem().toString();
				String var6 = comboBoxNewDossSitu6.getSelectedItem().toString();

				// setzen der HexagramVariable auf "0" oder "1"
				if (var1.equals("6") || var1.equals("7")) {

					var1 = "1";
				}
				if (var1.equals("8") || var1.equals("9")) {
					var1 = "0";
				}

				if (var2.equals("6") || var2.equals("7")) {

					var2 = "1";
				}
				if (var2.equals("8") || var2.equals("9")) {
					var2 = "0";
				}

				if (var3.equals("6") || var3.equals("7")) {

					var3 = "1";
				}
				if (var3.equals("8") || var3.equals("9")) {
					var3 = "0";
				}

				if (var4.equals("6") || var4.equals("7")) {

					var4 = "1";
				}
				if (var4.equals("8") || var4.equals("9")) {
					var4 = "0";
				}

				if (var5.equals("6") || var5.equals("7")) {

					var5 = "1";
				}
				if (var5.equals("8") || var5.equals("9")) {
					var5 = "0";
				}

				if (var6.equals("6") || var6.equals("7")) {

					var6 = "1";
				}
				if (var6.equals("8") || var6.equals("9")) {
					var6 = "0";
				}

				// Zusammensetzen der einzelnen Combobox verten
				perspectiveID = var1.concat(var2).concat(var3).concat(var4).concat(var5).concat(var6);

				// Kontrolle ob sich die MutationsString verändert hatt 
				//- Wenn ja, übermitteln
				if (!(hexagrammID.equals(perspectiveID))) {
					hexagrammID = perspectiveID;

					if (hexagrammID.length() == 6) {	
						Hexagram getHexa = new Hexagram(LocalRepository.currentUser.getUserID(), "GETHEXAGRAM", hexagrammID);
						Hexagram[] hexaIDs = (Hexagram[]) (getHexa.writeXMLMessage(getHexa));
						recieveFuxi = hexaIDs[0].getFuxiID();
						recieveNuc = hexaIDs[0].getNuclearID();
						recieveInv = hexaIDs[0].getInverseID();
						recieveRev = hexaIDs[0].getReverseID();
					}

				}
				//Aufsplitten der Variabel für Line HexagrammVisualisierung
				visualizeLineHexa = hexagrammID.split("");

				
			}
			
			/**
			 * Vsualisierung der Hexagramme per Mouselistener anhand der Identifikatoren( recieveNuc,recieveInv, recieveRev...) welche aus der
			 * Datenbank stammen
			 * 
			 * @author ***anonymized***
			 */
			public void mouseClicked(MouseEvent arg0) {
				if (hexagrammID.length() == 6) {

				//	*****************************Update der Visualisierung des Nuclear Hexagram *****************************
				
					//Aufsplitten des erhaltenen Identifiers
					String[] visualizeNuclearHexa = recieveNuc.split("");
					
					//Line1
					if(visualizeNuclearHexa[1].equals("1")){
						buttonNewDossNucLineFull1.setVisible(true);
						buttonNewDossNucLineHalf1.setVisible(false);
					}
					if(visualizeNuclearHexa[1].equals("0")){
						buttonNewDossNucLineFull1.setVisible(false);
						buttonNewDossNucLineHalf1.setVisible(true);
					}
					//Line2
					if(visualizeNuclearHexa[2].equals("1")){
						buttonNewDossNucLineFull2.setVisible(true);
						buttonNewDossNucLineHalf2.setVisible(false);
					}
					if(visualizeNuclearHexa[2].equals("0")){
						buttonNewDossNucLineFull2.setVisible(false);
						buttonNewDossNucLineHalf2.setVisible(true);
					}
					//Line3
					if(visualizeNuclearHexa[3].equals("1")){
						buttonNewDossNucLineFull3.setVisible(true);
						buttonNewDossNucLineHalf3.setVisible(false);
					}
					if(visualizeNuclearHexa[3].equals("0")){
						buttonNewDossNucLineFull3.setVisible(false);
						buttonNewDossNucLineHalf3.setVisible(true);
					}
					//Line4
					if(visualizeNuclearHexa[4].equals("1")){
						buttonNewDossNucLineFull4.setVisible(true);
						buttonNewDossNucLineHalf4.setVisible(false);
					}
					if(visualizeNuclearHexa[4].equals("0")){
						buttonNewDossNucLineFull4.setVisible(false);
						buttonNewDossNucLineHalf4.setVisible(true);
					}
					//Line5
					if(visualizeNuclearHexa[5].equals("1")){
						buttonNewDossNucLineFull5.setVisible(true);
						buttonNewDossNucLineHalf5.setVisible(false);
					}
					if(visualizeNuclearHexa[5].equals("0")){
						buttonNewDossNucLineFull5.setVisible(false);
						buttonNewDossNucLineHalf5.setVisible(true);
					}
					//Line6
					if(visualizeNuclearHexa[6].equals("1")){
						buttonNewDossNucLineFull6.setVisible(true);
						buttonNewDossNucLineHalf6.setVisible(false);
					}
					if(visualizeNuclearHexa[6].equals("0")){
						buttonNewDossNucLineFull6.setVisible(false);
						buttonNewDossNucLineHalf6.setVisible(true);
					}
					
					
				//*****************************	Update der Visualisierung des Inverse Hexagrammes*****************************
					//Aufsplitten des erhaltenen Identifiers
					String[] visualizeInverseHexa = recieveInv.split("");
					
					//Line1
					if(visualizeInverseHexa[1].equals("1")){
						buttonNewDossInverLineFull1.setVisible(true);
						buttonNewDossInverLineHalf1.setVisible(false);
					}
					if(visualizeInverseHexa[1].equals("0")){
						buttonNewDossInverLineFull1.setVisible(false);
						buttonNewDossInverLineHalf1.setVisible(true);
					}
					//Line2
					if(visualizeInverseHexa[2].equals("1")){
						buttonNewDossInverLineFull2.setVisible(true);
						buttonNewDossInverLineHalf2.setVisible(false);
					}
					if(visualizeInverseHexa[2].equals("0")){
						buttonNewDossInverLineFull2.setVisible(false);
						buttonNewDossInverLineHalf2.setVisible(true);
					}
					//Line3
					if(visualizeInverseHexa[3].equals("1")){
						buttonNewDossInverLineFull3.setVisible(true);
						buttonNewDossInverLineHalf3.setVisible(false);
					}
					if(visualizeInverseHexa[3].equals("0")){
						buttonNewDossInverLineFull3.setVisible(false);
						buttonNewDossInverLineHalf3.setVisible(true);
					}
					//Line4
					if(visualizeInverseHexa[4].equals("1")){
						buttonNewDossInverLineFull4.setVisible(true);
						buttonNewDossInverLineHalf4.setVisible(false);
					}
					if(visualizeInverseHexa[4].equals("0")){
						buttonNewDossInverLineFull4.setVisible(false);
						buttonNewDossInverLineHalf4.setVisible(true);
					}
					//Line5
					if(visualizeInverseHexa[5].equals("1")){
						buttonNewDossInverLineFull5.setVisible(true);
						buttonNewDossInverLineHalf5.setVisible(false);
					}
					if(visualizeInverseHexa[5].equals("0")){
						buttonNewDossInverLineFull5.setVisible(false);
						buttonNewDossInverLineHalf5.setVisible(true);
					}
					//Line6
					if(visualizeInverseHexa[6].equals("1")){
						buttonNewDossInverLineFull6.setVisible(true);
						buttonNewDossInverLineHalf6.setVisible(false);
					}
					if(visualizeInverseHexa[6].equals("0")){
						buttonNewDossInverLineFull6.setVisible(false);
						buttonNewDossInverLineHalf6.setVisible(true);
					}
					
//					*****************************Update der Visualisierung des Reverse Hexagrammes*****************************
					String[] visualizeReverseHexa = recieveRev.split("");
					
					//Line1
					if(visualizeReverseHexa[1].equals("1")){
						buttonNewDossRevLineFull1.setVisible(true);
						buttonNewDossRevLineHalf1.setVisible(false);
					}
					if(visualizeReverseHexa[1].equals("0")){
						buttonNewDossRevLineFull1.setVisible(false);
						buttonNewDossRevLineHalf1.setVisible(true);
					}
					//Line2
					if(visualizeReverseHexa[2].equals("1")){
						buttonNewDossRevLineFull2.setVisible(true);
						buttonNewDossRevLineHalf2.setVisible(false);
					}
					if(visualizeReverseHexa[2].equals("0")){
						buttonNewDossRevLineFull2.setVisible(false);
						buttonNewDossRevLineHalf2.setVisible(true);
					}
					//Line3
					if(visualizeReverseHexa[3].equals("1")){
						buttonNewDossRevLineFull3.setVisible(true);
						buttonNewDossRevLineHalf3.setVisible(false);
					}
					if(visualizeReverseHexa[3].equals("0")){
						buttonNewDossRevLineFull3.setVisible(false);
						buttonNewDossRevLineHalf3.setVisible(true);
					}
					//Line4
					if(visualizeReverseHexa[4].equals("1")){
						buttonNewDossRevLineFull4.setVisible(true);
						buttonNewDossRevLineHalf4.setVisible(false);
					}
					if(visualizeReverseHexa[4].equals("0")){
						buttonNewDossRevLineFull4.setVisible(false);
						buttonNewDossRevLineHalf4.setVisible(true);
					}
					//Line5
					if(visualizeReverseHexa[5].equals("1")){
						buttonNewDossRevLineFull5.setVisible(true);
						buttonNewDossRevLineHalf5.setVisible(false);
					}
					if(visualizeReverseHexa[5].equals("0")){
						buttonNewDossRevLineFull5.setVisible(false);
						buttonNewDossRevLineHalf5.setVisible(true);
					}
					//Line6
					if(visualizeReverseHexa[6].equals("1")){
						buttonNewDossRevLineFull6.setVisible(true);
						buttonNewDossRevLineHalf6.setVisible(false);
					}
					if(visualizeReverseHexa[6].equals("0")){
						buttonNewDossRevLineFull6.setVisible(false);
						buttonNewDossRevLineHalf6.setVisible(true);
					}
					
					
//					*****************************Update der Visualisierung des Fuxi Hexagrammes*****************************
					String[] visualizeFuxiHexa = recieveFuxi.split("");
					
					//Line1
					if(visualizeFuxiHexa[1].equals("1")){
						buttonNewDossFuxiLineFull1.setVisible(true);
						buttonNewDossFuxiLineHalf1.setVisible(false);
					}
					if(visualizeFuxiHexa[1].equals("0")){
						buttonNewDossFuxiLineFull1.setVisible(false);
						buttonNewDossFuxiLineHalf1.setVisible(true);
					}
					//Line2
					if(visualizeFuxiHexa[2].equals("1")){
						buttonNewDossFuxiLineFull2.setVisible(true);
						buttonNewDossFuxiLineHalf2.setVisible(false);
					}
					if(visualizeFuxiHexa[2].equals("0")){
						buttonNewDossFuxiLineFull2.setVisible(false);
						buttonNewDossFuxiLineHalf2.setVisible(true);
					}
					//Line3
					if(visualizeFuxiHexa[3].equals("1")){
						buttonNewDossFuxiLineFull3.setVisible(true);
						buttonNewDossFuxiLineHalf3.setVisible(false);
					}
					if(visualizeFuxiHexa[3].equals("0")){
						buttonNewDossFuxiLineFull3.setVisible(false);
						buttonNewDossFuxiLineHalf3.setVisible(true);
					}
					//Line4
					if(visualizeFuxiHexa[4].equals("1")){
						buttonNewDossFuxiLineFull4.setVisible(true);
						buttonNewDossFuxiLineHalf4.setVisible(false);
					}
					if(visualizeFuxiHexa[4].equals("0")){
						buttonNewDossFuxiLineFull4.setVisible(false);
						buttonNewDossFuxiLineHalf4.setVisible(true);
					}
					//Line5
					if(visualizeFuxiHexa[5].equals("1")){
						buttonNewDossFuxiLineFull5.setVisible(true);
						buttonNewDossFuxiLineHalf5.setVisible(false);
					}
					if(visualizeFuxiHexa[5].equals("0")){
						buttonNewDossFuxiLineFull5.setVisible(false);
						buttonNewDossFuxiLineHalf5.setVisible(true);
					}
					//Line6
					if(visualizeFuxiHexa[6].equals("1")){
						buttonNewDossFuxiLineFull6.setVisible(true);
						buttonNewDossFuxiLineHalf6.setVisible(false);
					}
					if(visualizeFuxiHexa[6].equals("0")){
						buttonNewDossFuxiLineFull6.setVisible(false);
						buttonNewDossFuxiLineHalf6.setVisible(true);
					}
		
				}
			}
		});
		
	//********************************************************************************************************************************************
		//Action Listener zum Setzten der LinienKommentarBoxen - SituationsHexagramm
		
		buttonNewDossSituLineFull6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(true);
				labelNewDossSituLineComLineNr.setText("Line 6");
				
			}
		});
		buttonNewDossSituLineCross6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(true);
				labelNewDossSituLineComLineNr.setText("Line 6");
				
			}
		});
		buttonNewDossSituLineCircle6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(true);
				labelNewDossSituLineComLineNr.setText("Line 6");
				
			}
		});
		buttonNewDossSituLineHalf6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(true);
				labelNewDossSituLineComLineNr.setText("Line 6");
				
			}
		});

		buttonNewDossSituLineFull5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(true);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 5");
				
			}
		});
		buttonNewDossSituLineHalf5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(true);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 5");
				
			}
		});
		buttonNewDossSituLineCross5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(true);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 5");
				
			}
		});
		buttonNewDossSituLineCircle5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(true);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossSituLineFull4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(true);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 4");
				
			}
		});
		buttonNewDossSituLineHalf4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(true);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 4");
				
			}
		});
		buttonNewDossSituLineCross4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(true);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 4");
				
			}
		});
		buttonNewDossSituLineCircle4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(true);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 4");
				
			}
		});

		
		
		buttonNewDossSituLineFull3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(true);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 3");
				
			}
		});
		buttonNewDossSituLineHalf3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(true);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 3");
				
			}
		});
		buttonNewDossSituLineCross3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(true);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 3");
				
			}
		});
		buttonNewDossSituLineCircle3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(true);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 3");
				
			}
		});
		
		
		buttonNewDossSituLineFull2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(true);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 2");
				
			}
		});
		buttonNewDossSituLineHalf2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(true);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 2");
				
			}
		});
		buttonNewDossSituLineCross2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(true);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 2");
				
			}
		});
		buttonNewDossSituLineCircle2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(false);
				panelLineCommentSituLine2.setVisible(true);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossSituLineFull1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(true);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 1");
				
			}
		});
		buttonNewDossSituLineHalf1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(true);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 1");
				
			}
		});
		buttonNewDossSituLineCross1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(true);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 1");
				
			}
		});
		buttonNewDossSituLineCircle1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentSituLine1.setVisible(true);
				panelLineCommentSituLine2.setVisible(false);
				panelLineCommentSituLine3.setVisible(false);
				panelLineCommentSituLine4.setVisible(false);
				panelLineCommentSituLine5.setVisible(false);
				panelLineCommentSituLine6.setVisible(false);
				labelNewDossSituLineComLineNr.setText("Line 1");
				
			}
		});
		//********************************************************************************************************************************************
				//Action Listener zum Setzten der LinienKommentarBoxen - Perspektivhexagramm
		
		buttonNewDossPersLineFull1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(true);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossPersLineHalf1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(true);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossPersLineFull2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(false);
				panelLineCommentPerspLine2.setVisible(true);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossPersLineHalf2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(false);
				panelLineCommentPerspLine2.setVisible(true);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossPersLineFull3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(false);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(true);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossPersLineHalf3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(true);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(true);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossPersLineFull4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(false);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(true);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossPersLineHalf4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(false);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(true);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossPersLineFull5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(false);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(true);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossPersLineHalf5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(false);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(true);
				panelLineCommentPerspLine6.setVisible(false);
				labelNewDossPerspLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossPersLineFull6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(false);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(true);
				labelNewDossPerspLineComLineNr.setText("Line 6");
				
			}
		});
		
		buttonNewDossPersLineHalf6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentPerspLine1.setVisible(false);
				panelLineCommentPerspLine2.setVisible(false);
				panelLineCommentPerspLine3.setVisible(false);
				panelLineCommentPerspLine4.setVisible(false);
				panelLineCommentPerspLine5.setVisible(false);
				panelLineCommentPerspLine6.setVisible(true);
				labelNewDossPerspLineComLineNr.setText("Line 6");
				
			}
		});
		
		
		//********************************************************************************************************************************************
		//Action Listener zum Setzten der LinienKommentarBoxen - NuklearHexagramm

		buttonNewDossNucLineFull1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(true);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossNucLineHalf1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(true);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossNucLineFull2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(false);
				panelLineCommentNucLine2.setVisible(true);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossNucLineHalf2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(false);
				panelLineCommentNucLine2.setVisible(true);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossNucLineFull3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(false);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(true);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossNucLineHalf3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(true);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(true);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossNucLineFull4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(false);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(true);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossNucLineHalf4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(false);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(true);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossNucLineFull5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(false);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(true);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossNucLineHalf5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(false);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(true);
				panelLineCommentNucLine6.setVisible(false);
				labelNewDossNucLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossNucLineFull6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(false);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(true);
				labelNewDossNucLineComLineNr.setText("Line 6");
				
			}
		});
		
		buttonNewDossNucLineHalf6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentNucLine1.setVisible(false);
				panelLineCommentNucLine2.setVisible(false);
				panelLineCommentNucLine3.setVisible(false);
				panelLineCommentNucLine4.setVisible(false);
				panelLineCommentNucLine5.setVisible(false);
				panelLineCommentNucLine6.setVisible(true);
				labelNewDossNucLineComLineNr.setText("Line 6");
				
			}
		});
		
		
		//********************************************************************************************************************************************
		//Action Listener zum Setzten der LinienKommentarBoxen - InverseHexagramm

		buttonNewDossInverLineFull1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(true);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossInverLineHalf1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(true);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossInverLineFull2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(false);
				panelLineCommentInvLine2.setVisible(true);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossInverLineHalf2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(false);
				panelLineCommentInvLine2.setVisible(true);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossInverLineFull3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(false);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(true);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossInverLineHalf3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(true);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(true);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossInverLineFull4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(false);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(true);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossInverLineHalf4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(false);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(true);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossInverLineFull5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(false);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(true);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossInverLineHalf5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(false);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(true);
				panelLineCommentInvLine6.setVisible(false);
				labelNewDossInvLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossInverLineFull6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(false);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(true);
				labelNewDossInvLineComLineNr.setText("Line 6");
				
			}
		});
		
		buttonNewDossInverLineHalf6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentInvLine1.setVisible(false);
				panelLineCommentInvLine2.setVisible(false);
				panelLineCommentInvLine3.setVisible(false);
				panelLineCommentInvLine4.setVisible(false);
				panelLineCommentInvLine5.setVisible(false);
				panelLineCommentInvLine6.setVisible(true);
				labelNewDossInvLineComLineNr.setText("Line 6");
				
			}
		});

		//********************************************************************************************************************************************
		//Action Listener zum Setzten der LinienKommentarBoxen - ReverseHexagramm

		buttonNewDossRevLineFull1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(true);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossRevLineHalf1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(true);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossRevLineFull2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(false);
				panelLineCommentRevLine2.setVisible(true);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossRevLineHalf2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(false);
				panelLineCommentRevLine2.setVisible(true);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossRevLineFull3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(false);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(true);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossRevLineHalf3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(true);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(true);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossRevLineFull4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(false);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(true);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossRevLineHalf4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(false);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(true);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossRevLineFull5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(false);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(true);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossRevLineHalf5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(false);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(true);
				panelLineCommentRevLine6.setVisible(false);
				labelNewDossRevLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossRevLineFull6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(false);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(true);
				labelNewDossRevLineComLineNr.setText("Line 6");
				
			}
		});
		
		buttonNewDossRevLineHalf6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentRevLine1.setVisible(false);
				panelLineCommentRevLine2.setVisible(false);
				panelLineCommentRevLine3.setVisible(false);
				panelLineCommentRevLine4.setVisible(false);
				panelLineCommentRevLine5.setVisible(false);
				panelLineCommentRevLine6.setVisible(true);
				labelNewDossRevLineComLineNr.setText("Line 6");
				
			}
		});
		
		//********************************************************************************************************************************************
		//Action Listener zum Setzten der LinienKommentarBoxen - Fuxi

		buttonNewDossFuxiLineFull1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(true);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossFuxiLineHalf1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(true);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 1");
				
			}
		});
		
		buttonNewDossFuxiLineFull2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(false);
				panelLineCommentFuxiLine2.setVisible(true);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossFuxiLineHalf2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(false);
				panelLineCommentFuxiLine2.setVisible(true);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 2");
				
			}
		});
		
		buttonNewDossFuxiLineFull3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(false);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(true);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossFuxiLineHalf3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(true);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(true);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 3");
				
			}
		});
		
		buttonNewDossFuxiLineFull4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(false);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(true);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossFuxiLineHalf4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(false);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(true);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 4");
				
			}
		});
		
		buttonNewDossFuxiLineFull5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(false);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(true);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossFuxiLineHalf5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(false);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(true);
				panelLineCommentFuxiLine6.setVisible(false);
				labelNewDossFuxiLineComLineNr.setText("Line 5");
				
			}
		});
		
		buttonNewDossFuxiLineFull6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(false);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(true);
				labelNewDossFuxiLineComLineNr.setText("Line 6");
				
			}
		});
		
		
		
		buttonNewDossFuxiLineHalf6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLineCommentFuxiLine1.setVisible(false);
				panelLineCommentFuxiLine2.setVisible(false);
				panelLineCommentFuxiLine3.setVisible(false);
				panelLineCommentFuxiLine4.setVisible(false);
				panelLineCommentFuxiLine5.setVisible(false);
				panelLineCommentFuxiLine6.setVisible(true);
				labelNewDossFuxiLineComLineNr.setText("Line 6");
				
			}
		});
		
		//********************************************************************************************************************************************
		/**
		 * Setzen des 
		 */
		buttonNewDossSituEditCate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				try {
					EditCategoryGUI dialog = new EditCategoryGUI();
					dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}
		});
		
		//********************************************************************************************************************************************
		/**
		 * erstellen des Objekts Dossier per Klick auf Save
		 * @author ***anonymized***
		 */
		
		buttonNewDossSituSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//UserID on Top of Class

				//HexagramID = sendMutationString
				int categoryID = 0;
				int dossierID = 0;
				//String categoryTitle = comboBoxNewDossSituCategory.getSelectedItem().toString();//CategoryTitle muss noch übermittelt werden!!
				String date = labelNewDossSituDateData.getText();
				String question = textAreaNewDossSituQuest.getText();
				String userComment = textAreaNewDossSituUserComment.getText();
				
	
				String fuxiComment = textAreaNewDossFuxiHexagramComment.getText();
				String fuxiJudgement	= textAreaNewDossFuxiJudgement.getText();
				String fuxiImage =		textAreaNewDossFuxiImage.getText();
				String fuxiLineComment1 = textAreaNewDossFuxiLineComComLine1.getText();
				String fuxiLineComment2 = textAreaNewDossFuxiLineComComLine2.getText();
				String fuxiLineComment3 = textAreaNewDossFuxiLineComComLine3.getText();
				String fuxiLineComment4 = textAreaNewDossFuxiLineComComLine4.getText();
				String fuxiLineComment5 = textAreaNewDossFuxiLineComComLine5.getText();
				String fuxiLineComment6 = textAreaNewDossFuxiLineComComLine6.getText();
				
				String perspectiveComment = textAreaNewDossPerspHexagramComment.getText();
				String perspectiveJudgement	= textAreaNewDossPerspJudgement.getText();
				String perspecitveImage =		textAreaNewDossPerspImage.getText();
				String perspectiveCommentLine1 = textAreaNewDossPerspLineComComLine1.getText();
				String perspectiveCommentLine2 = textAreaNewDossPerspLineComComLine2.getText();
				String perspectiveCommentLine3 = textAreaNewDossPerspLineComComLine3.getText();
				String perspectiveCommentLine4 = textAreaNewDossPerspLineComComLine4.getText();
				String perspectiveCommentLine5 = textAreaNewDossPerspLineComComLine5.getText();
				String perspectiveCommentLine6 = textAreaNewDossPerspLineComComLine6.getText();
				
				String situationComment = textAreaNewDossSituHexagramComment.getText();
				String situationJudgement	= textAreaNewDossSituJudgement.getText();
				String situationImage =		textAreaNewDossSituImage.getText();
				String situationLineComment1 = textAreaNewDossSituLineComComLine1.getText();
				String situationLineComment2 = textAreaNewDossSituLineComComLine2.getText();
				String situationLineComment3 = textAreaNewDossSituLineComComLine3.getText();
				String situationLineComment4 = textAreaNewDossSituLineComComLine4.getText();
				String situationLineComment5 = textAreaNewDossSituLineComComLine5.getText();
				String situationLineComment6 = textAreaNewDossSituLineComComLine6.getText();
				
				String nuclearComment = textAreaNewDossNucHexagramComment.getText();
				String nuclearJudgement	= textAreaNewDossNucJudgement.getText();
				String nuclearImage =		textAreaNewDossNucImage.getText();
				String nuclearLineComment1 = textAreaNewDossNucLineComComLine1.getText();
				String nuclearLineComment2 = textAreaNewDossNucLineComComLine2.getText();
				String nuclearLineComment3 = textAreaNewDossNucLineComComLine3.getText();
				String nuclearLineComment4 = textAreaNewDossNucLineComComLine4.getText();
				String nuclearLineComment5 = textAreaNewDossNucLineComComLine5.getText();
				String nuclearLineComment6 = textAreaNewDossNucLineComComLine6.getText();
				
				String inverseComment = textAreaNewDossInverseHexagramComment.getText();
				String inverseJudgement	= textAreaNewDossInverseJudgement.getText();
				String inverseImage =		textAreaNewDossInverseImage.getText();
				String inverseLineComment1 = textAreaNewDossInvLineComComLine1.getText();
				String inverseLineComment2 = textAreaNewDossInvLineComComLine2.getText();
				String inverseLineComment3 = textAreaNewDossInvLineComComLine3.getText();
				String inverseLineComment4 = textAreaNewDossInvLineComComLine4.getText();
				String inverseLineComment5 = textAreaNewDossInvLineComComLine5.getText();
				String inverseLineComment6 = textAreaNewDossInvLineComComLine6.getText();
				
				String reverseComment = textAreaNewDossReverseHexagramComment.getText();
				String reverseJudgement	= textAreaNewDossReverseJudgement.getText();
				String reverseImage =		textAreaNewDossReverseImage.getText();
				String reverseLineComment1 = textAreaNewDossFuxiLineComComLine1.getText();
				String reverseLineComment2 = textAreaNewDossFuxiLineComComLine2.getText();
				String reverseLineComment3 = textAreaNewDossFuxiLineComComLine3.getText();
				String reverseLineComment4 = textAreaNewDossFuxiLineComComLine4.getText();
				String reverseLineComment5 = textAreaNewDossFuxiLineComComLine5.getText();
				String reverseLineComment6 = textAreaNewDossFuxiLineComComLine6.getText();


				
				Dossier dossDefaultObject = new Dossier(
						LocalRepository.currentUser.getUserID(), 
						"NEWDOSSIER",
						hexagrammID, 
						categoryID, 
						dossierID, 
						date,
						question, 
						userComment, 
						perspectiveComment,
						perspectiveJudgement,
						perspecitveImage,
						perspectiveCommentLine1, 
						perspectiveCommentLine2,
						perspectiveCommentLine3, 
						perspectiveCommentLine4,
						perspectiveCommentLine5, 
						perspectiveCommentLine6,
						situationComment, 
						situationJudgement,
						situationImage,
						situationLineComment1,
						situationLineComment2, 
						situationLineComment3,
						situationLineComment4, 
						situationLineComment5,
						situationLineComment6, 
						fuxiComment, 
						fuxiJudgement,
						fuxiImage,
						fuxiLineComment1,
						fuxiLineComment2, 
						fuxiLineComment3, 
						fuxiLineComment4,
						fuxiLineComment5, 
						fuxiLineComment6, 
						nuclearComment,
						nuclearJudgement,
						nuclearImage,
						nuclearLineComment1, 
						nuclearLineComment2,
						nuclearLineComment3, 
						nuclearLineComment4,
						nuclearLineComment5, 
						nuclearLineComment6,
						inverseComment,
						inverseJudgement,
						inverseImage,
						inverseLineComment1,
						inverseLineComment2, 
						inverseLineComment3,
						inverseLineComment4, 
						inverseLineComment5,
						inverseLineComment6, 
						reverseComment,
						reverseJudgement,
						reverseImage,
						reverseLineComment1,
						reverseLineComment2,
						reverseLineComment3, 
						reverseLineComment4,
						reverseLineComment5, 
						reverseLineComment6);

				//Übermitteln des Kompletten DossierObject
				dossDefaultObject.writeXMLMessage(dossDefaultObject);
				
			}
		});
		
		
		//********************************************************************************************************************************************
		/**
		 * erstellen des Benutzerobjektes per Klick auf Save	changes
		 * @author ***anonymized***
		 */
		
		final String password = new String(passwordFieldEditProfPassword.getPassword());
		
		buttonEditProfileSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				User userUpdate = new User(LocalRepository.port, LocalRepository.host, LocalRepository.currentUser.getUserID(), textFieldEditProfEmail.getText(),
											passwordFieldEditProfPassword.getPassword().toString(), textFieldEditProfFirstName.getText(), textFieldEditProfLastName.getText(), "UPDATEUSER", true);
				
				userUpdate.writeXMLMessage(userUpdate);
			}
		});
		
		//********************************************************************************************************************************************
		/**
		 * Ausgabe der Verlinkten Dossier in UnlinkDoss - Füllen der JList listUnlinkDossDataRight
		 * @author ***anonymized***
		 */
		
		listUnlinkDossDataLeft.addMouseListener(new MouseListener() {
			

			public void mouseReleased(MouseEvent arg0) {
			}
			public void mousePressed(MouseEvent arg0) {
			}
			public void mouseExited(MouseEvent arg0) {
			}
			public void mouseEntered(MouseEvent arg0) {
			}

			public void mouseClicked(MouseEvent arg0) {
				String selectedValue =	listUnlinkDossDataLeft.getSelectedValue().toString();
				
				//druchsuchen der dossierQuestionRepository nach dem selecteValue von listUnlinkDossDataLeft
				for(int i = 0; i < dossierQuestionRepository.toArray().length; i++){
					if(LocalRepository.dossiers[i].getQuestion().equals(selectedValue)){
					
						DossierLink getLinks = new DossierLink("GETDOSSIERLINKS", 	LocalRepository.dossiers[i].getDossierID(), 0);
						LocalRepository.dossierlinks = (DossierLink[])getLinks.writeXMLMessage(getLinks);
						
						//Füllen des Lokalen Speichers mit den DossierLinkFragen
						for(int j = 0; j < LocalRepository.dossiers.length; j++){
							if(LocalRepository.dossierlinks[j].getLinkedDossierID() == LocalRepository.dossiers[j].getDossierID()){
								dossierLinkRepository.add(LocalRepository.dossiers[j].getQuestion());
							}		
						}
						listUnlinkDossDataRight.setListData(dossierLinkRepository.toArray());
					}
				}
			}
		});
		
		
		//********************************************************************************************************************************************
		/**
		 * Verlinken zweier Dossier per klick auf buttonLinkDossLinkTogether
		 * @author ***anonymized***
		 */
			buttonLinkDossLinkTogether.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				for(int i = 0; i < LocalRepository.dossiers.length; i++){			
					if(LocalRepository.dossiers[i].getQuestion().equals(listLinkDossDoss.getSelectedValue().toString())){
						
						for(int j = 0; j < LocalRepository.dossiers.length; j++){
							if(LocalRepository.dossiers[j].getQuestion().equals(listLinkDossRelation.getSelectedValue().toString())){

								DossierLink link = new DossierLink("LINKDOSSIER", LocalRepository.dossiers[i].getDossierID(), LocalRepository.dossiers[j].getDossierID());
								link.writeXMLMessage(link);
							}						
						}	
					}									
				}
			}
		});
		
			
		//********************************************************************************************************************************************
			/**
			 *Setzten des CurrenDossier Dokuments auf das passende Dossier welches in 
			 *listShowDossDataRight ausgwählt ist und zeigt die enthaltenen Elemente
			 *in der Ansicht new Dossier an
			 *
			 * @author ***anonymized***
			 */
		buttonShowDossShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//Setzten des CurrenDossier Dokuments auf das passende Dossier welches in 
				//listShowDossDataRight ausgwählt ist
				for(int i =0; i < LocalRepository.dossiers.length; i++){
					if(LocalRepository.dossiers[i].getQuestion().equals(listShowDossDataRight.getSelectedValue().toString())){
						LocalRepository.currentDossier = LocalRepository.dossiers[i];
					}		
				}
				
				textAreaNewDossSituQuest.setText(LocalRepository.currentDossier.getQuestion());
				textAreaNewDossSituUserComment.setText(LocalRepository.currentDossier.getUserComment());
				textAreaNewDossSituHexagramComment.setText(LocalRepository.currentDossier.getSituationComment());
				
				for(int i = 0; i < categoryTitleRepository.toArray().length; i++){					
					if(categoryTitleRepository.toArray()[i].equals(LocalRepository.currentDossier.getCategoryTitle())){
						comboBoxNewDossSituCategory.setSelectedIndex(i);
					}			
				}
				
				textAreaNewDossSituJudgement.setText(LocalRepository.currentDossier.getJudgementSituation());
				textAreaNewDossSituImage.setText(LocalRepository.currentDossier.getImageSituation());
				
				textAreaNewDossSituLineComComLine1.setText(LocalRepository.currentDossier.getSituationLineComment1());
				textAreaNewDossSituLineComComLine2.setText(LocalRepository.currentDossier.getSituationLineComment2());
				textAreaNewDossSituLineComComLine3.setText(LocalRepository.currentDossier.getSituationLineComment3());
				textAreaNewDossSituLineComComLine4.setText(LocalRepository.currentDossier.getSituationLineComment4());
				textAreaNewDossSituLineComComLine5.setText(LocalRepository.currentDossier.getSituationLineComment5());
				textAreaNewDossSituLineComComLine6.setText(LocalRepository.currentDossier.getSituationLineComment6());
				
				textAreaNewDossPerspJudgement.setText(LocalRepository.currentDossier.getJudgementContext());
				textAreaNewDossPerspImage.setText(LocalRepository.currentDossier.getImageContext());;
				
				textAreaNewDossPerspHexagramComment.setText(LocalRepository.currentDossier.getContextComment());
				textAreaNewDossPerspLineComComLine1.setText(LocalRepository.currentDossier.getContextCommentLine1());
				textAreaNewDossPerspLineComComLine2.setText(LocalRepository.currentDossier.getContextCommentLine2());
				textAreaNewDossPerspLineComComLine3.setText(LocalRepository.currentDossier.getContextCommentLine3());
				textAreaNewDossPerspLineComComLine4.setText(LocalRepository.currentDossier.getContextCommentLine4());
				textAreaNewDossPerspLineComComLine5.setText(LocalRepository.currentDossier.getContextCommentLine5());
				textAreaNewDossPerspLineComComLine6.setText(LocalRepository.currentDossier.getContextCommentLine6());
	
				textAreaNewDossNucJudgement.setText(LocalRepository.currentDossier.getJudgementNuclear());
				textAreaNewDossNucImage.setText(LocalRepository.currentDossier.getImageNuclear());
				
				textAreaNewDossNucHexagramComment.setText(LocalRepository.currentDossier.getNuclearComment());
				textAreaNewDossNucLineComComLine1.setText(LocalRepository.currentDossier.getNuclearLineComment1());
				textAreaNewDossNucLineComComLine2.setText(LocalRepository.currentDossier.getNuclearLineComment2());
				textAreaNewDossNucLineComComLine3.setText(LocalRepository.currentDossier.getNuclearLineComment3());
				textAreaNewDossNucLineComComLine4.setText(LocalRepository.currentDossier.getNuclearLineComment4());
				textAreaNewDossNucLineComComLine5.setText(LocalRepository.currentDossier.getNuclearLineComment5());
				textAreaNewDossNucLineComComLine6.setText(LocalRepository.currentDossier.getNuclearLineComment6());
				
				
				textAreaNewDossInverseJudgement.setText(LocalRepository.currentDossier.getJudgementInverse());
				textAreaNewDossInverseImage.setText(LocalRepository.currentDossier.getImageInverse());
				
				textAreaNewDossInverseHexagramComment.setText(LocalRepository.currentDossier.getInverseComment());
				textAreaNewDossInvLineComComLine1.setText(LocalRepository.currentDossier.getInverseLineComment1());
				textAreaNewDossInvLineComComLine2.setText(LocalRepository.currentDossier.getInverseLineComment2());
				textAreaNewDossInvLineComComLine3.setText(LocalRepository.currentDossier.getInverseLineComment3());
				textAreaNewDossInvLineComComLine4.setText(LocalRepository.currentDossier.getInverseLineComment4());
				textAreaNewDossInvLineComComLine5.setText(LocalRepository.currentDossier.getInverseLineComment5());
				textAreaNewDossInvLineComComLine6.setText(LocalRepository.currentDossier.getInverseLineComment6());	
				
				
				textAreaNewDossReverseJudgement.setText(LocalRepository.currentDossier.getJudgementReverse());
				textAreaNewDossReverseImage.setText(LocalRepository.currentDossier.getImageReverse());			
				textAreaNewDossReverseHexagramComment.setText(LocalRepository.currentDossier.getReverseComment());
				textAreaNewDossRevLineComComLine1.setText(LocalRepository.currentDossier.getReverseLineComment1());
				textAreaNewDossRevLineComComLine2.setText(LocalRepository.currentDossier.getReverseLineComment2());
				textAreaNewDossRevLineComComLine3.setText(LocalRepository.currentDossier.getReverseLineComment3());
				textAreaNewDossRevLineComComLine4.setText(LocalRepository.currentDossier.getReverseLineComment4());
				textAreaNewDossRevLineComComLine5.setText(LocalRepository.currentDossier.getReverseLineComment5());
				textAreaNewDossRevLineComComLine6.setText(LocalRepository.currentDossier.getReverseLineComment6());
				
				
				textAreaNewDossFuxiJudgement.setText(LocalRepository.currentDossier.getJudgementFuxi());
				textAreaNewDossFuxiImage.setText(LocalRepository.currentDossier.getImageFuxi());
				textAreaNewDossFuxiJudgement.setText(LocalRepository.currentDossier.getFuxiComment());
				textAreaNewDossFuxiLineComComLine1.setText(LocalRepository.currentDossier.getFuxiLineComment1());
				textAreaNewDossFuxiLineComComLine2.setText(LocalRepository.currentDossier.getFuxiLineComment2());
				textAreaNewDossFuxiLineComComLine3.setText(LocalRepository.currentDossier.getFuxiLineComment3());
				textAreaNewDossFuxiLineComComLine4.setText(LocalRepository.currentDossier.getFuxiLineComment4());
				textAreaNewDossFuxiLineComComLine5.setText(LocalRepository.currentDossier.getFuxiLineComment5());
				textAreaNewDossFuxiLineComComLine6.setText(LocalRepository.currentDossier.getFuxiLineComment6());
				
					

				
			}
		});
		
		//********************************************************************************************************************************************
		/**
		 * Zurücksetzen der Ansicht NewDossier
		 * @author ***anonymized***
		 */
		buttonNewDossSituClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				textAreaNewDossSituQuest.setText("");
				textAreaNewDossSituUserComment.setText("");
				textAreaNewDossSituHexagramComment.setText("");
				
				textAreaNewDossSituJudgement.setText("");
				textAreaNewDossSituImage.setText("");
				
				textAreaNewDossSituLineComComLine1.setText("");
				textAreaNewDossSituLineComComLine2.setText("");
				textAreaNewDossSituLineComComLine3.setText("");
				textAreaNewDossSituLineComComLine4.setText("");
				textAreaNewDossSituLineComComLine5.setText("");
				textAreaNewDossSituLineComComLine6.setText("");
				
				textAreaNewDossPerspJudgement.setText("");
				textAreaNewDossPerspImage.setText("");
				
				textAreaNewDossPerspHexagramComment.setText("");
				textAreaNewDossPerspLineComComLine1.setText("");
				textAreaNewDossPerspLineComComLine2.setText("");
				textAreaNewDossPerspLineComComLine3.setText("");
				textAreaNewDossPerspLineComComLine4.setText("");
				textAreaNewDossPerspLineComComLine5.setText("");
				textAreaNewDossPerspLineComComLine6.setText("");
	
				textAreaNewDossNucJudgement.setText("");
				textAreaNewDossNucImage.setText("");
				
				textAreaNewDossNucHexagramComment.setText("");
				textAreaNewDossNucLineComComLine1.setText("");
				textAreaNewDossNucLineComComLine2.setText("");
				textAreaNewDossNucLineComComLine3.setText("");
				textAreaNewDossNucLineComComLine4.setText("");
				textAreaNewDossNucLineComComLine5.setText("");
				textAreaNewDossNucLineComComLine6.setText("");
				
				
				textAreaNewDossInverseJudgement.setText("");
				textAreaNewDossInverseImage.setText("");
				
				textAreaNewDossInverseHexagramComment.setText("");
				textAreaNewDossInvLineComComLine1.setText("");
				textAreaNewDossInvLineComComLine2.setText("");
				textAreaNewDossInvLineComComLine3.setText("");
				textAreaNewDossInvLineComComLine4.setText("");
				textAreaNewDossInvLineComComLine5.setText("");
				textAreaNewDossInvLineComComLine6.setText("");	
				
				
				textAreaNewDossReverseJudgement.setText("");
				textAreaNewDossReverseImage.setText("");
				
				textAreaNewDossReverseHexagramComment.setText("");
				textAreaNewDossRevLineComComLine1.setText("");
				textAreaNewDossRevLineComComLine2.setText("");
				textAreaNewDossRevLineComComLine3.setText("");
				textAreaNewDossRevLineComComLine4.setText("");
				textAreaNewDossRevLineComComLine5.setText("");
				textAreaNewDossRevLineComComLine6.setText("");
				
				
				textAreaNewDossFuxiJudgement.setText("");
				textAreaNewDossFuxiImage.setText("");
				
				textAreaNewDossFuxiJudgement.setText("");
				textAreaNewDossFuxiLineComComLine1.setText("");
				textAreaNewDossFuxiLineComComLine2.setText("");
				textAreaNewDossFuxiLineComComLine3.setText("");
				textAreaNewDossFuxiLineComComLine4.setText("");
				textAreaNewDossFuxiLineComComLine5.setText("");
				textAreaNewDossFuxiLineComComLine6.setText("");
				
			}
		});
		//********************************************************************************************************************************************
		/**
		 * Exportieren der Dossier werte per Anwendung der Methode 
		 * @author ***anonymized***
		 */
		buttonShowDossExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i =0; i < LocalRepository.dossiers.length; i++){
					if(LocalRepository.dossiers[i].getQuestion().equals(listShowDossDataRight.getSelectedValue().toString())){
						LocalRepository.currentDossier = LocalRepository.dossiers[i];
					}		
				}
				

//				Dossier testDoss = new Dossier( 
//		"",
//						111111, 
//						"", 
//						"", 
//						111111,
//						1111111, 
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"", 
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"", 
//						"", 
//						"",
//						"",
//						"",
//						"", 
//						"",
//						"",
//						"", 
//						"",
//						"", 
//						"");
				
				//Füllen des Exports mit Testwerten
				Hexagram situation = new HexagramComment(perspectiveID, 0, "", "", "", "", "", "", "", "");
				Hexagram perspective = new HexagramComment(perspectiveID, 0, "", "", "", "", "", "", "", "");
				Hexagram nuclear = new HexagramComment(perspectiveID, 0, "", "", "", "", "", "", "", "");
				Hexagram inverse = new HexagramComment(perspectiveID, 0, "", "", "", "", "", "", "", "");
				Hexagram reverse = new HexagramComment(perspectiveID, 0, "", "", "", "", "", "", "", "");
				Hexagram fuxi = new HexagramComment(perspectiveID, 0, "", "", "", "", "", "", "", "");

				
				export.export(LocalRepository.currentDossier, situation, perspective, nuclear, inverse, reverse, fuxi);
				
			}
		});
		
	}
}
