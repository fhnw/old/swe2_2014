package ch.fhnw.richards.lecture02.codingStyle.kissExamples;

public class KissExample3 {

	public static void main(String[] args) {
	}
	
	public int getBinaerZahl2(int[] zahlen) {
		int bits[] = new int[zahlen.length];
		
		for (int i = 0; i < bits.length; i++) {
			bits[i] = zahlen[i] % 2;
		}
		
		int result = 0;
		for (int i = 0; i < bits.length; i++) {
			result = result * 10 + bits[i];
		}
		
		return result;
	}
	
	public int getBinaerZahl3(int[] zahlen) {
		int result = 0;
		for (int i = 0; i < zahlen.length; i++) {
			result = result * 10 +  zahlen[i] % 2;
		}
		return result;
	}
	

	public int getBinaerZahl1(int zahl1b, int zahl2b, int zahl3b, int zahl4b, int zahl5b, int zahl6b) {
		int binaerZahl1 = 0;
		int binaerZahl2 = 0;
		int binaerZahl3 = 0;
		int binaerZahl4 = 0;
		int binaerZahl5 = 0;
		int binaerZahl6 = 0;
		int binaerZahl;

		switch (zahl1b) {

		case 6:
			binaerZahl1 = 0;
			break;
		case 7:
			binaerZahl1 = 1;
			break;
		case 8:
			binaerZahl1 = 0;
			break;
		case 9:
			binaerZahl1 = 1;
			break;
		}

		switch (zahl2b) {

		case 6:
			binaerZahl2 = 0;
			break;
		case 7:
			binaerZahl2 = 1;
			break;
		case 8:
			binaerZahl2 = 0;
			break;
		case 9:
			binaerZahl2 = 1;
			break;
		}

		switch (zahl3b) {

		case 6:
			binaerZahl3 = 0;
			break;
		case 7:
			binaerZahl3 = 1;
			break;
		case 8:
			binaerZahl3 = 0;
			break;
		case 9:
			binaerZahl3 = 1;
			break;
		}

		switch (zahl4b) {

		case 6:
			binaerZahl4 = 0;
			break;
		case 7:
			binaerZahl4 = 1;
			break;
		case 8:
			binaerZahl4 = 0;
			break;
		case 9:
			binaerZahl4 = 1;
			break;
		}

		switch (zahl5b) {

		case 6:
			binaerZahl5 = 0;
			break;
		case 7:
			binaerZahl5 = 1;
			break;
		case 8:
			binaerZahl5 = 0;
			break;
		case 9:
			binaerZahl5 = 1;
			break;
		}

		switch (zahl6b) {

		case 6:
			binaerZahl6 = 0;
			break;
		case 7:
			binaerZahl6 = 1;
			break;
		case 8:
			binaerZahl6 = 0;
			break;
		case 9:
			binaerZahl6 = 1;
			break;
		}

		binaerZahl = binaerZahl6 + 10 * binaerZahl5 + 100 * binaerZahl4 + 1000 * binaerZahl3
				+ 10000 * binaerZahl2 + 100000 * binaerZahl1;

		return binaerZahl;
	}
}
