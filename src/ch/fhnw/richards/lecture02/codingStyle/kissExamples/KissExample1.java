package ch.fhnw.richards.lecture02.codingStyle.kissExamples;

/**
 * Which of the following methods is easier to understand?
 * 
 * Note that all of these schemas were proposed by presumably competent programmers; see
 * http://stackoverflow.com/questions/1709562/coding-standards-coding -best-practices-in-c
 * 
 * Consider: The purpose is to call generateReport only if all the conditions are true
 */

public class KissExample1 {

	public boolean Try1() {
		boolean retval = false;
		do {
			if (!isAdmin()) {
				break;
			}
			if (!isConditionOne()) {
				break;
			}
			if (!isConditionTwo()) {
				break;
			}
			if (!isConditionThree()) {
				break;
			}

			retval = generateReport();
		} while (false);
		return retval;
	}

	public boolean Try2() {
		if (isAdmin() && isConditionOne() && isConditionTwo() && isConditionThree()) {
			return generateReport();
		}

		return false;
	}

	public boolean Try3() {
		return isAdmin()
				&& isConditionOne()
				&& isConditionTwo()
				&& isConditionThree()
				&& generateReport();
	}
	
	public boolean Try4() {
		boolean retval = isAdmin() && isConditionOne() && isConditionTwo() && isConditionThree();
		if (retval) retval = generateReport();
		return retval;
	}

	// ----- Code below this point is irrelevant to the exercise -----

	private boolean isAdmin() {
		return false;
	}

	private boolean isConditionOne() {
		return false;
	}

	private boolean isConditionTwo() {
		return false;
	}

	private boolean isConditionThree() {
		return false;
	}

	private boolean generateReport() {
		return true;
	}
}
