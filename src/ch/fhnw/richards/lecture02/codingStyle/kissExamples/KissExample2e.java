package ch.fhnw.richards.lecture02.codingStyle.kissExamples;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

/**
 * An improved version of Example2d: If we want to combine the JButton
 * and the JLabel as a unit, we create a class to represent this entity.
 * 
 * Note how simple our main constructor has become!
 */
public class KissExample2e extends JFrame {
	private static final int NUM_INDICATORS = 6;
	ControlPair[] controlPairs = new ControlPair[NUM_INDICATORS];

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new KissExample2a();
			}
		});
	}

	public KissExample2e() {
		super();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLayout(new GridLayout(NUM_INDICATORS, 1));

		for (int i = 1; i <= NUM_INDICATORS; i++) {
			controlPairs[i - 1] = new ControlPair(this, i);
		}

		this.pack();
		this.setVisible(true);
	}

	/**
	 * An indicator has become visible; all others should
	 * be invisible
	 * 
	 * @param id The id of the visible indicator
	 */
	public void actionOccurred(int id) {
		for (ControlPair controlPair : controlPairs) {
			if (controlPair.getID() != id) controlPair.setInvisible();
		}
	}
	
	/**
	 * Our special control pair, containing both button and indicator
	 * 
	 * Note: it now makes sense to include the "visibility" functionality
	 * within this subclass; we set our own indicator to visible, and tell
	 * our parent that we have done so.
	 */
	private static class ControlPair extends Box implements ActionListener {
		private KissExample2e parent;
		private int id;
		private MyButton btn;
		private Indicator indicator;
		
		public ControlPair(KissExample2e parent, int id) {
			super(BoxLayout.X_AXIS);
			this.parent = parent;
			this.id = id;
			btn = new MyButton(id);
			btn.addActionListener(this);
			this.add(btn);
			indicator = new Indicator(id);
			this.add(indicator);			
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			indicator.setVisible(true);
			parent.actionOccurred(id);
		}
		
		public int getID() {
			return id;
		}
		
		public void setInvisible() {
			indicator.setVisible(false);
		}
	}

	private static class MyButton extends JButton {
		public MyButton(int id) {
			super();
			setBorder(new BevelBorder(BevelBorder.RAISED));
			setBackground(Color.blue);
			setForeground(Color.white);
			setText("button" + id);
			setPreferredSize(new Dimension(100, 30));
			setVisible(true);
		}
	}

	private static class Indicator extends JLabel {
		public Indicator(int id) {
			setVisible(false);
			setForeground(Color.blue);
			setPreferredSize(new Dimension(60, 30));
			setHorizontalAlignment(SwingConstants.CENTER);
			setText("icon" + id);
		}
	}
}
