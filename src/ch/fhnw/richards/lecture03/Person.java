package ch.fhnw.richards.lecture03;

import java.util.ArrayList;

/**
 * Information about a single person. In this example, we have only minimal information: name,
 * gender, father and mother. Gender is handled with an enumerated type
 */
public class Person {
	public static enum Gender { male, female };
	
	private String name;
	private Gender gender;
	private Person father;
	private Person mother;
	
	/**
	 * Constructor to set all items at the same time
	 */
	public Person(String name, Gender gender, Person father, Person mother) {
		this.name = name;
		this.gender = gender;
		this.father = father;
		this.mother = mother;
	}
	
	/**
	 * Return all siblings found either through the mother or through the father.
	 * We must eliminate "this" person from the list, since you cannot be your
	 * own sibling.
	 */
	public String getSiblingNames(ArrayList<Person> people) {
		ArrayList<Person> siblings = new ArrayList<>();
		
		for (Person p : people) {
			if (p.father == this.father || p.mother == this.mother) {
				if (p != this) {
					siblings.add(p);
				}
			}
		}

		StringBuffer tmp = new StringBuffer();
		for (Person p : siblings) {
			tmp.append(p.getName());
			tmp.append(" ");
		}
		return tmp.toString();
	}

	public String getName() {
		return name;
	}

	public Gender getGender() {
		return gender;
	}

	public Person getFather() {
		return father;
	}

	public Person getMother() {
		return mother;
	}
}
