package ch.fhnw.richards.lecture03;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

class LabelPair extends Box {
	Dimension labelSize = new Dimension(150,15);
	JLabel value;
	
	public LabelPair(String description) {
		super(BoxLayout.X_AXIS);
		this.add(Box.createHorizontalStrut(30));
		JLabel descLabel = new JLabel(description);
		descLabel.setPreferredSize(labelSize);
		this.add(descLabel);
		this.add(Box.createHorizontalStrut(30));
		value = new JLabel();
		value.setPreferredSize(labelSize);
		this.add(value);
		this.add(Box.createHorizontalStrut(30));
		this.add(Box.createHorizontalGlue());
	}
	
	public String getValue() {
		return value.getText();
	}
	
	public void setValue(String text) {
		value.setText(text);
	}
}
