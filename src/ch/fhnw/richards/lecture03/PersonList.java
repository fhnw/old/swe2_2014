package ch.fhnw.richards.lecture03;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;

public class PersonList extends Box implements ActionListener {
	FamilyRelationships main; // Reference to main program, so we can access list of people

	public PersonList(FamilyRelationships main) {
		super(BoxLayout.Y_AXIS);
		this.main = main;
		createButtons(main.getPeople());
		this.add(Box.createVerticalGlue());
	}

	private void createButtons(ArrayList<Person> people) {
		for (Person p : people) {
			JButton btn = new JButton(p.getName());
			this.add(btn);
			btn.addActionListener(this);
		}
	}

	/**
	 * When a button is pressed, get the name of the person from the button, find the person in the
	 * list of people, then tell the main program to display the details for this person.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		String name = btn.getText();
		
		Person personFound = null;
		for (Person p : main.getPeople()) {
			if (p.getName().equals(name)) {
				personFound = p;
				break;
			}
		}
		main.displayPerson(personFound);
	}
}
