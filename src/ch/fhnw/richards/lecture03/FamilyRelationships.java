package ch.fhnw.richards.lecture03;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class FamilyRelationships extends JFrame {
	private ArrayList<Person> people = new ArrayList<>(); // Central list of people
	private PersonList personList; // The display showing the people
	private PersonDetail personDetail; // The JPanel displaying the details of a person
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new FamilyRelationships();
			}
		});
	}

	private FamilyRelationships() {
		super("Family Relationships");
		createSampleData();
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		
		personList = new PersonList(this);
		this.add(personList, BorderLayout.WEST);
		
		personDetail = new PersonDetail();
		this.add(personDetail, BorderLayout.CENTER);
		
		this.pack();
		this.setVisible(true);
	}
	
	/**
	 * Allow access to the central list of people
	 * 
	 * @return An ArrayList containing all known people
	 */
	public ArrayList<Person> getPeople() {
		return people;
	}
	
	/**
	 * Display the details about a particular person
	 */
	public void displayPerson(Person p) {
		personDetail.displayPerson(p, people);
	}
	
	private void createSampleData() {
		Person ann = new Person("Ann", Person.Gender.female, null, null);
		Person ralph = new Person("Ralph", Person.Gender.male, null, null); 
		Person tom = new Person("Tom", Person.Gender.male, null, null); 
		Person jane = new Person("Jane", Person.Gender.female, null, null);
		people.add(ann); people.add(ralph); people.add(tom); people.add(jane);
		
		Person marisa = new Person("Marisa", Person.Gender.female, ralph, ann);
		Person manfred = new Person("Manfred", Person.Gender.male, ralph, ann);
		Person heidi = new Person("Heidi", Person.Gender.female, tom, jane);
		Person hilda = new Person("Hilda", Person.Gender.female, tom, jane);
		people.add(marisa); people.add(manfred); people.add(heidi); people.add(hilda);
		
		Person thomas = new Person("Thomas", Person.Gender.male, manfred, heidi);
		Person katrin = new Person("Katrin", Person.Gender.female, manfred, heidi);
		Person gabriel = new Person("Gabriel", Person.Gender.male, manfred, heidi);
		people.add(thomas); people.add(katrin); people.add(gabriel);
	}
}
