package ch.fhnw.richards.lecture03;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.border.LineBorder;

/**
 * This class displays the details about a person. Each field is displayed as a pair
 * of JLabels: one JLabel contains the description, the other contains the value.
 * These two JLabels are therefore encapsulated in a special class "LabelPair".
 */
public class PersonDetail extends Box {
	LabelPair lblName = new LabelPair("Name");
	LabelPair lblGender = new LabelPair("Gender");
	LabelPair lblFather = new LabelPair("Father");
	LabelPair lblMother = new LabelPair("Mother");
	LabelPair lblSiblings = new LabelPair("Siblings");
	
	public PersonDetail() {
		super(BoxLayout.Y_AXIS);
		this.add(Box.createVerticalStrut(50));
		this.add(lblName);
		this.add(lblGender);
		this.add(lblFather);
		this.add(lblMother);
		this.add(Box.createVerticalStrut(50));
		this.add(lblSiblings);
		this.setBorder(new LineBorder(Color.BLACK, 2));
	}
	
	public void displayPerson(Person p, ArrayList<Person> people) {
		lblName.setValue(p.getName());
		lblGender.setValue(p.getGender().toString());
		
		Person father = p.getFather();
		String fatherName = "";
		if (father != null) fatherName = father.getName();
		lblFather.setValue(fatherName);
		
		Person mother = p.getMother();
		String motherName = "";
		if (mother != null) motherName = mother.getName();
		lblMother.setValue(motherName);
		
		lblSiblings.setValue(p.getSiblingNames(people));
	}
}
