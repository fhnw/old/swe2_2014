package ch.fhnw.richards.chat.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class EventHandler implements ActionListener {
	SimpleChatClient view;
	ClientLogic logic;
	
	public EventHandler(SimpleChatClient view, ClientLogic logic) {
		this.view = view;
		this.logic = logic;
		
		// Connect ActionListener to the relevant controls
		view.btnConnect.addActionListener(this);
		view.btnSend.addActionListener(this);
		view.txtMessage.addActionListener(this);
		
		view.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				EventHandler.this.logic.disconnect();
				System.exit(0);
			}
		});
	}

	/**
	 * Determine which control generated the event,
	 * and take the appropriate action
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		Object control = evt.getSource();
		if (control == view.btnConnect) {
			logic.connect();
		} else {
			logic.sendChatMessage();
		}
	}
}
