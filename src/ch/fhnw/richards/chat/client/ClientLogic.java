package ch.fhnw.richards.chat.client;

import java.io.IOException;
import java.net.Socket;

import ch.fhnw.richards.chat.message.Message;

public class ClientLogic extends Thread {
	private SimpleChatClient view;
	private Socket socket;
	private volatile boolean stopThread = false;
	
	public ClientLogic(SimpleChatClient view) {
		this.view = view;
	}
	
	public void connect() {
		try {
			int port = Integer.parseInt(view.txtPort.getText());
			socket = new Socket(view.txtIP.getText(), port);
			Message msgRegister = new Message(Message.MessageType.Register);
			msgRegister.setName(view.txtName.getText());
			msgRegister.send(socket);
			
			// Start receiving messages
			this.start();
			
			// TODO verify that we receive a "confirm" message
		} catch (Exception e) {
			// Do nothing - we are closing anyway
		}
	}
	
	public void disconnect() {
		try {
			socket.close();
			stopThread = true;
			this.interrupt();
		} catch (IOException e) {
			// Do nothing - we are closing anyway
		}
	}
	
	public void sendChatMessage() {
		Message chat = new Message(Message.MessageType.Chat);
		chat.setName(view.txtName.getText());
		chat.setText(view.txtMessage.getText());
		view.txtMessage.setText("");
		view.txtMessage.requestFocus();		
		try {
			chat.send(socket);
		} catch (Exception e) {
			// TODO Add error handling
		}
	}
	
	@Override
	public void run() {
		while (!stopThread) {
			try {
				Message msgIn = Message.receive(socket);
				Message.MessageType msgType = msgIn.getType();
				if (msgType == Message.MessageType.Chat) {
					String content = view.txtChat.getText();
					content += "\n" + msgIn.getName() + ": " + msgIn.getText();
					view.txtChat.setText(content);
				}
			} catch (Exception e) {
				// TODO Add error handling
			}			
		}		
	}
}
