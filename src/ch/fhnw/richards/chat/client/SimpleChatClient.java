package ch.fhnw.richards.chat.client;

import java.awt.BorderLayout;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimpleChatClient extends JFrame {
	JLabel lblIP = new JLabel("IP");
	JTextField txtIP = new JTextField("127.0.0.1", 16);
	JLabel lblPort = new JLabel("Port");
	JTextField txtPort = new JTextField("50001", 6);
	JLabel lblName = new JLabel("Name");
	JTextField txtName = new JTextField(10);
	JButton btnConnect = new JButton("Connect");
	JTextArea txtChat = new JTextArea(20, 50);
	JTextField txtMessage = new JTextField(50);
	JButton btnSend = new JButton("Send");

	public static void main(String[] args) {
		SimpleChatClient view = new SimpleChatClient();
		ClientLogic logic = new ClientLogic(view);
		EventHandler controller = new EventHandler(view, logic);
	}

	public SimpleChatClient() {
		this.setTitle("SimpleChat client");
		this.setLayout(new BorderLayout());
		
		// Top box contains IP/Port info
		Box topBox = Box.createHorizontalBox();
		topBox.add(lblIP);
		topBox.add(txtIP);
		topBox.add(Box.createHorizontalStrut(20));
		topBox.add(lblPort);
		topBox.add(txtPort);
		topBox.add(Box.createHorizontalStrut(20));
		topBox.add(lblName);
		topBox.add(txtName);
		topBox.add(Box.createHorizontalStrut(50));
		topBox.add(Box.createHorizontalGlue());
		topBox.add(btnConnect);
		this.add(topBox, BorderLayout.NORTH);
		
		// Bottom box contains message and send-button
		Box bottomBox = Box.createHorizontalBox();
		bottomBox.add(txtMessage);
		bottomBox.add(btnSend);
		this.add(bottomBox, BorderLayout.SOUTH);

		// put txtClients in a ScrollPane
		JScrollPane scrollPane = new JScrollPane(txtChat);
		txtChat.setEditable(false);
		this.add(scrollPane, BorderLayout.CENTER);

		pack();
		setVisible(true);
	}
}