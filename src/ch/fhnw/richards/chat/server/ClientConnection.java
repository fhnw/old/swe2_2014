package ch.fhnw.richards.chat.server;

import java.io.IOException;
import java.net.Socket;

import ch.fhnw.richards.chat.message.Message;

public class ClientConnection extends Thread {
	private EventHandler controller;
	private Socket socket;
	private String name;
	
	public ClientConnection(EventHandler controller, Socket socket) {
		this.controller = controller;
		this.socket = socket;
		this.name = socket.getInetAddress().toString(); // default
	}
	
	public void close() {
		try {
			this.interrupt(); // stop waiting for messages			
			socket.close();
		} catch (IOException e) {
			// We are closing - do nothing
		}
	}
	
	@Override
	public boolean equals(Object c) {
		boolean isEqual = false;
		if (c instanceof ClientConnection) {
			isEqual = ((ClientConnection) c).name.equals(name);
		}
		return isEqual;
	}
	
	@Override
	public String toString() {
		return name + " (" + socket.getInetAddress().toString() + ")" + '\n';
	}
	
	/**
	 * Process messages forever...
	 */
	@Override
	public void run() {
		try {
			// Answer messages forever
			Message.MessageType lastMessageType = null;
			while (true) {
				// Read a message from the client
				Message msgIn = Message.receive(socket);
				lastMessageType = msgIn.getType();

				// Process message
				Message msgOut = null;
				switch (lastMessageType) {
				case Register:
					msgOut = new Message(Message.MessageType.Confirm);
					name = msgIn.getName(); // Set client's name
					msgOut.setName(name);
					msgOut.send(socket);
					controller.updateClientDisplay();
					break;
				case Chat:
					controller.broadcast(msgIn);
					break;
				}
			}
		} catch (Exception e) {
			// TODO Add error handling
		}
	}
	
	public Socket getSocket() {
		return socket;
	}
}
