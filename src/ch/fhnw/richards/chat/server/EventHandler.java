package ch.fhnw.richards.chat.server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import ch.fhnw.richards.chat.message.Message;

public class EventHandler {
	SimpleChatServer view;
	private ArrayList<ClientConnection> clientList = new ArrayList<>();
	private Listener listener;
	
	public EventHandler(SimpleChatServer view) {
		this.view = view;
		
		view.btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				startServer();
			}
		});
		
		view.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				stopServer();
			}
		});
	}
	
	/**
	 * Start the ServerSocket and set up client management 
	 */
	private void startServer() {
		int portNumber = Integer.parseInt(view.txtPort.getText());
		try {
			listener = new Listener(this, portNumber);
			listener.start();
			view.btnStart.setEnabled(false);
		} catch (IOException e) {
			// TODO Add error handling
		}
	}
	
	/**
	 * Terminate all client connections, end the server
	 */
	private void stopServer() {
		listener.stopListening();
		
		// Important: Do not depend on structure of list,
		// as we will be changing it. Hence, "while not empty"
		while (!clientList.isEmpty()) {
			ClientConnection client = clientList.get(0);
			client.close();
			removeClient(client);
		}
		
		System.exit(0);
	}

	/**
	 * Add a new client to the connection list
	 */
	public void addClient(ClientConnection newClient) {
		synchronized (clientList) {
			clientList.add(newClient);
			view.updateClientDisplay(clientList);
		}
	}

	/**
	 * Remove an existing client from the list, when the connection has been closed
	 */
	public void removeClient(ClientConnection oldClient) {
		synchronized (clientList) {
			for (Iterator<ClientConnection> i = clientList.iterator(); i.hasNext();) {
				ClientConnection client = i.next();
				if (client.equals(oldClient))
					i.remove();
			}
			view.updateClientDisplay(clientList);
		}
	}
	
	/**
	 * Broadcast a chat message to all clients
	 */
	public void broadcast(Message msgIn) {
		for (ClientConnection client : clientList) {
			try {
				msgIn.send(client.getSocket());
			} catch (Exception e) {
				// TODO Add error handling
			}
		}
	}
	
	/**
	 * Trigger a display update in the view
	 */
	public void updateClientDisplay() {
		view.updateClientDisplay(clientList);
	}
}
