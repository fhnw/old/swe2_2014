package ch.fhnw.richards.chat.message;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.simpleframework.xml.*;
import org.simpleframework.xml.core.Persister;

@Root
public class Message {
	public enum MessageType {
		Register, Confirm, Error, Chat
	};

	// Data included in a message
	@Element
	private MessageType type;

	@Element
	private String name;

	@Element(required = false)
	private String text;

	/**
	 * Receive a message: create a message object and fill it using data transmitted over the given
	 * socket.
	 * 
	 * @param socket
	 *          the socket to read from
	 * @return the new message object, or null in case of an error
	 * @throws Exception
	 */
	public static Message receive(Socket socket) throws Exception {
		Serializer netIn = new Persister();
		InputStream in = socket.getInputStream();
		Message msg = netIn.read(Message.class, in);
		return msg;
	}

	/**
	 * Constructor to create new messages; the annotation is required to allow the SimpleXML interface
	 * to create objects from XML input.
	 * 
	 * @param type
	 *          the type of the message
	 */
	public Message(@Element(name = "type") MessageType type) {
		this.type = type;
	}

	/**
	 * Send the current message.
	 * 
	 * @param socket
	 *          the socket to write to
	 * @throws Exception
	 */
	public void send(Socket socket) throws Exception {
		Serializer netOut = new Persister();
		OutputStream out = socket.getOutputStream();
		netOut.write(this, out);
		out.write('\n');
		out.flush();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public MessageType getType() {
		return type;
	}
}
