package ch.fhnw.richards.lecture01.rounding;

public class RoundingInt {

	public static void main(String[] args) {
		int a = 1000000;
		int b = (a+1) / 1000;
		int c = a / 1000;
		int d = b-c;
		int e = 1000000 * d;
		int f = e - 1000;
		System.out.print(f);
	}

}
